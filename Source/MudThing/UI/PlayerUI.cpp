// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerUI.h"
#include "Player/MainPlayer.h"
#include "Engine/World.h"
#include "Player/MainPlayerController.h"
#include "Player/StatSubsystem.h"
#include "UI/MainHUD.h"
#include "TimerManager.h"
#include "Components/Button.h"
#include "Components/UniformGridPanel.h"

FTimerDelegate RestTimerDelegate;

bool UPlayerUI::Initialize()
{
	bool Success = Super::Initialize();
	if (!Success) return false;

	if (!ensure(btnForward != nullptr)) return false;
	btnForward->OnClicked.AddDynamic(this, &UPlayerUI::MoveForwardPressed);

	if (!ensure(btnBackwards != nullptr)) return false;
	btnBackwards->OnClicked.AddDynamic(this, &UPlayerUI::TurnAroundPressed);

	if (!ensure(btnLeft != nullptr)) return false;
	btnLeft->OnClicked.AddDynamic(this, &UPlayerUI::TurnLeftPressed);

	if (!ensure(btnRight != nullptr)) return false;
	btnRight->OnClicked.AddDynamic(this, &UPlayerUI::TurnRightPressed);

	if (!ensure(btnRight45 != nullptr)) return false;
	btnRight45->OnClicked.AddDynamic(this, &UPlayerUI::Right45Pressed);

	if (!ensure(btnLeft45 != nullptr)) return false;
	btnLeft45->OnClicked.AddDynamic(this, &UPlayerUI::Left45Pressed);

	if (!ensure(btn_Commands != nullptr)) return false;
	btn_Commands->OnClicked.AddDynamic(this, &UPlayerUI::CommandsClicked);

	if (!ensure(btn_Inventory != nullptr)) return false;
	btn_Inventory->OnClicked.AddDynamic(this, &UPlayerUI::InventoryClicked);

	if (!ensure(btn_Rest != nullptr)) return false;
	btn_Rest->OnClicked.AddDynamic(this, &UPlayerUI::RestClicked);

	return true;
}

AMainPlayerController* UPlayerUI::GetMainPC()
{
	UWorld* World = GetWorld();
	AMainPlayerController* TheMainPC = Cast<AMainPlayerController>(World->GetFirstPlayerController());
	return TheMainPC;
}

UStatSubsystem* UPlayerUI::GetStatSubsys()
{
	UGameInstance* GameInstance = Cast<UGameInstance>(GetGameInstance());
	UStatSubsystem* TheSubsys = GameInstance->GetSubsystem<UStatSubsystem>();
	return TheSubsys;
}

void UPlayerUI::MoveForwardPressed()
{
	StatSubsys = GetStatSubsys();
	MainPC = GetMainPC();
	if (!ensure(StatSubsys != nullptr)) return;
	if (!ensure(MainPC != nullptr)) return;
	StatSubsys->StopResting();
	GetWorld()->GetTimerManager().PauseTimer(RestTimerHandle);
	MainPC->MoveToTargetPoint();
}

void UPlayerUI::TurnAroundPressed()
{
	StatSubsys = GetStatSubsys();
	MainPC = GetMainPC();
	if (!ensure(StatSubsys != nullptr)) return;
	if (!ensure(MainPC != nullptr)) return;
	StatSubsys->StopResting();
	GetWorld()->GetTimerManager().PauseTimer(RestTimerHandle);
	MainPC->TurnAround();
}

void UPlayerUI::TurnLeftPressed()
{
	StatSubsys = GetStatSubsys();
	MainPC = GetMainPC();
	if (!ensure(StatSubsys != nullptr)) return;
	if (!ensure(MainPC != nullptr)) return;
	StatSubsys->StopResting();
	GetWorld()->GetTimerManager().PauseTimer(RestTimerHandle);
	MainPC->TurnLeft();
}

void UPlayerUI::TurnRightPressed()
{
	StatSubsys = GetStatSubsys();
	MainPC = GetMainPC();
	if (!ensure(StatSubsys != nullptr)) return;
	if (!ensure(MainPC != nullptr)) return;
	StatSubsys->StopResting();
	GetWorld()->GetTimerManager().PauseTimer(RestTimerHandle);
	MainPC->TurnRight();
}

void UPlayerUI::Left45Pressed()
{
	StatSubsys = GetStatSubsys();
	MainPC = GetMainPC();
	if (!ensure(StatSubsys != nullptr)) return;
	if (!ensure(MainPC != nullptr)) return;
	StatSubsys->StopResting();
	GetWorld()->GetTimerManager().PauseTimer(RestTimerHandle);
	MainPC->TurnLeft45();
}

void UPlayerUI::Right45Pressed()
{
	StatSubsys = GetStatSubsys();
	MainPC = GetMainPC();
	if (!ensure(StatSubsys != nullptr)) return;
	if (!ensure(MainPC != nullptr)) return;
	StatSubsys->StopResting();
	GetWorld()->GetTimerManager().PauseTimer(RestTimerHandle);
	MainPC->TurnRight45();
}

void UPlayerUI::CommandsClicked()
{
	MainPC = GetMainPC();
	btn_Commands->SetVisibility(ESlateVisibility::Hidden);
	if (!ensure(ugp_CommandPanel != nullptr)) return;
	ugp_CommandPanel->SetVisibility(ESlateVisibility::Visible);
	UWidget* Child = ugp_CommandPanel->GetChildAt(0);
	Child->SetUserFocus(MainPC);
}

void UPlayerUI::InventoryClicked()
{
	MainPC = GetMainPC();
	if (!ensure(MainPC != nullptr)) return;
	MainPC->ToggleStatusScreen();
}

// Not yet implemented, will fire every frame, setup timer
void UPlayerUI::RestClicked()
{
	MainPC = GetMainPC();
	if (!ensure(MainPC != nullptr)) return;
	AMainHUD* MainHUD = Cast<AMainHUD>(MainPC->GetHUD());
	if (!ensure(MainHUD != nullptr)) return;
	StatSubsys = GetStatSubsys();
	if (!ensure(StatSubsys != nullptr)) return;
	StatSubsys->bIsCurrentlyResting = true;
	if (StatSubsys->bStopResting == false)
	{
		MainHUD->ReportToUI("Your party sits down to rest.", false);
		MainHUD->bIsInRestMode = true;
		MainPC->ToggleCombat({ 0 });
		
		int32 NumInParty = StatSubsys->NumberOfPlayersInParty;

		for (int32 i = 0; i <= NumInParty - 1; i++)
		{
			// fire timer
			UE_LOG(LogTemp, Warning, TEXT("Looping"));
			RestTimerDelegate.BindUFunction(this, FName("FireRestTick"), i);
			if (!GetWorld()->GetTimerManager().IsTimerActive(RestTimerHandle))
			{
				GetWorld()->GetTimerManager().SetTimer(RestTimerHandle, RestTimerDelegate, RestTickDelay, false);
			}
			//StatSubsys->RestTick((ESelectedCharacter)i);
		}
	}
}

void UPlayerUI::FireRestTick(int32 PlayerIndex)
{
	StatSubsys = GetStatSubsys();
	StatSubsys->RestTick((ESelectedCharacter)PlayerIndex);
	if (StatSubsys->bStopResting == false)
	{
		RestClicked();
	}
	else
	{
		StatSubsys->StopResting();
	}
}

EDirection UPlayerUI::GetFacingDirection()
{
	EDirection FacingDirection;
	FVector FacingVector;
	auto World = GetWorld();
	if (World)
	{
		auto PlayerController = World->GetFirstPlayerController();
		if (PlayerController)
		{
			MainPlayer = Cast<AMainPlayer>(PlayerController->GetPawn());
			FacingVector = MainPlayer->GetActorForwardVector();
			float FacingX = FacingVector.X;
			float FacingY = FacingVector.Y;
			if (FMath::IsNearlyEqual(FacingX, 1, 0.1f))
			{
				FacingDirection = EDirection::D_North;
				return FacingDirection;
			}
			if (FMath::IsNearlyEqual(FacingX, -1, 0.1f))
			{
				FacingDirection = EDirection::D_South;
				return FacingDirection;
			}
			if (FMath::IsNearlyEqual(FacingX, 0.707f, 0.1f))
			{
				if (FMath::IsNearlyEqual(FacingY, 0.707f, 0.1f))
				{
					FacingDirection = EDirection::D_Northeast;
					return FacingDirection;
				}
				else
				{
					FacingDirection = EDirection::D_Northwest;
					return FacingDirection;
				}
			}
			if (FMath::IsNearlyEqual(FacingX, -0.707f, 0.1f))
			{
				if (FMath::IsNearlyEqual(FacingY, -0.707f, 0.1f))
				{
					FacingDirection = EDirection::D_Southwest;
					return FacingDirection;
				}
				else
				{
					FacingDirection = EDirection::D_Southeast;
					return FacingDirection;
				}
			}
			if (FMath::IsNearlyEqual(FacingY, 1.0f, 0.1f))
			{
				FacingDirection = EDirection::D_East;
				return FacingDirection;
			}
			if (FMath::IsNearlyEqual(FacingY, -1.0f, 0.1f))
			{
				FacingDirection = EDirection::D_West;
				return FacingDirection;
			}			
		}
	}
	UE_LOG(LogTemp, Warning, TEXT("Should Never Get here in code"));
	return EDirection::D_North;
}
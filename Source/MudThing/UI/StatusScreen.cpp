// Copyright 2019, fadedGames - http://fadedGames.net


#include "StatusScreen.h"
#include "Player/StatSubsystem.h"
#include "Player/InventorySubsystem.h"
#include "Components/TextBlock.h"
#include "Inventory/ItemWidget.h"
#include "Components/Button.h"
#include "Components/ScrollBox.h"


bool UStatusScreen::Initialize()
{
	bool Success = Super::Initialize();
	if (!Success) return false;

	return true;
}

UStatSubsystem* UStatusScreen::GetStatSubsys()
{
	UGameInstance* GameInstance = Cast<UGameInstance>(GetGameInstance());
	UStatSubsystem* TheSubsys = GameInstance->GetSubsystem<UStatSubsystem>();
	return TheSubsys;
}

UInventorySubsystem* UStatusScreen::GetInventorySubsys()
{
	UGameInstance* GameInstance = Cast<UGameInstance>(GetGameInstance());
	if (!GameInstance) return nullptr;
	UInventorySubsystem* TheSubsys = GameInstance->GetSubsystem<UInventorySubsystem>();
	return TheSubsys;
}

void UStatusScreen::GetBasicStats(ESelectedCharacter SelectedChar)
{
	StatSubsys = GetStatSubsys();

	CharData = StatSubsys->GetCharacterData(SelectedChar);
	FString HoldingString;
	HoldingString = FString::FromInt(CharData.CharacterBaseStats.Strength);
	txt_Str->SetText(FText::FromString(HoldingString));

	HoldingString = FString::FromInt(CharData.CharacterBaseStats.Agility);
	txt_Agi->SetText(FText::FromString(HoldingString));

	HoldingString = FString::FromInt(CharData.CharacterBaseStats.Intellect);
	txt_Int->SetText(FText::FromString(HoldingString));

	HoldingString = FString::FromInt(CharData.CharacterBaseStats.Wisdom);
	txt_Wis->SetText(FText::FromString(HoldingString));

	HoldingString = FString::FromInt(CharData.CharacterBaseStats.Constitution);
	txt_Con->SetText(FText::FromString(HoldingString));

	HoldingString = FString::FromInt(CharData.CharacterBaseStats.Charm);
	txt_Cha->SetText(FText::FromString(HoldingString));

	HoldingString = CharData.CharacterName.ToString();
	txt_Name->SetText(FText::FromString(HoldingString));
}

void UStatusScreen::GetSecondaries(ESelectedCharacter SelectedChar)
{
	StatSubsys = GetStatSubsys();

	CharData = StatSubsys->GetCharacterData(SelectedChar);
	FString HoldingString;
	int32 HoldingInt;
	HoldingInt = FMath::RoundToInt(CharData.CharacterSecondaryStats.Perception);
	HoldingString = FString::FromInt(HoldingInt);
	txt_Perception->SetText(FText::FromString(HoldingString));

	HoldingInt = FMath::RoundToInt(CharData.CharacterSecondaryStats.Stealth);
	HoldingString = FString::FromInt(HoldingInt);
	txt_Stealth->SetText(FText::FromString(HoldingString));

	HoldingInt = FMath::RoundToInt(CharData.CharacterSecondaryStats.Traps);
	HoldingString = FString::FromInt(HoldingInt);
	txt_Traps->SetText(FText::FromString(HoldingString));

	HoldingInt = FMath::RoundToInt(CharData.CharacterSecondaryStats.Picklocks);
	HoldingString = FString::FromInt(HoldingInt);
	txt_Picks->SetText(FText::FromString(HoldingString));

	HoldingInt = FMath::RoundToInt(CharData.CharacterSecondaryStats.MartialArts);
	HoldingString = FString::FromInt(HoldingInt);
	txt_MA->SetText(FText::FromString(HoldingString));

	HoldingInt = FMath::RoundToInt(CharData.CharacterCombatStats.ResistMagic);
	HoldingString = FString::FromInt(HoldingInt);
	txt_MR->SetText(FText::FromString(HoldingString));
}

void UStatusScreen::GetCombatStats(ESelectedCharacter SelectedChar)
{
	StatSubsys = GetStatSubsys();

	CharData = StatSubsys->GetCharacterData(SelectedChar);
	FString HoldingString;
	int32 HoldingInt;
	int32 HoldingInt2;
	HoldingInt = CharData.CharacterBaseStats.Experience;
	HoldingString = FString::FromInt(HoldingInt);
	txt_Exp->SetText(FText::FromString(HoldingString));

	HoldingInt = CharData.CharacterBaseStats.Level;
	HoldingString = FString::FromInt(HoldingInt);
	txt_Level->SetText(FText::FromString(HoldingString));

	HoldingInt = FMath::RoundToInt(CharData.CharacterCombatStats.AC_Hit);
	HoldingInt2 = FMath::RoundToInt(CharData.CharacterCombatStats.AC_DR);
	HoldingString = FString::FromInt(HoldingInt) + "/" + FString::FromInt(HoldingInt2);
	txt_AC->SetText(FText::FromString(HoldingString));

	HoldingString = StatSubsys->GetClassAsString(CharData.CharacterBaseStats.Class);
	txt_Class->SetText(FText::FromString(HoldingString));

	HoldingInt = CharData.CharacterBaseStats.HP_Current;
	HoldingInt2 = CharData.CharacterBaseStats.HP_Max;
	HoldingString = FString::FromInt(HoldingInt) + "/" + FString::FromInt(HoldingInt2);
	txt_HP->SetText(FText::FromString(HoldingString));

	HoldingInt = CharData.CharacterBaseStats.CP;
	HoldingString = FString::FromInt(HoldingInt);
	tct_CP->SetText(FText::FromString(HoldingString));

	if (CharData.CharacterBaseStats.MP_Max == -1)
	{
		txt_MAText->SetVisibility(ESlateVisibility::Hidden);
		txt_Mana->SetVisibility(ESlateVisibility::Hidden);
	}
	else
	{
		txt_MAText->SetVisibility(ESlateVisibility::Visible);
		txt_Mana->SetVisibility(ESlateVisibility::Visible);
		HoldingInt = CharData.CharacterBaseStats.MP_Current;
		HoldingInt2 = CharData.CharacterBaseStats.MP_Max;
		HoldingString = FString::FromInt(HoldingInt) + "/" + FString::FromInt(HoldingInt2);
		txt_Mana->SetText(FText::FromString(HoldingString));
	}

	if (CharData.CharacterCombatStats.Spellcasting == -1)
	{
		txt_Spellcasting->SetVisibility(ESlateVisibility::Hidden);
		txt_Spellcasting_desc->SetVisibility(ESlateVisibility::Hidden);
	}
	else
	{
		txt_Spellcasting->SetVisibility(ESlateVisibility::Visible);
		txt_Spellcasting_desc->SetVisibility(ESlateVisibility::Visible);
		HoldingInt = FMath::RoundToInt(CharData.CharacterCombatStats.Spellcasting);
		HoldingString = FString::FromInt(HoldingInt);
		txt_Spellcasting->SetText(FText::FromString(HoldingString));
	}
}

void UStatusScreen::PopulateItemListForChar(ESelectedCharacter SelectedChar)
{
	FStringClassReference ItemWidgetClassRef(TEXT("WidgetBlueprint'/Game/UI/Inventory/WBP_Item.WBP_Item'"));
	sb_Inventory->ClearChildren();
	InventorySubsys = GetInventorySubsys();
	if (!InventorySubsys) return;
	TArray<FItem> WorkingInventory = InventorySubsys->GetCharInventory(SelectedChar);
	for (int i = 0; i < WorkingInventory.Num(); i++)
	{
		UE_LOG(LogTemp, Warning, TEXT("For Loop running iteration: %i"), i);
		FString ItemName = WorkingInventory[i].ItemName.ToString();
		if (ItemName == "" || ItemName == "None")
		{
			UE_LOG(LogTemp, Warning, TEXT("Name Check failed"));
			return;
		}
		else
		{
			if (UClass* ItemWidgetClass = ItemWidgetClassRef.TryLoadClass<UItemWidget>())
			{
				UE_LOG(LogTemp, Warning, TEXT("Creating Widget"));
				CreateWidget(this, ItemWidgetClass);
				UItemWidget* ItemWidgetCast = Cast<UItemWidget>(ItemWidgetClass);
				ItemWidgetCast->IndexInInventory = i;
				ItemWidgetCast->MyItem = WorkingInventory[i];
				ItemWidgetCast->txt_Item->SetText(FText::FromString(ItemName));
				sb_Inventory->AddChild(ItemWidgetCast);
			}
		}
	}
}
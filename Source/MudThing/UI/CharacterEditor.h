// Copyright 2019, fadedGames - http://fadedGames.net

#pragma once

#include "GlobalData.h"

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "CharacterEditor.generated.h"

/**
 * 
 */

class UButton;
class UComboBoxString;
class UCanvasPanel;
class UEditableTextBox;
class UTextBlock;
class UVerticalBox;

UENUM(BlueprintType)
enum class EStatToChange : uint8
{
	ESTC_Strength,
	ESTC_Agility,
	ESTC_Intellect,
	ESTC_Wisdom,
	ESTC_Constitution,
	ESTC_Charm
};

UCLASS()
class MUDTHING_API UCharacterEditor : public UUserWidget
{
	GENERATED_BODY()

public:
	virtual bool Initialize() override;

	void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

protected:
	// UI Logic Variables
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "UI Logic")
	bool bRaceSelected = false;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "UI Logic")
	bool bClassSelected = false;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "UI Logic")
	TArray<int32> AbilityKeys;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "UI Logic")
	TArray<float> AbilityValues;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "UI Logic")
	FString AbilityBonusString;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "UI Logic")
	FString ExpTable;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "UI Logic")
	int32 ExpForNextLevel;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "UI Logic")
	bool bConfirmedName;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "UI Logic")
	bool bConfirmedOnce = false;

	// UI Elements
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "UI Elements", meta = (BindWidget))
	UButton* btn_BackToRace;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "UI Elements", meta = (BindWidget))
	UButton* btn_ConfirmClass;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "UI Elements", meta = (BindWidget))
	UButton* btn_ConfirmRace;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "UI Elements", meta = (BindWidget))
	UButton* btn_SelectedName;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "UI Elements", meta = (BindWidget))
	UComboBoxString* cb_ClassSelect;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "UI Elements", meta = (BindWidget))
	UComboBoxString* cb_RaceSelect;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "UI Elements", meta = (BindWidget))
	UCanvasPanel* cp_CharEdit;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "UI Elements", meta = (BindWidget))
	UCanvasPanel* CP_CreationPanel;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "UI Elements", meta = (BindWidget))
	UEditableTextBox* etb_NameEntry;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "UI Elements", meta = (BindWidget))
	UTextBlock* txt_Description;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "UI Elements", meta = (BindWidget))
	UTextBlock* txt_EnterName;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "UI Elements", meta = (BindWidget))
	UTextBlock* txt_Select;

	// Data Variables
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "DataVariables")
	ERaces SelectedRace;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "DataVariables")
	EClasses SelectedClass;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "DataVariables")
	ESelectedCharacter SelectedCharacter;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "DataVariables")
	FName CharacterName;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "DataVariables")
	FClassData ClassData;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "DataVariables")
	FRaceData RaceData;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "DataVariables")
	FCharacterData CharacterData;

	// Creation Elements
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CreationElements", meta = (BindWidget))
	UButton* btn_LowerAgi;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CreationElements", meta = (BindWidget))
	UButton* btn_LowerCha;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CreationElements", meta = (BindWidget))
	UButton* btn_LowerCon;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CreationElements", meta = (BindWidget))
	UButton* btn_LowerInt;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CreationElements", meta = (BindWidget))
	UButton* btn_LowerStr;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CreationElements", meta = (BindWidget))
	UButton* btn_LowerWis;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CreationElements", meta = (BindWidget))
	UButton* btn_RaiseAgi;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CreationElements", meta = (BindWidget))
	UButton* btn_RaiseCha;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CreationElements", meta = (BindWidget))
	UButton* btn_RaiseCon;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CreationElements", meta = (BindWidget))
	UButton* btn_RaiseInt;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CreationElements", meta = (BindWidget))
	UButton* btn_RaiseStr;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CreationElements", meta = (BindWidget))
	UButton* btn_RaiseWis;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CreationElements", meta = (BindWidget))
	UTextBlock* txt_AgiCurrent;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CreationElements", meta = (BindWidget))
	UTextBlock* txt_ChaCurrent;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CreationElements", meta = (BindWidget))
	UTextBlock* txt_ConCurrent;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CreationElements", meta = (BindWidget))
	UTextBlock* txt_IntCurrent;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CreationElements", meta = (BindWidget))
	UTextBlock* txt_StrCurrent;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CreationElements", meta = (BindWidget))
	UTextBlock* txt_WisCurrent;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CreationElements", meta = (BindWidget))
	UTextBlock* txt_AgiRange;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CreationElements", meta = (BindWidget))
	UTextBlock* txt_ChaRange;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CreationElements", meta = (BindWidget))
	UTextBlock* txt_ConRange;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CreationElements", meta = (BindWidget))
	UTextBlock* txt_IntRange;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CreationElements", meta = (BindWidget))
	UTextBlock* txt_StrRange;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CreationElements", meta = (BindWidget))
	UTextBlock* txt_WisRange;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CreationElements", meta = (BindWidget))
	UTextBlock* txt_EditorClass;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CreationElements", meta = (BindWidget))
	UTextBlock* txt_EditorRace;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CreationElements", meta = (BindWidget))
	UTextBlock* txt_EditorName;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CreationElements", meta = (BindWidget))
	UTextBlock* txt_RemainingCP;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CreationElements", meta = (BindWidget))
	UTextBlock* txt_UnusedCPWarning;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CreationElements", meta = (BindWidget))
	UTextBlock* txt_UnusedCPConfirm;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CreationElements", meta = (BindWidget))
	UVerticalBox* vb_EditorLowerStat;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CreationElements", meta = (BindWidget))
	UVerticalBox* vb_EditorRaiseStat;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CreationElements", meta = (BindWidget))
	UVerticalBox* vb_EditorStatData;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CreationElements", meta = (BindWidget))
	UVerticalBox* vb_EditorStatDesc;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CreationElements", meta = (BindWidget))
	UVerticalBox* vb_EditorStatEntry;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CreationElements", meta = (BindWidget))
	UVerticalBox* vb_EditorTopData;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CreationElements", meta = (BindWidget))
	UVerticalBox* vb_EditorTopDesc;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CreationElements", meta = (BindWidget))
	UButton* btn_ConfirmRolls;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CreationElements", meta = (BindWidget))
	UButton* btn_StartOver;

	UFUNCTION(BlueprintCallable)
	void PopulateEditorData();

	UFUNCTION(BlueprintCallable)
	void PopulateComboBoxes();

	UFUNCTION(BlueprintCallable)
	void PopulateDescWithRace();

	UFUNCTION(BlueprintCallable)
	void PopulateDescWithClass();

	UFUNCTION()
	void ConfirmStatRolls();

	UFUNCTION(BlueprintImplementableEvent)
	void CreateSaveDataBP(FCharacterData CharData, ESelectedCharacter SelChar);

private:

	class UStatSubsystem* StatSubsys;

	FText MakeStatRangeString(int32 Min, int32 Max);

	FRaceData* GetRaceData(ERaces RaceToGet);

	FRaceData* GetRaceData(int RaceIndex);

	FClassData* GetClassData(EClasses ClassToGet);

	FClassData* GetClassData(int ClassIndex);

	UStatSubsystem* GetStatSubsys();

	UFUNCTION()
	void RaceChanged(FString sItem, ESelectInfo::Type selType);

	UFUNCTION()
	void ClassChanged(FString sItem, ESelectInfo::Type selType);

	UFUNCTION()
	void AllowClassSelection();

	UFUNCTION()
	void BackToRaceSelection();

	UFUNCTION()
	void NameSelected(const FText& InText, ETextCommit::Type CommitMethod);

	UFUNCTION()
	void AllowNameSelection();

	UFUNCTION()
	void NameSelectionClicked();

	bool CheckIfNameIsValid(FText SelectedName);

	FString RaceEnumToString(ERaces RaceToConvert);
	FString ClassEnumAsString(EClasses ClassToGet);

	UFUNCTION()
	void LowerStr();

	UFUNCTION()
	void LowerAgi();

	UFUNCTION()
	void LowerInt();

	UFUNCTION()
	void LowerWis();

	UFUNCTION()
	void LowerCon();

	UFUNCTION()
	void LowerCha();

	UFUNCTION()
	void RaiseStr();

	UFUNCTION()
	void RaiseAgi();

	UFUNCTION()
	void RaiseInt();

	UFUNCTION()
	void RaiseWis();

	UFUNCTION()
	void RaiseCon();

	UFUNCTION()
	void RaiseCha();

	void RollFinalStats();

	UFUNCTION()
	void StartOver();

	float SelectedRaceExpTable;

	float SelectedClassExpTable;

	float TotalExpTable;
};

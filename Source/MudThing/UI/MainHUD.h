// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "MainHUD.generated.h"

class UUserWidget;

UCLASS()
class MUDTHING_API AMainHUD : public AHUD
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintImplementableEvent)
	void ToggleMainUI();

	UFUNCTION(BlueprintImplementableEvent)
	void ToggleCombatUI();

	UFUNCTION(BlueprintImplementableEvent)
	void ToggleStatusScreen();

	UFUNCTION(BlueprintImplementableEvent)
	void TabUp();

	UFUNCTION(BlueprintImplementableEvent)
	void TabDown();

	UFUNCTION(BlueprintImplementableEvent)
	void ToggleMainMenu();

	UFUNCTION(BlueprintImplementableEvent)
	void ToggleTextLog();

	UFUNCTION(BlueprintImplementableEvent)
	void ReportToUI(const FString& TextToReport, bool Overwrite);

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	bool bIsInRestMode;
};

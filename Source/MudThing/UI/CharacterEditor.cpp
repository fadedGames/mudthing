// Copyright 2019, fadedGames - http://fadedGames.net


#include "CharacterEditor.h"
#include "Components/Button.h"
#include "Components/ComboBoxString.h"
#include "Components/EditableTextBox.h"
#include "Components/CanvasPanel.h"
#include "Components/TextBlock.h"
#include "Components/VerticalBox.h"
#include "Player/StatSubsystem.h"
#include "Engine/World.h"
#include "ConstructorHelpers.h"
#include "Engine/DataTable.h"
#include "Kismet/GameplayStatics.h"



bool UCharacterEditor::Initialize()
{
	bool Successful = Super::Initialize();
	if (!Successful) return false;
	PopulateComboBoxes();
	SelectedCharacter = ESelectedCharacter::SC_Character0;
	cp_CharEdit->SetVisibility(ESlateVisibility::Hidden);
	
	PopulateDescWithRace();
	// combo box bindings
	cb_RaceSelect->OnSelectionChanged.AddDynamic(this, &UCharacterEditor::RaceChanged);
	cb_ClassSelect->OnSelectionChanged.AddDynamic(this, &UCharacterEditor::ClassChanged);

	// text box bindings
	etb_NameEntry->OnTextCommitted.AddDynamic(this, &UCharacterEditor::NameSelected);

	// button bindings
	btn_ConfirmRace->OnClicked.AddDynamic(this, &UCharacterEditor::AllowClassSelection);
	btn_BackToRace->OnClicked.AddDynamic(this, &UCharacterEditor::BackToRaceSelection);
	btn_ConfirmClass->OnClicked.AddDynamic(this, &UCharacterEditor::AllowNameSelection);
	btn_SelectedName->OnClicked.AddDynamic(this, &UCharacterEditor::NameSelectionClicked);
	btn_ConfirmRolls->OnClicked.AddDynamic(this, &UCharacterEditor::ConfirmStatRolls);
	btn_StartOver->OnClicked.AddDynamic(this, &UCharacterEditor::StartOver);

	// stat point bindings
	btn_LowerStr->OnClicked.AddDynamic(this, &UCharacterEditor::LowerStr);
	btn_LowerAgi->OnClicked.AddDynamic(this, &UCharacterEditor::LowerAgi);
	btn_LowerInt->OnClicked.AddDynamic(this, &UCharacterEditor::LowerInt);
	btn_LowerWis->OnClicked.AddDynamic(this, &UCharacterEditor::LowerWis);
	btn_LowerCon->OnClicked.AddDynamic(this, &UCharacterEditor::LowerCon);
	btn_LowerCha->OnClicked.AddDynamic(this, &UCharacterEditor::LowerCha);
	btn_RaiseStr->OnClicked.AddDynamic(this, &UCharacterEditor::RaiseStr);
	btn_RaiseAgi->OnClicked.AddDynamic(this, &UCharacterEditor::RaiseAgi);
	btn_RaiseInt->OnClicked.AddDynamic(this, &UCharacterEditor::RaiseInt);
	btn_RaiseWis->OnClicked.AddDynamic(this, &UCharacterEditor::RaiseWis);
	btn_RaiseCon->OnClicked.AddDynamic(this, &UCharacterEditor::RaiseCon);
	btn_RaiseCha->OnClicked.AddDynamic(this, &UCharacterEditor::RaiseCha);

	return Successful;
}

void UCharacterEditor::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);
}

void UCharacterEditor::RaceChanged(FString sItem, ESelectInfo::Type selType)
{
	SelectedRace = (ERaces)cb_RaceSelect->GetSelectedIndex();
	PopulateDescWithRace();
}

void UCharacterEditor::ClassChanged(FString sItem, ESelectInfo::Type selType)
{
	SelectedClass = (EClasses)cb_ClassSelect->GetSelectedIndex();
	PopulateDescWithClass();
}

void UCharacterEditor::PopulateComboBoxes()
{
	cb_ClassSelect->ClearOptions();
	cb_RaceSelect->ClearOptions();

	for (int i = 1; i <= MAX_RACES; i++)
	{
		FRaceData* RaceDataLU = GetRaceData(i);
		cb_RaceSelect->AddOption(RaceDataLU->Race);
	}
	cb_RaceSelect->SetSelectedIndex(0);
	SelectedRace = ERaces::R_Human;

	for (int i = 1;i <= MAX_CLASSES; i++)
	{
		FClassData* ClassDataLU = GetClassData(i);
		cb_ClassSelect->AddOption(ClassDataLU->Class);
	}
	cb_ClassSelect->SetSelectedIndex(0);
	SelectedClass = EClasses::C_Warrior;
}

void UCharacterEditor::PopulateDescWithRace()
{
	TArray<int32> RaceAbilityKeys;
	TArray<float> RaceAbilityValues;
	TotalExpTable = 0.0f;
	StatSubsys = GetStatSubsys();
	if (!StatSubsys) return;
	AbilityBonusString = "";
	SelectedRace = (ERaces)cb_RaceSelect->GetSelectedIndex();
	FRaceData* TempRaceData = GetRaceData(SelectedRace);
	if (!TempRaceData) return;
	TempRaceData->RaceAbilities.GenerateKeyArray(RaceAbilityKeys);
	TempRaceData->RaceAbilities.GenerateValueArray(RaceAbilityValues);
	ExpTable = "Race Exp Table: +" + FString::FromInt(FMath::FloorToInt(TempRaceData->RaceExpMultiplier * 100)) + "%";
	SelectedRaceExpTable = TempRaceData->RaceExpMultiplier;
	for (int i = 0; i <= RaceAbilityKeys.Num() - 1; i++)
	{
		if (AbilityBonusString == "")
		{
			AbilityBonusString = StatSubsys->AbilityStrings[RaceAbilityKeys[i]] + ": " + FString::FromInt((int32)RaceAbilityValues[i]);
		}
		else
		{
			AbilityBonusString = AbilityBonusString + ", " + StatSubsys->AbilityStrings[RaceAbilityKeys[i]] + ": " + FString::FromInt((int32)RaceAbilityValues[i]);
		}
	}
	FText FormatForDesc = FText::FromString("{Description}\n\n{Bonuses}\n\n{ExpTable}");
	FText Description = FText::FormatNamed(FormatForDesc, TEXT("Description"), TempRaceData->RaceDescription, TEXT("Bonuses"), FText::FromString(AbilityBonusString), TEXT("ExpTable"), FText::FromString(ExpTable));
	txt_Description->SetText(Description);
}

void UCharacterEditor::PopulateDescWithClass()
{
	TArray<int32> ClassAbilityKeys;
	TArray<float> ClassAbilityValues;
	StatSubsys = GetStatSubsys();
	AbilityBonusString = "";
	SelectedClass = (EClasses)cb_ClassSelect->GetSelectedIndex();
	FClassData* TempClassData = GetClassData(SelectedClass);
	if (!TempClassData) return;
	TempClassData->ClassAbilities.GenerateKeyArray(ClassAbilityKeys);
	TempClassData->ClassAbilities.GenerateValueArray(ClassAbilityValues);
	SelectedClassExpTable = TempClassData->ClassExpMultiplier;
	TotalExpTable = SelectedClassExpTable + SelectedRaceExpTable;
	ExpTable = "Class Exp Table: +" + FString::FromInt(FMath::RoundToInt(SelectedClassExpTable * 100)) + "%" + 
		"\nTotal Exp Table: +" + FString::FromInt(FMath::RoundToInt(TotalExpTable * 100)) + "%";
	for (int i = 0; i <= ClassAbilityKeys.Num() - 1; i++)
	{
		if (AbilityBonusString == "")
		{
			AbilityBonusString = StatSubsys->AbilityStrings[ClassAbilityKeys[i]] + ": " + FString::FromInt((int32)ClassAbilityValues[i]);
		}
		else
		{
			AbilityBonusString = AbilityBonusString + ", " + StatSubsys->AbilityStrings[ClassAbilityKeys[i]] + ": " + FString::FromInt((int32)ClassAbilityValues[i]);
		}
	}
	FText FormatForDesc = FText::FromString("{Description}\n\n{Bonuses}\n\n{ExpTable}");
	FText Description = FText::FormatNamed(FormatForDesc, TEXT("Description"), TempClassData->ClassDescription, TEXT("Bonuses"), FText::FromString(AbilityBonusString), TEXT("ExpTable"), FText::FromString(ExpTable));
	txt_Description->SetText(Description);
}

void UCharacterEditor::AllowClassSelection()
{
	bRaceSelected = true;
	cb_RaceSelect->SetIsEnabled(false);
	cb_ClassSelect->SetIsEnabled(true);
	txt_Select->SetText(FText::FromString("Please Select A Class"));
	cb_ClassSelect->SetVisibility(ESlateVisibility::Visible);
	btn_ConfirmClass->SetVisibility(ESlateVisibility::Visible);
	btn_BackToRace->SetVisibility(ESlateVisibility::Visible);
	btn_ConfirmRace->SetVisibility(ESlateVisibility::Hidden);
}

void UCharacterEditor::BackToRaceSelection()
{
	bRaceSelected = false;
	cb_RaceSelect->SetIsEnabled(true);
	txt_Select->SetText(FText::FromString("Please Select A Race"));
	cb_ClassSelect->SetVisibility(ESlateVisibility::Hidden);
	cb_ClassSelect->SetIsEnabled(false);
	btn_ConfirmClass->SetVisibility(ESlateVisibility::Hidden);
	btn_BackToRace->SetVisibility(ESlateVisibility::Hidden);
	btn_ConfirmRace->SetVisibility(ESlateVisibility::Visible);
	PopulateComboBoxes();
}

void UCharacterEditor::AllowNameSelection()
{
	etb_NameEntry->SetVisibility(ESlateVisibility::Visible);
	btn_SelectedName->SetVisibility(ESlateVisibility::Visible);
	txt_EnterName->SetVisibility(ESlateVisibility::Visible);
	cb_ClassSelect->SetIsEnabled(false);
	cb_RaceSelect->SetIsEnabled(false);
	btn_ConfirmClass->SetVisibility(ESlateVisibility::Hidden);
	btn_BackToRace->SetVisibility(ESlateVisibility::Hidden);
	btn_StartOver->SetVisibility(ESlateVisibility::Visible);
}

void UCharacterEditor::NameSelectionClicked()
{
	NameSelected(etb_NameEntry->GetText(), ETextCommit::OnEnter);
}

void UCharacterEditor::NameSelected(const FText& InText, ETextCommit::Type CommitMethod)
{
	if (CheckIfNameIsValid(InText))
	{
		CharacterName = FName(*InText.ToString());
		StatSubsys = GetStatSubsys();
		CharacterData = StatSubsys->CreateCharacterData(SelectedRace, SelectedClass, CharacterName, SelectedCharacter);
		RaceData = *GetRaceData(SelectedRace);
		ClassData = *GetClassData(SelectedClass);
		CP_CreationPanel->SetVisibility(ESlateVisibility::Hidden);
		cp_CharEdit->SetVisibility(ESlateVisibility::Visible);
		PopulateEditorData();
	}
	else
	{
		txt_EnterName->SetText(FText::FromString("You must enter a valid name!"));
	}
}

bool UCharacterEditor::CheckIfNameIsValid(FText SelectedName)
{
	FString NameEntered = SelectedName.ToString();
	if (NameEntered == "" || NameEntered == "Empty" || NameEntered == "None")
	{
		return false;
	}
	return true;
}

void UCharacterEditor::PopulateEditorData()
{
	StatSubsys = GetStatSubsys();
	FRaceData raceData = StatSubsys->GetRaceData(SelectedRace);
	if (CharacterData.CharacterName == "" || CharacterData.CharacterName == "Empty" || CharacterData.CharacterName == "None")
	{
		CharacterData = StatSubsys->GetCharacterData(SelectedCharacter);
	}
	// Set top data
	FString RaceString = RaceEnumToString(SelectedRace);
	FString ClassString = ClassEnumAsString(SelectedClass);
	txt_EditorName->SetText(FText::FromName(CharacterName));
	txt_EditorRace->SetText(FText::FromString(RaceString));
	txt_EditorClass->SetText(FText::FromString(ClassString));
	txt_RemainingCP->SetText(FText::FromString(FString::FromInt(CharacterData.CharacterBaseStats.CP)));

	// Set Ability Score Ranges
	txt_StrRange->SetText(MakeStatRangeString(raceData.RaceStrMin, raceData.RaceStrMax));
	txt_AgiRange->SetText(MakeStatRangeString(raceData.RaceAgiMin, raceData.RaceAgiMax));
	txt_WisRange->SetText(MakeStatRangeString(raceData.RaceWisMin, raceData.RaceWisMax));
	txt_IntRange->SetText(MakeStatRangeString(raceData.RaceIntMin, raceData.RaceIntMax));
	txt_ConRange->SetText(MakeStatRangeString(raceData.RaceConMin, raceData.RaceConMax));
	txt_ChaRange->SetText(MakeStatRangeString(raceData.RaceChaMin, raceData.RaceChaMax));

	// Set Ability Score Current Values
	txt_StrCurrent->SetText(FText::FromString(FString::FromInt(CharacterData.CharacterBaseStats.Strength)));
	txt_AgiCurrent->SetText(FText::FromString(FString::FromInt(CharacterData.CharacterBaseStats.Agility)));
	txt_WisCurrent->SetText(FText::FromString(FString::FromInt(CharacterData.CharacterBaseStats.Wisdom)));
	txt_IntCurrent->SetText(FText::FromString(FString::FromInt(CharacterData.CharacterBaseStats.Intellect)));
	txt_ConCurrent->SetText(FText::FromString(FString::FromInt(CharacterData.CharacterBaseStats.Constitution)));
	txt_ChaCurrent->SetText(FText::FromString(FString::FromInt(CharacterData.CharacterBaseStats.Charm)));
}

FText UCharacterEditor::MakeStatRangeString(int32 Min, int32 Max)
{
	FString StatMin = FString::FromInt(Min);
	FString StatMax = FString::FromInt(Max);
	FString StatRange = StatMin + " to " + StatMax;
	return FText::FromString(StatRange);
}

FRaceData* UCharacterEditor::GetRaceData(ERaces RaceToGet)
{
	FString ContextString = "";
	FString LookupString = FString::FromInt((int32)SelectedRace + 1);
	UDataTable* RaceDataTable = Cast<UDataTable>(StaticLoadObject(UDataTable::StaticClass(), NULL, TEXT("/Game/GameLogic/Data/DT_RaceData.DT_RaceData")));
	FRaceData* RaceDataLU = RaceDataTable->FindRow<FRaceData>(FName(*LookupString), ContextString, false);
	return RaceDataLU;
}

FRaceData* UCharacterEditor::GetRaceData(int RaceIndex)
{

	FString ContextString = "";
	FString LookupString = FString::FromInt(RaceIndex);
	UDataTable* RaceDataTable = Cast<UDataTable>(StaticLoadObject(UDataTable::StaticClass(), NULL, TEXT("/Game/GameLogic/Data/DT_RaceData.DT_RaceData")));
	FRaceData* RaceDataLU = RaceDataTable->FindRow<FRaceData>(FName(*LookupString), ContextString, false);
	return RaceDataLU;
}

FClassData* UCharacterEditor::GetClassData(EClasses ClassToGet)
{
	FString ContextString = "";
	FString LookupString = FString::FromInt((int32)SelectedClass + 1);
	UDataTable* ClassDataTable = Cast<UDataTable>(StaticLoadObject(UDataTable::StaticClass(), NULL, TEXT("/Game/GameLogic/Data/DT_ClassData.DT_ClassData")));
	FClassData* ClassDataLU = ClassDataTable->FindRow<FClassData>(FName(*LookupString), ContextString, false);
	return ClassDataLU;
}

FClassData* UCharacterEditor::GetClassData(int ClassIndex)
{
	FString ContextString = "";
	FString LookupString = FString::FromInt(ClassIndex);
	UDataTable* ClassDataTable = Cast<UDataTable>(StaticLoadObject(UDataTable::StaticClass(), NULL, TEXT("/Game/GameLogic/Data/DT_ClassData.DT_ClassData")));
	FClassData* ClassDataLU = ClassDataTable->FindRow<FClassData>(FName(*LookupString), ContextString, false);
	return ClassDataLU;
}

UStatSubsystem* UCharacterEditor::GetStatSubsys()
{
	UGameInstance* GameInstance = Cast<UGameInstance>(GetGameInstance());
	if (!GameInstance) return nullptr;
	return Cast<UStatSubsystem>(GameInstance->GetSubsystem<UStatSubsystem>());
	
}

FString UCharacterEditor::RaceEnumToString(ERaces RaceToConvert)
{
	switch (RaceToConvert)
	{
	case ERaces::R_Human:
		return "Human";
		break;
	case ERaces::R_Dwarf:
		return "Dwarf";
		break;
	case ERaces::R_Gnome:
		return "Gnome";
		break;
	case ERaces::R_Halfling:
		return "Halfling";
		break;
	case ERaces::R_Elf:
		return "Elf";
		break;
	case ERaces::R_HalfElf:
		return "Half-Elf";
		break;
	case ERaces::R_DarkElf:
		return "Dark-Elf";
		break;
	case ERaces::R_HalfOrc:
		return "Half-Orc";
		break;
	case ERaces::R_Goblin:
		return "Goblin";
		break;
	case ERaces::R_HalfOgre:
		return "Half-Ogre";
		break;
	case ERaces::R_Kang:
		return "Kang";
		break;
	case ERaces::R_Nekojin:
		return "Nekojin";
		break;
	case ERaces::R_GauntOne:
		return "Gaunt One";
		break;
	}
	return "Invalid Race";
}

FString UCharacterEditor::ClassEnumAsString(EClasses ClassToGet)
{
	switch (ClassToGet)
	{
	case EClasses::C_Warrior:
		return "Warrior";
		break;
	case EClasses::C_Witchunter:
		return "Witchunter";
		break;
	case EClasses::C_Paladin:
		return "Paladin";
		break;
	case EClasses::C_Cleric:
		return "Cleric";
		break;
	case EClasses::C_Priest:
		return "Priest";
		break;
	case EClasses::C_Missionary:
		return "Missionary";
		break;
	case EClasses::C_Ninja:
		return "Ninja";
		break;
	case EClasses::C_Rogue:
		return "Rogue";
		break;
	case EClasses::C_Bard:
		return "Bard";
		break;
	case EClasses::C_Gypsy:
		return "Gypsy";
		break;
	case EClasses::C_Warlock:
		return "Warlock";
		break;
	case EClasses::C_Mage:
		return "Mage";
		break;
	case EClasses::C_Druid:
		return "Druid";
		break;
	case EClasses::C_Ranger:
		return "Ranger";
		break;
	case EClasses::C_Monk:
		return "Monk";
		break;
	}
	return "Invalid Class";
}

void UCharacterEditor::LowerStr()
{
	StatSubsys = GetStatSubsys();
	int32 NewStat;
	int32 NewCP;
	StatSubsys->LowerStatPoint(RaceData.RaceStrMin, RaceData.RaceStrMax, CharacterData.CharacterBaseStats.Strength, CharacterData.CharacterBaseStats.CP, NewStat, NewCP);
	CharacterData.CharacterBaseStats.Strength = NewStat;
	CharacterData.CharacterBaseStats.CP = NewCP;
	
	PopulateEditorData();
}

void UCharacterEditor::LowerAgi()
{
	StatSubsys = GetStatSubsys();
	int32 NewStat;
	int32 NewCP;
	StatSubsys->LowerStatPoint(RaceData.RaceAgiMin, RaceData.RaceAgiMax, CharacterData.CharacterBaseStats.Agility, CharacterData.CharacterBaseStats.CP, NewStat, NewCP);
	CharacterData.CharacterBaseStats.Agility = NewStat;
	CharacterData.CharacterBaseStats.CP = NewCP;

	PopulateEditorData();
}

void UCharacterEditor::LowerInt()
{
	StatSubsys = GetStatSubsys();
	int32 NewStat;
	int32 NewCP;
	StatSubsys->LowerStatPoint(RaceData.RaceIntMin, RaceData.RaceIntMax, CharacterData.CharacterBaseStats.Intellect, CharacterData.CharacterBaseStats.CP, NewStat, NewCP);
	CharacterData.CharacterBaseStats.Intellect = NewStat;
	CharacterData.CharacterBaseStats.CP = NewCP;

	PopulateEditorData();
}

void UCharacterEditor::LowerWis()
{
	StatSubsys = GetStatSubsys();
	int32 NewStat;
	int32 NewCP;
	StatSubsys->LowerStatPoint(RaceData.RaceWisMin, RaceData.RaceWisMax, CharacterData.CharacterBaseStats.Wisdom, CharacterData.CharacterBaseStats.CP, NewStat, NewCP);
	CharacterData.CharacterBaseStats.Wisdom = NewStat;
	CharacterData.CharacterBaseStats.CP = NewCP;

	PopulateEditorData();
}
void UCharacterEditor::LowerCon()
{
	StatSubsys = GetStatSubsys();
	int32 NewStat;
	int32 NewCP;
	StatSubsys->LowerStatPoint(RaceData.RaceConMin, RaceData.RaceConMax, CharacterData.CharacterBaseStats.Constitution, CharacterData.CharacterBaseStats.CP, NewStat, NewCP);
	CharacterData.CharacterBaseStats.Constitution = NewStat;
	CharacterData.CharacterBaseStats.CP = NewCP;

	PopulateEditorData();
}

void UCharacterEditor::LowerCha()
{
	StatSubsys = GetStatSubsys();
	int32 NewStat;
	int32 NewCP;
	StatSubsys->LowerStatPoint(RaceData.RaceChaMin, RaceData.RaceChaMax, CharacterData.CharacterBaseStats.Charm, CharacterData.CharacterBaseStats.CP, NewStat, NewCP);
	CharacterData.CharacterBaseStats.Charm = NewStat;
	CharacterData.CharacterBaseStats.CP = NewCP;

	PopulateEditorData();
}

void UCharacterEditor::RaiseStr()
{
	StatSubsys = GetStatSubsys();
	int32 NewStat;
	int32 NewCP;
	StatSubsys->RaiseStatPoint(RaceData.RaceStrMin, RaceData.RaceStrMax, CharacterData.CharacterBaseStats.Strength, CharacterData.CharacterBaseStats.CP, NewStat, NewCP);
	CharacterData.CharacterBaseStats.Strength = NewStat;
	CharacterData.CharacterBaseStats.CP = NewCP;
	PopulateEditorData();
}

void UCharacterEditor::RaiseAgi()
{
	StatSubsys = GetStatSubsys();
	int32 NewStat;
	int32 NewCP;
	StatSubsys->RaiseStatPoint(RaceData.RaceAgiMin, RaceData.RaceAgiMax, CharacterData.CharacterBaseStats.Agility, CharacterData.CharacterBaseStats.CP, NewStat, NewCP);
	CharacterData.CharacterBaseStats.Agility = NewStat;
	CharacterData.CharacterBaseStats.CP = NewCP;
	PopulateEditorData();
}

void UCharacterEditor::RaiseInt()
{
	StatSubsys = GetStatSubsys();
	int32 NewStat;
	int32 NewCP;
	StatSubsys->RaiseStatPoint(RaceData.RaceIntMin, RaceData.RaceIntMax, CharacterData.CharacterBaseStats.Intellect, CharacterData.CharacterBaseStats.CP, NewStat, NewCP);
	CharacterData.CharacterBaseStats.Intellect = NewStat;
	CharacterData.CharacterBaseStats.CP = NewCP;
	PopulateEditorData();
}

void UCharacterEditor::RaiseWis()
{
	StatSubsys = GetStatSubsys();
	int32 NewStat;
	int32 NewCP;
	StatSubsys->RaiseStatPoint(RaceData.RaceWisMin, RaceData.RaceWisMax, CharacterData.CharacterBaseStats.Wisdom, CharacterData.CharacterBaseStats.CP, NewStat, NewCP);
	CharacterData.CharacterBaseStats.Wisdom = NewStat;
	CharacterData.CharacterBaseStats.CP = NewCP;
	PopulateEditorData();
}

void UCharacterEditor::RaiseCon()
{
	StatSubsys = GetStatSubsys();
	int32 NewStat;
	int32 NewCP;
	StatSubsys->RaiseStatPoint(RaceData.RaceConMin, RaceData.RaceConMax, CharacterData.CharacterBaseStats.Constitution, CharacterData.CharacterBaseStats.CP, NewStat, NewCP);
	CharacterData.CharacterBaseStats.Constitution = NewStat;
	CharacterData.CharacterBaseStats.CP = NewCP;
	PopulateEditorData();
}

void UCharacterEditor::RaiseCha()
{
	StatSubsys = GetStatSubsys();
	int32 NewStat;
	int32 NewCP;
	StatSubsys->RaiseStatPoint(RaceData.RaceChaMin, RaceData.RaceChaMax, CharacterData.CharacterBaseStats.Charm, CharacterData.CharacterBaseStats.CP, NewStat, NewCP);
	CharacterData.CharacterBaseStats.Charm = NewStat;
	CharacterData.CharacterBaseStats.CP = NewCP;
	PopulateEditorData();
}

void UCharacterEditor::ConfirmStatRolls()
{
	StatSubsys = GetStatSubsys();
	if (bConfirmedOnce)
	{
		RollFinalStats();
		CreateSaveDataBP(CharacterData, SelectedCharacter);
		UWorld* World = GetWorld();
		UGameplayStatics::OpenLevel(World, FName("TestMap"), true);
	}
	else
	{
		bConfirmedOnce = true;
		if (CharacterData.CharacterBaseStats.CP > 0)
		{
			txt_UnusedCPConfirm->SetVisibility(ESlateVisibility::Visible);
			txt_UnusedCPWarning->SetVisibility(ESlateVisibility::Visible);
		}
		else
		{
			txt_UnusedCPConfirm->SetVisibility(ESlateVisibility::Visible);
		}
	}
}

void UCharacterEditor::RollFinalStats()
{
	StatSubsys = GetStatSubsys();
	StatSubsys->RollFinalStats(CharacterData, SelectedCharacter, true);
}

void UCharacterEditor::StartOver()
{
	txt_EnterName->SetVisibility(ESlateVisibility::Hidden);
	etb_NameEntry->SetVisibility(ESlateVisibility::Hidden);
	btn_ConfirmRolls->SetVisibility(ESlateVisibility::Hidden);
	btn_StartOver->SetVisibility(ESlateVisibility::Hidden);
	cb_RaceSelect->SetIsEnabled(true);
	cb_ClassSelect->SetIsEnabled(false);
	cb_ClassSelect->SetVisibility(ESlateVisibility::Hidden);
	btn_SelectedName->SetVisibility(ESlateVisibility::Hidden);
	btn_ConfirmRace->SetVisibility(ESlateVisibility::Visible);
}
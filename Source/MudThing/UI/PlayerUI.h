// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "GlobalData.h"
#include "Engine/EngineTypes.h"
#include "PlayerUI.generated.h"

class UButton;
class UStatSubsystem;
class AMainPlayerController;

UCLASS()
class MUDTHING_API UPlayerUI : public UUserWidget
{
	GENERATED_BODY()

protected:
	virtual bool Initialize();

	UStatSubsystem* GetStatSubsys();

	AMainPlayerController* GetMainPC();

	UFUNCTION(BlueprintCallable)
	EDirection GetFacingDirection();

	class AMainPlayer* MainPlayer;

	UPROPERTY(meta = (BindWidget))
	UButton* btnBackwards;

	UPROPERTY(meta = (BindWidget))
	UButton* btnForward;

	UPROPERTY(meta = (BindWidget))
	UButton* btnLeft;

	UPROPERTY(meta = (BindWidget))
	UButton* btnRight;

	UPROPERTY(meta = (BindWidget))
	UButton* btnLeft45;

	UPROPERTY(meta = (BindWidget))
	UButton* btnRight45;

	UPROPERTY(meta = (BindWidget), BlueprintReadOnly)
	UButton* btn_Commands;

	UPROPERTY(meta = (BindWidget))
	UButton* btn_Inventory;

	UPROPERTY(meta = (BindWidget))
	UButton* btn_Rest;

	UPROPERTY(meta = (BindWidget))
	UButton* btn_Search;

	UPROPERTY(meta = (BindWidget), BlueprintReadOnly)
	class UUniformGridPanel* ugp_CommandPanel;

	UFUNCTION()
	void MoveForwardPressed();

	UFUNCTION()
	void TurnAroundPressed();

	UFUNCTION()
	void TurnLeftPressed();

	UFUNCTION()
	void TurnRightPressed();

	UFUNCTION()
	void Left45Pressed();

	UFUNCTION()
	void Right45Pressed();

	UFUNCTION()
	void CommandsClicked();

	UFUNCTION()
	void InventoryClicked();
	UFUNCTION()
	void RestClicked();

	UFUNCTION()
	void FireRestTick(int32 PlayerIndex);

private:
	UStatSubsystem* StatSubsys;

	AMainPlayerController* MainPC;

	FTimerHandle RestTimerHandle;
	
	int32 RestTickDelay = 4;


};

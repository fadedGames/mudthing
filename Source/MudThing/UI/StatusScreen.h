// Copyright 2019, fadedGames - http://fadedGames.net

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "GlobalData.h"
#include "StatusScreen.generated.h"

class UTextBlock;
class UButton;
class UScrollBox;

UCLASS()
class MUDTHING_API UStatusScreen : public UUserWidget
{
	GENERATED_BODY()
	
public:
	virtual bool Initialize();

protected:

	UPROPERTY()
	FCharacterData CharData;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	ESelectedCharacter SelectedCharacter;

	/// Basic Stats
	UPROPERTY(meta = (BindWidget))
	UTextBlock* txt_AC;			 
								 
	UPROPERTY(meta = (BindWidget))
	UTextBlock* txt_Class;		 
	
	UPROPERTY(meta = (BindWidget))
	UTextBlock* txt_Race;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* txt_Exp;		 
								 
	UPROPERTY(meta = (BindWidget))
	UTextBlock* txt_HP;			 
								 
	UPROPERTY(meta = (BindWidget))
	UTextBlock* txt_Level;		 
								 
	UPROPERTY(meta = (BindWidget))
	UTextBlock* txt_Mana;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* txt_MAText;
								 
	UPROPERTY(meta = (BindWidget))
	UTextBlock* txt_Name;		 

	UPROPERTY(meta = (BindWidget))
	UTextBlock* tct_CP;
								 
	/// Stat Scores				 
	UPROPERTY(meta = (BindWidget))
	UTextBlock* txt_Agi;		 
								 
	UPROPERTY(meta = (BindWidget))
	UTextBlock* txt_Cha;		 
								 
	UPROPERTY(meta = (BindWidget))
	UTextBlock* txt_Con;		 
								 
	UPROPERTY(meta = (BindWidget))
	UTextBlock* txt_Int;		 
								 
	UPROPERTY(meta = (BindWidget))
	UTextBlock* txt_Str;		 
								 
	UPROPERTY(meta = (BindWidget))
	UTextBlock* txt_Wis;		 
								 
	/// Secondary Stats			 
	UPROPERTY(meta = (BindWidget))
	UTextBlock* txt_MA;			 
								 
	UPROPERTY(meta = (BindWidget))
	UTextBlock* txt_MR;			 
								 
	UPROPERTY(meta = (BindWidget))
	UTextBlock* txt_Perception;	 
								 
	UPROPERTY(meta = (BindWidget))
	UTextBlock* txt_Picks;		 
								
	UPROPERTY(meta = (BindWidget))
	UTextBlock* txt_Stealth;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* txt_Traps;

	/// Combat Stats
	UPROPERTY(meta = (BindWidget))
	UTextBlock* txt_Spellcasting;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* txt_Spellcasting_desc;

	UPROPERTY(meta = (BindWidget))
	UButton* btn_CloseStats;

	UPROPERTY(meta = (BindWidget))
	UScrollBox* sb_Inventory;

	UFUNCTION(BlueprintCallable)
	void GetBasicStats(ESelectedCharacter SelectedChar);
	
	UFUNCTION(BlueprintCallable)
	void GetSecondaries(ESelectedCharacter SelectedChar);

	UFUNCTION(BlueprintCallable)
	void GetCombatStats(ESelectedCharacter SelectedChar);

	UFUNCTION(BlueprintCallable)
	void PopulateItemListForChar(ESelectedCharacter SelectedChar);

private:
	class UStatSubsystem* StatSubsys;

	class UInventorySubsystem* InventorySubsys;

	UStatSubsystem* GetStatSubsys();

	UInventorySubsystem* GetInventorySubsys();

	TSubclassOf<UUserWidget> ItemWidget;

};

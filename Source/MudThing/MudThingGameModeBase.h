// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MudThingGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class MUDTHING_API AMudThingGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};

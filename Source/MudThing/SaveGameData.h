// Copyright 2019, fadedGames - http://fadedGames.net

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "GlobalData.h"
#include "SaveGameData.generated.h"

/**
 * 
 */
UCLASS()
class MUDTHING_API USaveGameData : public USaveGame
{
	GENERATED_BODY()
	
public:

	USaveGameData();

	// Stat Component Data
	UPROPERTY(VisibleAnywhere, Category = "StatData")
	FCharacterData Char0Data;

	UPROPERTY(VisibleAnywhere, Category = "StatData")
	FCharacterData Char1Data;

	UPROPERTY(VisibleAnywhere, Category = "StatData")
	FCharacterData Char2Data;

	UPROPERTY(VisibleAnywhere, Category = "StatData")
	TMap<int32, float> Char0AbilsFromStats;

	UPROPERTY(VisibleAnywhere, Category = "StatData")
	TMap<int32, float> Char1AbilsFromStats;

	UPROPERTY(VisibleAnywhere, Category = "StatData")
	TMap<int32, float> Char2AbilsFromStats;

	UPROPERTY(VisibleAnywhere, Category = "StatData")
	TMap<int32, float> Char0AbilsFromSpells;

	UPROPERTY(VisibleAnywhere, Category = "StatData")
	TMap<int32, float> Char1AbilsFromSpells;

	UPROPERTY(VisibleAnywhere, Category = "StatData")
	TMap<int32, float> Char2AbilsFromSpells;

	UPROPERTY(VisibleAnywhere, Category = "StatData")
	TArray<FStatusEffect> Char0Debuffs;

	UPROPERTY(VisibleAnywhere, Category = "StatData")
	TArray<FStatusEffect> Char1Debuffs;

	UPROPERTY(VisibleAnywhere, Category = "StatData")
	TArray<FStatusEffect> Char2Debuffs;

	UPROPERTY(VisibleAnywhere, Category = "StatData")
	int32 NumberOfPlayersInParty;

	// Inventory Data
	UPROPERTY(VisibleAnywhere, Category = "InventoryData")
	TArray<FItem> Char0Inventory;

	UPROPERTY(VisibleAnywhere, Category = "InventoryData")
	TArray<FItem> Char1Inventory;

	UPROPERTY(VisibleAnywhere, Category = "InventoryData")
	TArray<FItem> Char2Inventory;

	UPROPERTY(VisibleAnywhere, Category = "InventoryData")
	TMap<EEquipmentSlot, FItem> Char0EquippedItems;

	UPROPERTY(VisibleAnywhere, Category = "InventoryData")
	TMap<EEquipmentSlot, FItem> Char1EquippedItems;

	UPROPERTY(VisibleAnywhere, Category = "InventoryData")
	TMap<EEquipmentSlot, FItem> Char2EquippedItems;
};

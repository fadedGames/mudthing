// Fill out your copyright notice in the Description page of Project Settings.


#include "RoomPoint.h"
#include "Components/SphereComponent.h"
#include "Player/MainPlayerController.h"
#include "Player/MainPlayer.h"

// Sets default values
ARoomPoint::ARoomPoint()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	
	SphereComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	RootComponent = SphereComp;
	SphereComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	SphereComp->SetCollisionObjectType(ECC_Vehicle);

	// TODO remove this magic number
	ChanceForCombat = 20;
}

void ARoomPoint::SetCastRefs()
{
	auto world = GetWorld();
	if (world)
	{
		MPC = Cast<AMainPlayerController>(GetWorld()->GetFirstPlayerController());
		if (MPC)
		{
			// set room point reference in main player for ground items 
			MainPlayer = Cast<AMainPlayer>(MPC->GetPawn());
		}
	}
}

void ARoomPoint::BeginPlay()
{
	Super::BeginPlay();
	SetCastRefs();
	SphereComp->OnComponentBeginOverlap.AddDynamic(this, &ARoomPoint::CombatCheck);
	SphereComp->OnComponentEndOverlap.AddDynamic(this, &ARoomPoint::HidePickupUI);
}

TArray<int32> ARoomPoint::RandomizeMobs()
{
	uint8 randomIndex = 0;
	TArray<int32> completedGroup;
	// always spawn at least 1
	randomIndex = FMath::RandRange(1, MobsAllowedToSpawn.Num());
	completedGroup.Add(randomIndex);
	for (uint8 i = 0; i < MaxNumberOfMobs - 1; i++)
	{
		bool shouldSpawn = (bool)FMath::RandRange(0, 1);
		if (shouldSpawn)
		{
			randomIndex = FMath::RandRange(1, MobsAllowedToSpawn.Num());
			completedGroup.Add(randomIndex);
		}
	}
	return completedGroup;
}

void ARoomPoint::CombatCheck(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	CheckForItemsInRoom();
	int32 combatRoll = FMath::RandRange(1, 100);

	MainPlayer->CurrentRoomPoint = this;

	// check for combat
	if (combatRoll <= ChanceForCombat)
	{
		MPC->ToggleCombat(RandomizeMobs());
	}
}


void ARoomPoint::AddToDroppedItems(FItem DroppedItem)
{
	ItemsOnGround.Add(DroppedItem.ItemNumber);
}

void ARoomPoint::CheckForItemsInRoom()
{
	if (ItemsOnGround.Num() > 0)	
	{
		ReportItemsInRoom();
	}
	else
	{
		MainPlayer->HideItemsOnGround();
	}
}

void ARoomPoint::HidePickupUI(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	MainPlayer->HideItemsOnGround();
}


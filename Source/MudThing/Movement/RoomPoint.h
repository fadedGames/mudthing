// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GlobalData.h"
#include "RoomPoint.generated.h"

UCLASS()
class MUDTHING_API ARoomPoint : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ARoomPoint();

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	class USphereComponent* SphereComp;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int32 ChanceForCombat;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "!IMPORTANTDATA")
	TArray<int32> EnemyGroup;

	UFUNCTION(BlueprintCallable)
	void AddToDroppedItems(FItem DroppedItem);

	UFUNCTION(BlueprintImplementableEvent)
	void ReportItemsInRoom();

protected:
	UFUNCTION()
	void CombatCheck(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TArray<int32> ItemsOnGround;

	UFUNCTION(BlueprintCallable)
	void CheckForItemsInRoom();

	UFUNCTION()
	void HidePickupUI(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION()
	void SetCastRefs();

	UPROPERTY()
	class AMainPlayer* MainPlayer;

	UPROPERTY()
	class AMainPlayerController* MPC;

	virtual void BeginPlay() override;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TArray<uint8> MobsAllowedToSpawn;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	uint8 MaxNumberOfMobs;

	UFUNCTION()
	TArray<int32> RandomizeMobs();
};

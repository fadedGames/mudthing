// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "GlobalData.generated.h"

// Constants
const int32 MAX_RACES = 13;
const int32 MAX_CLASSES = 15;

UENUM(BlueprintType)
enum class EClasses : uint8
{
	C_Warrior		UMETA(DisplayName = "Warrior"),		  //1
	C_Witchunter	UMETA(DisplayName = "Witchunter"),	  //2
	C_Paladin		UMETA(DisplayName = "Paladin"),		  //3
	C_Cleric		UMETA(DisplayName = "Cleric"),		  //4
	C_Priest		UMETA(DisplayName = "Priest"),		  //5
	C_Missionary	UMETA(DisplayName = "Missionary"),	  //6
	C_Ninja			UMETA(DisplayName = "Ninja"),		  //7
	C_Rogue			UMETA(DisplayName = "Rogue"),		  //8
	C_Bard			UMETA(DisplayName = "Bard"),		  //9
	C_Gypsy			UMETA(DisplayName = "Gypsy"),		  //10
	C_Warlock		UMETA(DisplayName = "Warlock"),		  //11
	C_Mage			UMETA(DisplayName = "Mage"),		  //12
	C_Druid			UMETA(DisplayName = "Druid"),		  //13
	C_Ranger		UMETA(DisplayName = "Ranger"),		  //14
	C_Monk			UMETA(DisplayName = "Monk")			  //15
};

UENUM(BlueprintType)
enum class ERaces : uint8
{
	R_Human			UMETA(DisplayName = "Human"),		  //1
	R_Dwarf			UMETA(DisplayName = "Dwarf"),		  //2
	R_Gnome			UMETA(DisplayName = "Gnome"),		  //3
	R_Halfling		UMETA(DisplayName = "Halfling"),	  //4
	R_Elf			UMETA(DisplayName = "Elf"),			  //5
	R_HalfElf		UMETA(DisplayName = "Half-Elf"),	  //6
	R_DarkElf		UMETA(DisplayName = "Dark-Elf"),	  //7
	R_HalfOrc		UMETA(DisplayName = "Half-Orc"),	  //8
	R_Goblin		UMETA(DisplayName = "Goblin"),		  //9
	R_HalfOgre		UMETA(DisplayName = "Half-Ogre"),	  //10
	R_Kang			UMETA(DisplayName = "Kang"),		  //11
	R_Nekojin		UMETA(DisplayName = "Nekojin"),		  //12
	R_GauntOne		UMETA(DisplayName = "Gaunt One")	  //13
};

UENUM(BlueprintType)
enum class EAbilMagicClass : uint8
{
	AMC_Arcane		UMETA(DisplayName = "Arcane"),
	AMC_Holy		UMETA(DisplayName = "Holy"),
	AMC_Nature		UMETA(DisplayName = "Nature"),
	AMC_Lyrical		UMETA(DisplayName = "Lyrical"),
	AMC_Ki			UMETA(DisplayName = "Ki"),
	AMC_None		UMETA(DisplayName = "None")
};

UENUM(BlueprintType)
enum class EAbilPhysClass : uint8
{
	APC_Might		UMETA(DisplayName = "Might"),
	APC_Dexterity	UMETA(DisplayName = "Dexterity"),
	APC_Ki			UMETA(DisplayName = "Ki"),
	APC_None		UMETA(DisplayName = "None")
};

UENUM(BlueprintType)
enum class EElementType : uint8
{
	ET_Normal		UMETA(DisplayName = "Normal"),
	ET_Fire			UMETA(DisplayName = "Fire"),
	ET_Cold			UMETA(DisplayName = "Cold"),
	ET_Poison		UMETA(DisplayName = "Poison"),
	ET_Stone		UMETA(DisplayName = "Stone"),
	ET_Lightning	UMETA(DisplayName = "Lightning"),
	ET_Physical		UMETA(DisplayName = "Physical")
};

UENUM(BlueprintType)
enum class ETargetType : uint8
{
	TT_Self			UMETA(DisplayName = "Self"),
	TT_Friendly		UMETA(DisplayName = "Self or Friendly"),
	TT_Any			UMETA(DisplayName = "Any"),
	TT_Enemy		UMETA(DisplayName = "Enemy"),
	TT_SplitRoom	UMETA(DisplayName = "Room-Split"),
	TT_SplitParty	UMETA(DisplayName = "Party-Split"),
	TT_FullRoom		UMETA(DisplayName = "Room-Full"),
	TT_FullParty	UMETA(DisplayName = "Party-Full"),
	TT_Item			UMETA(DisplayName = "Item")
};

UENUM(BlueprintType)
enum class EAbilApplication : uint8
{
	AA_Direct		UMETA(DisplayName = "Direct"),
	AA_OverTime		UMETA(DisplayName = "OverTime"),
	AA_ApplyEffect	UMETA(DisplayName = "ApplyEffect"),
	AA_RemoveEffect	UMETA(DisplayName = "RemoveEffect")
};

UENUM(BlueprintType)
enum class EAbilEffectType : uint8
{
	AET_Damage		UMETA(DisplayName = "Damage"),
	AET_Heal		UMETA(DisplayName = "Heal"),
	AET_Buff		UMETA(DisplayName = "Buff"),
	AET_Debuff		UMETA(DisplayName = "Debuff")
};

UENUM(BlueprintType)
enum class EEquipmentSlot : uint8
{
	ES_Head			UMETA(DisplayName = "Head"),		//0
	ES_Ears			UMETA(DisplayName = "Ears"),		//1
	ES_Eyes			UMETA(DisplayName = "Eyes"),		//2
	ES_Face			UMETA(DisplayName = "Face"),		//3
	ES_Neck			UMETA(DisplayName = "Neck"),		//4
	ES_Back			UMETA(DisplayName = "Back"),		//5
	ES_Torso		UMETA(DisplayName = "Torso"),		//6
	ES_Arms			UMETA(DisplayName = "Arms"),		//7
	ES_Wrist		UMETA(DisplayName = "Wrist"),		//8
	ES_Wrist2		UMETA(DisplayName = "Wrist2"),		//9
	ES_Hands		UMETA(DisplayName = "Hands"),		//10
	ES_Finger		UMETA(DisplayName = "Finger"),		//11
	ES_Finger2		UMETA(DisplayName = "Finger2"),		//12
	ES_Waist		UMETA(DisplayName = "Waist"),		//13
	ES_Legs			UMETA(DisplayName = "Legs"),		//14
	ES_Feet			UMETA(DisplayName = "Feet"),		//15
	ES_Worn			UMETA(DisplayName = "Worn"),		//16
	ES_Offhand		UMETA(DisplayName = "Offhand"),		//17
	ES_Weapon		UMETA(DisplayName = "Weapon"),		//18
	ES_None			UMETA(DisplayName = "None")			//19
};

UENUM(BlueprintType)
enum class EWeaponType : uint8
{
	WT_1H_Sharp	UMETA(DisplayName = "1-Handed Sharp"),
	WT_2H_Sharp	UMETA(DisplayName = "2-Handed Sharp"),
	WT_1H_Blunt	UMETA(DisplayName = "1-Handed Blunt"),
	WT_2H_Blunt	UMETA(DisplayName = "2-Handed Blunt"),
	WT_Staff	UMETA(DisplayName = "Staff"),
	WT_None		UMETA(DisplayName = "None")

};

UENUM(BlueprintType)
enum class EArmorType : uint8
{
	AT_Natural	UMETA(DisplayName = "Natural"),
	AT_Cloth	UMETA(DisplayName = "Silk"),
	AT_Ninja	UMETA(DisplayName = "Ninja"),
	AT_Leather	UMETA(DisplayName = "Leather"),
	AT_Chain	UMETA(DisplayName = "Chain"),
	AT_Scale	UMETA(DisplayName = "Scale"),
	AT_Plate	UMETA(DisplayName = "Plate"),
	AT_None		UMETA(DisplayName = "None")
};

UENUM(BlueprintType)
enum class EItemType : uint8
{
	IT_Armor	UMETA(DisplayName = "Armor"),
	IT_Weapon	UMETA(DisplayName = "Weapon"),
	IT_Consume	UMETA(DisplayName = "Consumable"),
	IT_Key		UMETA(DisplayName = "Key"),
	IT_Quest	UMETA(DisplayName = "Quest/Special"),
	IT_Scroll	UMETA(DisplayName = "Scroll")
};

UENUM(BlueprintType)
enum class EDirection : uint8
{
	D_North		UMETA(DisplayName = "North"),
	D_Northeast	UMETA(DisplayName = "Northeast"),
	D_East		UMETA(DisplayName = "East"),
	D_Southeast	UMETA(DisplayName = "Southeast"),
	D_South		UMETA(DisplayName = "South"),
	D_Southwest	UMETA(DisplayName = "Southwest"),
	D_West		UMETA(DisplayName = "West"),
	D_Northwest	UMETA(DisplayName = "Northwest")
};

UENUM(BlueprintType)
enum class EAttackType : uint8
{
	AT_Physical,
	AT_Spell,
	AT_Steal
};

UENUM(BlueprintType)
enum class ESelectedCharacter : uint8
{
	SC_Character0,
	SC_Character1,
	SC_Character2
};

UENUM(BlueprintType)
enum class ESelectedEnemy : uint8
{
	SE_Enemy0,
	SE_Enemy1,
	SE_Enemy2,
	SE_Enemy3,
	SE_Enemy4,
	SE_Enemy5
};

UENUM(BlueprintType)
enum class ESpellResistType : uint8
{
	SRT_MagRes	UMETA(DisplayName = "MagicResist"),
	SRT_Str		UMETA(DisplayName = "Strength"),
	SRT_Int		UMETA(DisplayName = "Intellect"),
	SRT_Wis		UMETA(DisplayName = "Wisdom"),
	SRT_Agi		UMETA(DisplayName = "Agility"),
	SRT_Con		UMETA(DisplayName = "Constitution"),
	SRT_Cha		UMETA(DisplayName = "Charm")
};

UENUM(BlueprintType)
enum class ESpellTypeOfResist : uint8
{
	STR_Never		UMETA(DisplayName = "Never"),
	STR_AntiMagic	UMETA(DisplayName = "Anti-Magic Only"),
	STR_Everyone	UMETA(DisplayName = "Everyone")
};

USTRUCT(BlueprintType)
struct FPlayerBaseStats : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) int32 HP_Max;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) int32 HP_Current;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) TArray<int32> HP_Roll;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) int32 MP_Max;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) int32 MP_Current;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) int32 Strength;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) int32 Agility;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) int32 Intellect;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) int32 Wisdom;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) int32 Constitution;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) int32 Charm;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) EClasses Class;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) ERaces Race;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) int32 Experience;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) int32 Level;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) int32 CP;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) int32 EvilPoints;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) int32 EncumberanceMax;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) int32 EncumberanceCurrent;
};

USTRUCT(BlueprintType)
struct FPlayerSecondaryStats : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) float Perception = 0;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) float Stealth = 0;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) float Traps = 0;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) float Picklocks = 0;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) float Thievery = 0;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) float Tracking = 0;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) float MartialArts = 0;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) float Illumination = 0;
};

USTRUCT(BlueprintType)
struct FPlayerCombatStats : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) int32 Energy;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) float AC_Hit;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) float AC_DR;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) float Backstab;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) float Accuracy;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) float Spellcasting;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) float ResistMagic;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) float ResistFire;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) float ResistCold;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) float ResistPoison;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) float ResistStone;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) float ResistLightning;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) float ResistGood;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) float ResistEvil;
};


USTRUCT(BlueprintType)
struct FItemBaseBonuses : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) int32 HP_Max;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) int32 MP_Max;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) int32 Strength;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) int32 Agility;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) int32 Intellect;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) int32 Wisdom;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) int32 Constitution;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) int32 Charm;
};

USTRUCT(BlueprintType)
struct FItemSecondaryBonuses : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) float Perception;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) float Stealth;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) float Traps;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) float Picklocks;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) float MartialArts;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) float Illumination;
};

USTRUCT(BlueprintType)
struct FItemCombatBonuses : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) float AC_Hit;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) float AC_DR;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) float Backstab;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) int32 WeaponSpeed;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) int32 WeaponMinHit;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) int32 WeaponMaxHit;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) float Accuracy;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) float ResistMagic;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) float ResistFire;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) float ResistCold;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) float ResistPoison;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) float ResistStone;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) float ResistLightning;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) float ResistGood;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) float ResistEvil;
};

USTRUCT(BlueprintType)
struct FItemRequirements : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) int32 RequiredLevel;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) int32 RequiredStrength;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) int32 RequiredAgility;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) int32 RequiredIntellect;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) int32 RequiredWisdom;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) int32 RequiredConstitution;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) int32 RequiredCharm;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) int32 RequiredEvilPoints;
};

USTRUCT(BlueprintType)
struct FItem : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadWrite, EditAnywhere) int32 ItemNumber;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) FName ItemName;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) FText ItemDescription;

	UPROPERTY(BlueprintReadWrite, EditAnywhere) int32 ItemWeight;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) int32 ItemValueInCopper;

	UPROPERTY(BlueprintReadWrite, EditAnywhere) FItemRequirements ItemRequirements;

	UPROPERTY(BlueprintReadWrite, EditAnywhere) EItemType ItemType;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) EEquipmentSlot ItemEquipSlot;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) EWeaponType ItemWeaponType;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) EArmorType ItemArmorType;

	UPROPERTY(BlueprintReadWrite, EditAnywhere) int32 ItemMaxUses;

	UPROPERTY(BlueprintReadWrite, EditAnywhere) FItemBaseBonuses ItemBaseStats;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) FItemSecondaryBonuses ItemSecondaryStats;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) FItemCombatBonuses ItemCombatStats;

	UPROPERTY(BlueprintReadWrite, EditAnywhere) FString ItemMissMsg;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) TArray<FString> ItemHitMsg;

	UPROPERTY(BlueprintReadWrite, EditAnywhere) TMap<int32, float> ItemAbilities;
};

USTRUCT(BlueprintType)
struct FSpell : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadWrite, EditAnywhere) int32 SpellIndex;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) FName SpellName;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) FName SpellNameShort;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) FText SpellDescription;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) ETargetType SpellTargetType;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) TMap<EAbilMagicClass, int32> SpellMageryType;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) EElementType SpellElementType;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) EAbilApplication SpellApplication;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) EAbilEffectType SpellCategory;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) FText SpellCastMsg;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) FText SpellCurrentMsg;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) FText SpellEndMsg;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) TMap<int32, float> SpellAbilities;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) TArray<int32> SpellsRemoved;

	// Spell Requirements
	UPROPERTY(BlueprintReadWrite, EditAnywhere) int32 SpellDifficulty;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) int32 SpellManaCost;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) int32 SpellEnergyCost;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) int32 SpellRequiredLevel;

	// Spell growth
	UPROPERTY(BlueprintReadWrite, EditAnywhere) int32 SpellBaseMin;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) int32 SpellBaseMax;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) int32 SpellBaseDuration;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) int32 SpellLevelCap;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) int32 SpellLevelsPerMinIncrease;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) int32 SpellMinIncreaseAmount;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) int32 SpellLevelsPerMaxIncrease;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) int32 SpellMaxIncreaseAmount;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) int32 SpellLevelsPerDurationIncrease;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) int32 SpellDurationIncrease;

	// Spell resistances
	UPROPERTY(BlueprintReadWrite, EditAnywhere) int32 SpellResistVsMagRes;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) ESpellResistType SpellResistAbility;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) ESpellTypeOfResist SpellTypeResist;
};

USTRUCT(BlueprintType)
struct FStatusEffect
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) FSpell ModifiedSpellData;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) int32 EffectDuration;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) bool bWasCastByPlayer;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) ESelectedCharacter PlayerCaster;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) ESelectedEnemy EnemyCaster;
};


USTRUCT(BlueprintType)
struct FCharacterData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadWrite, EditAnywhere) FName CharacterName;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) int32 CharacterSlot;

	UPROPERTY(BlueprintReadWrite, EditAnywhere) bool bIsDropped;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) bool bHasTakenTurn;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) bool bHasCastSpell;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) TArray<FStatusEffect> ActiveStatusEffects;

	UPROPERTY(BlueprintReadWrite, EditAnywhere) TMap<int32, float> CharacterAbilities;

	UPROPERTY(BlueprintReadWrite, EditAnywhere) FPlayerBaseStats CharacterBaseStats;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) FPlayerSecondaryStats CharacterSecondaryStats;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) FPlayerCombatStats CharacterCombatStats;

};

USTRUCT(BlueprintType)
struct FClassData : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadWrite, EditAnywhere) FString Class;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) int32 ClassHPRollMin;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) int32 ClassHPRollMax;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) float ClassExpMultiplier;

	UPROPERTY(BlueprintReadWrite, EditAnywhere) FText ClassDescription;

	UPROPERTY(BlueprintReadWrite, EditAnywhere) int32 ClassCombatLevel;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) TArray<EArmorType> ClassAllowedArmorType;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) TArray<EWeaponType> ClassAllowedWeaponType;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) EAbilMagicClass ClassMageryType;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) int32 ClassMageryLevel;

	UPROPERTY(BlueprintReadWrite, EditAnywhere) TArray<FString> ClassTitles;

	UPROPERTY(BlueprintReadWrite, EditAnywhere) TMap<int32, float> ClassAbilities;

};

USTRUCT(BlueprintType)
struct FRaceData : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadWrite, EditAnywhere) FString Race;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere) FText RaceDescription;

	UPROPERTY(BlueprintReadWrite, EditAnywhere) float RaceExpMultiplier;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) int32 RaceHPBonus;

	UPROPERTY(BlueprintReadWrite, EditAnywhere) int32 RaceStrMin;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) int32 RaceStrMax;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) int32 RaceAgiMin;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) int32 RaceAgiMax;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) int32 RaceWisMin;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) int32 RaceWisMax;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) int32 RaceIntMin;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) int32 RaceIntMax;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) int32 RaceConMin;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) int32 RaceConMax;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) int32 RaceChaMin;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) int32 RaceChaMax;

	UPROPERTY(BlueprintReadWrite, EditAnywhere) TMap<int32, float> RaceAbilities;

};

USTRUCT(BlueprintType)
struct FEnemyAttacks : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadWrite, EditAnywhere) EAttackType AttackType;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) int32 AttackAccuracy;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) int32 AttackMinHit;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) int32 AttackMaxHit;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) int32 AttackReqEnergy;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) int32 AttackChanceOfUse;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) int32 AttackSpellToCast;

	UPROPERTY(BlueprintReadWrite, EditAnywhere) FString AttackHitMsg;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) FString AttackMissMsg;
};

USTRUCT(BlueprintType)
struct FEnemyData : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadWrite, EditAnywhere) FName EnemyName;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) int32 EnemyHP;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) int32 EnemyHPRegen;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) int32 EnemyMagicResistance;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) int32 EnemyEnergy;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) bool bIsDropped = true;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) bool bHasTakenTurn;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) bool bHasCastSpell;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) float EnemyACHit;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) float EnemyACDR;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) TMap<int32, float> EnemyAbilities;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) TArray<FEnemyAttacks> EnemyAttacks;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) int32 EnemyEXP;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) float EnemyEXPMultiplier;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) TArray<FItem> EnemyDrops;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) int32 EnemyCopperDropped;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) FString EnemyDeathMsg;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere) TArray<FStatusEffect> ActiveStatusEffects;

	UPROPERTY(BlueprintReadWrite, EditAnywhere) TArray<FString> EnemyNameModifiers;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) int32 EnemyCastOnDeath;
};

USTRUCT(BlueprintType)
struct FCharactersInventory
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadWrite, EditAnywhere) TArray<FItem> Inventory;
};




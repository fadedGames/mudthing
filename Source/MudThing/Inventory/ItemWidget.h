// Copyright 2019, fadedGames - http://fadedGames.net

#pragma once

#include "GlobalData.h"

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ItemWidget.generated.h"

class UButton;
class UTextBlock;

UCLASS()
class MUDTHING_API UItemWidget : public UUserWidget
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(meta = (BindWidget))
	UButton* btn_Item;

public:
	UPROPERTY(meta = (BindWidget))
	UTextBlock* txt_Item;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	FItem MyItem;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	int32 IndexInInventory;
};

// Copyright 2019, fadedGames - http://fadedGames.net

#pragma once

#include "CoreMinimal.h"
#include "GlobalData.h"
#include "Blueprint/UserWidget.h"
#include "GroundItemSelector.generated.h"

class UButton;

/**
 * 
 */
UCLASS()
class MUDTHING_API UGroundItemSelector : public UUserWidget
{
	GENERATED_BODY()

protected:
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, meta = (BindWidget))
	UButton* btn_Cancel;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, meta = (BindWidget))
	UButton* btn_LookAt;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, meta = (BindWidget))
	UButton* btn_Pickup;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	FItem ItemInteracting;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	int32 GroundItemIndex;
	
};

// Copyright 2019, fadedGames - http://fadedGames.net


#include "GroundItem.h"
#include "Components/Button.h"
#include "Inventory/GroundItemSelector.h"
#include "Components/TextBlock.h"

bool UGroundItem::Initialize()
{
	bool Success = Super::Initialize();
	if (!Success) return false;

	btn_GroundItem->OnClicked.AddDynamic(this, &UGroundItem::ClickedItem);
}

void UGroundItem::ClickedItem()
{
	//GroundItemSelector = CreateWidget(this, GroundItemSelectorClass);
}
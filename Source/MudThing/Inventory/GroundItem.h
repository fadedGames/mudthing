// Copyright 2019, fadedGames - http://fadedGames.net

#pragma once

#include "CoreMinimal.h"
#include "GlobalData.h"
#include "Blueprint/UserWidget.h"

#include "GroundItem.generated.h"

/**
 * 
 */
UCLASS()
class MUDTHING_API UGroundItem : public UUserWidget
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, meta = (BindWidget))
	class UButton* btn_GroundItem;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, meta = (BindWidget))
	class UTextBlock* txt_GroundItemName;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	FItem ItemIAm;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	int32 GroundItemIndex;

	UFUNCTION()
	void ClickedItem();

	UPROPERTY()
	TSubclassOf<UUserWidget> GroundItemSelectorClass;

	UPROPERTY()
	class UGroundItemSelector* GroundItemSelector;

public:
	virtual bool Initialize();
};

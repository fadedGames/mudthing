// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "GlobalData.h"
#include "TestEnemy.generated.h"

UCLASS()
class MUDTHING_API ATestEnemy : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ATestEnemy();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly)
	class USkeletalMeshComponent* SkeletalMeshComp;

	class USceneComponent* BaseSceneComponent;


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};

// Copyright 2019, fadedGames - http://fadedGames.net

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"

#include "GlobalData.h"

#include "InventorySubsystem.generated.h"

/**
 * 
 */
UCLASS()
class MUDTHING_API UInventorySubsystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()
	
public:
	// Sets default values for this component's properties
	UInventorySubsystem();

protected:
	virtual void Initialize(FSubsystemCollectionBase& Collection) override;

	void SetupReferences();

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	TArray<FItem> Character0Inventory;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	TArray<FItem> Character1Inventory;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	TArray<FItem> Character2Inventory;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	TMap<EEquipmentSlot, FItem> EquippedItems0;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	TMap<EEquipmentSlot, FItem> EquippedItems1;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	TMap<EEquipmentSlot, FItem> EquippedItems2;

	UPROPERTY()
	class UDataTable* ItemDataTable;

	UPROPERTY()
	class UStatSubsystem* StatSubSys;

	UPROPERTY()
	class AMainHUD* MainHUD;

	UFUNCTION()
	void SetEquippedItemInSlot(ESelectedCharacter SelectedCharacter, EEquipmentSlot SlotToEquip, FItem ItemToEquip);

	UFUNCTION(BlueprintCallable)
	void DropItem(ESelectedCharacter SelectedChar, int32 InventoryIndex);

	UFUNCTION(BlueprintCallable)
	void DropEquippedItem(ESelectedCharacter SelectedChar, EEquipmentSlot SlotToDrop);

	UFUNCTION(BlueprintCallable)
	bool GiveInventoryItemToCharacter(ESelectedCharacter CharacterGiving, ESelectedCharacter CharacterToRecieve, int32 IndexInGiverInventory);

	UFUNCTION(BlueprintCallable)
	bool GiveEquippedItemToCharacter(ESelectedCharacter CharacterGiving, ESelectedCharacter CharacterRecieving, EEquipmentSlot SlotToGive);

	UFUNCTION(BlueprintCallable)
	void PlaceDroppedItemsOnGround(FItem DroppedItem);

	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool HasEncForItem(ESelectedCharacter SelectedChar, FItem ItemToGet);

	UFUNCTION(BlueprintCallable)
	void UseItemFromInventory(ESelectedCharacter SelectedChar, int32 ItemIndexInInventory, FItem ItemToUse);

	UFUNCTION()
	void ReportToTextLog(const FString& String, bool bOverwrite);

public:

	UFUNCTION(BlueprintCallable)
	FItem GetEquippedItemInSlot(ESelectedCharacter SelectedCharacter, EEquipmentSlot SlotToGet);

	UFUNCTION(BlueprintCallable)
	void EquipItem(ESelectedCharacter SelectedCharacter, FItem ItemToEquip, int32 IndexInMainInventory);

	UFUNCTION(BlueprintCallable)
	void UnequipItem(ESelectedCharacter SelectedCharacter, EEquipmentSlot SlotToRemove);

	UFUNCTION(BlueprintCallable)
	bool AddItemToInventory(ESelectedCharacter SelectedChar, FItem ItemToAdd);

	UFUNCTION(BlueprintCallable)
	int32 GetTotalEncumbrance(ESelectedCharacter SelectedChar);

	UFUNCTION()
	TArray<FItem> GetCharInventory(ESelectedCharacter SelectedChar);
};

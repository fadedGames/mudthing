// Copyright 2019, fadedGames - http://fadedGames.net

#include "InventorySubsystem.h"

#include "ConstructorHelpers.h"
#include "Engine/DataTable.h"
#include "Player/MainPlayerController.h"
#include "UI/MainHUD.h"
#include "Engine/World.h"
#include "Player/StatSubsystem.h"
#include "Movement/RoomPoint.h"
#include "Player/MainPlayer.h"

UInventorySubsystem::UInventorySubsystem()
{
	static ConstructorHelpers::FObjectFinder<UDataTable> ItemDataTableTemp(TEXT("DataTable'/Game/Inventory/Data/DT_Items.DT_Items'"));
	ItemDataTable = ItemDataTableTemp.Object;

	FItem tempItem;
	tempItem.ItemAbilities.Add(0, 0.0f);
	Character0Inventory.Add(tempItem);
}

void UInventorySubsystem::Initialize(FSubsystemCollectionBase& Collection)
{
	Super::Initialize(Collection);
}

void UInventorySubsystem::SetupReferences()
{
	UGameInstance* GameInstance = Cast<UGameInstance>(GetGameInstance());
	if (!ensure(GameInstance != nullptr))
	{
		UE_LOG(LogTemp, Error, TEXT("Game instance not found!!"));
		return;
	}
	StatSubSys = GameInstance->GetSubsystem<UStatSubsystem>();
}

void UInventorySubsystem::SetEquippedItemInSlot(ESelectedCharacter SelectedCharacter, EEquipmentSlot SlotToEquip, FItem ItemToEquip)
{
	switch (SelectedCharacter)
	{
	case ESelectedCharacter::SC_Character0:
		EquippedItems0.Emplace(SlotToEquip, ItemToEquip);
		break;
	case ESelectedCharacter::SC_Character1:
		EquippedItems1.Emplace(SlotToEquip, ItemToEquip);
		break;
	case ESelectedCharacter::SC_Character2:
		EquippedItems2.Emplace(SlotToEquip, ItemToEquip);
		break;
	}
}

void UInventorySubsystem::DropItem(ESelectedCharacter SelectedChar, int32 InventoryIndex)
{
	SetupReferences();
	FItem ItemToBeDropped;
	FCharacterData CharData = StatSubSys->GetCharacterData(SelectedChar);
	FString CharName = CharData.CharacterName.ToString();;
	FString ItemName;
	switch (SelectedChar)
	{
	case ESelectedCharacter::SC_Character0:
		ItemToBeDropped = Character0Inventory[InventoryIndex];
		Character0Inventory.RemoveAt(InventoryIndex);
		PlaceDroppedItemsOnGround(ItemToBeDropped);
		ItemName = ItemToBeDropped.ItemName.ToString();
		ReportToTextLog(CharName + " dropped " + ItemName + ".", false);
		break;
	case ESelectedCharacter::SC_Character1:
		ItemToBeDropped = Character1Inventory[InventoryIndex];
		Character1Inventory.RemoveAt(InventoryIndex);
		PlaceDroppedItemsOnGround(ItemToBeDropped);
		ItemName = ItemToBeDropped.ItemName.ToString();
		ReportToTextLog(CharName + " dropped " + ItemName + ".", false);
		break;
	case ESelectedCharacter::SC_Character2:
		ItemToBeDropped = Character2Inventory[InventoryIndex];
		Character2Inventory.RemoveAt(InventoryIndex);
		PlaceDroppedItemsOnGround(ItemToBeDropped);
		ItemName = ItemToBeDropped.ItemName.ToString();
		ReportToTextLog(CharName + " dropped " + ItemName + ".", false);
		break;
	}
	StatSubSys->AddEncumberence(SelectedChar, -ItemToBeDropped.ItemWeight);
}

void UInventorySubsystem::DropEquippedItem(ESelectedCharacter SelectedChar, EEquipmentSlot SlotToDrop)
{
	SetupReferences();
	FItem EquippedItemToDrop;
	FItem TempEmptyItem;
	TempEmptyItem.ItemName = "None";
	FCharacterData CharData = StatSubSys->GetCharacterData(SelectedChar);
	FString CharName = CharData.CharacterName.ToString();;
	FString ItemName;
	switch (SelectedChar)
	{
	case ESelectedCharacter::SC_Character0:
		EquippedItemToDrop = EquippedItems0[SlotToDrop];
		StatSubSys->RemoveItemStatsFromChar(EquippedItemToDrop, SelectedChar);
		EquippedItems0.Emplace(SlotToDrop, TempEmptyItem);
		PlaceDroppedItemsOnGround(EquippedItemToDrop);
		ItemName = EquippedItemToDrop.ItemName.ToString();
		ReportToTextLog(CharName + " dropped " + ItemName + ".", false);
		break;
	case ESelectedCharacter::SC_Character1:
		EquippedItemToDrop = EquippedItems1[SlotToDrop];
		StatSubSys->RemoveItemStatsFromChar(EquippedItemToDrop, SelectedChar);
		EquippedItems1.Emplace(SlotToDrop, TempEmptyItem);
		PlaceDroppedItemsOnGround(EquippedItemToDrop);
		ItemName = EquippedItemToDrop.ItemName.ToString();
		ReportToTextLog(CharName + " dropped " + ItemName + ".", false);
		break;
	case ESelectedCharacter::SC_Character2:
		EquippedItemToDrop = EquippedItems2[SlotToDrop];
		StatSubSys->RemoveItemStatsFromChar(EquippedItemToDrop, SelectedChar);
		EquippedItems2.Emplace(SlotToDrop, TempEmptyItem);
		PlaceDroppedItemsOnGround(EquippedItemToDrop);
		ItemName = EquippedItemToDrop.ItemName.ToString();
		ReportToTextLog(CharName + " dropped " + ItemName + ".", false);
		break;
	}
	StatSubSys->AddEncumberence(SelectedChar, -EquippedItemToDrop.ItemWeight);
}

bool UInventorySubsystem::GiveInventoryItemToCharacter(ESelectedCharacter CharacterGiving, ESelectedCharacter CharacterToRecieve, int32 IndexInGiverInventory)
{
	SetupReferences();
	FItem TempToTrade;
	switch (CharacterGiving)
	{
	case ESelectedCharacter::SC_Character0:
		TempToTrade = Character0Inventory[IndexInGiverInventory];
		switch (CharacterToRecieve)
		{
		case ESelectedCharacter::SC_Character0:
			UE_LOG(LogTemp, Warning, TEXT("This code should never run!  Giving to yourself!"));
			return false;
			break;
		case ESelectedCharacter::SC_Character1:
			UE_LOG(LogTemp, Warning, TEXT("Attempting to give to Char 1"));
			if (HasEncForItem(CharacterToRecieve, TempToTrade))
			{
				StatSubSys->AddEncumberence(CharacterToRecieve, TempToTrade.ItemWeight);
				Character1Inventory.Add(TempToTrade);
				Character0Inventory.RemoveAt(IndexInGiverInventory);
				StatSubSys->AddEncumberence(CharacterGiving, -TempToTrade.ItemWeight);
				return true;
			}
			else
			{
				return false;
			}
			break;
		case ESelectedCharacter::SC_Character2:
			UE_LOG(LogTemp, Warning, TEXT("Attempting to give to Char 2"));
			if (HasEncForItem(CharacterToRecieve, TempToTrade))
			{
				StatSubSys->AddEncumberence(CharacterToRecieve, TempToTrade.ItemWeight);
				Character2Inventory.Add(TempToTrade);
				Character0Inventory.RemoveAt(IndexInGiverInventory);
				StatSubSys->AddEncumberence(CharacterGiving, -TempToTrade.ItemWeight);
				return true;
			}
			else
			{
				return false;
			}
			break;
		}
		break;
	case ESelectedCharacter::SC_Character1:
		TempToTrade = Character1Inventory[IndexInGiverInventory];

		switch (CharacterToRecieve)
		{
		case ESelectedCharacter::SC_Character0:
			if (HasEncForItem(CharacterToRecieve, TempToTrade))
			{
				StatSubSys->AddEncumberence(CharacterToRecieve, TempToTrade.ItemWeight);
				Character0Inventory.Add(TempToTrade);
				Character1Inventory.RemoveAt(IndexInGiverInventory);
				StatSubSys->AddEncumberence(CharacterGiving, -TempToTrade.ItemWeight);
				return true;
			}
			else
			{
				return false;
			}
			break;
		case ESelectedCharacter::SC_Character1:
			UE_LOG(LogTemp, Warning, TEXT("This code should never run!  Giving to yourself!"));
			return false;
			break;
		case ESelectedCharacter::SC_Character2:
			if (HasEncForItem(CharacterToRecieve, TempToTrade))
			{
				StatSubSys->AddEncumberence(CharacterToRecieve, TempToTrade.ItemWeight);
				Character2Inventory.Add(TempToTrade);
				Character1Inventory.RemoveAt(IndexInGiverInventory);
				StatSubSys->AddEncumberence(CharacterGiving, -TempToTrade.ItemWeight);
				return true;
			}
			else
			{
				return false;
			}
			break;
		}
		break;
	case ESelectedCharacter::SC_Character2:
		TempToTrade = Character2Inventory[IndexInGiverInventory];

		switch (CharacterToRecieve)
		{
		case ESelectedCharacter::SC_Character0:
			if (HasEncForItem(CharacterToRecieve, TempToTrade))
			{
				StatSubSys->AddEncumberence(CharacterToRecieve, TempToTrade.ItemWeight);
				Character0Inventory.Add(TempToTrade);
				Character2Inventory.RemoveAt(IndexInGiverInventory);
				StatSubSys->AddEncumberence(CharacterGiving, -TempToTrade.ItemWeight);
				return true;
			}
			else
			{
				return false;
			}
			break;
		case ESelectedCharacter::SC_Character1:
			if (HasEncForItem(CharacterToRecieve, TempToTrade))
			{
				StatSubSys->AddEncumberence(CharacterToRecieve, TempToTrade.ItemWeight);
				Character1Inventory.Add(TempToTrade);
				Character2Inventory.RemoveAt(IndexInGiverInventory);
				StatSubSys->AddEncumberence(CharacterGiving, -TempToTrade.ItemWeight);
				return true;
			}
			else
			{
				return false;
			}
			break;
		case ESelectedCharacter::SC_Character2:
			UE_LOG(LogTemp, Warning, TEXT("This code should never run!  Giving to yourself!"));
			return false;
			break;
		}
		break;
	}
	return false;
}

bool UInventorySubsystem::GiveEquippedItemToCharacter(ESelectedCharacter CharacterGiving, ESelectedCharacter CharacterRecieving, EEquipmentSlot SlotToGive)
{
	SetupReferences();
	FItem TempToTrade;
	FItem TempItem;
	TempItem.ItemName = "None";
	FCharacterData CharData = StatSubSys->GetCharacterData(CharacterRecieving);
	switch (CharacterGiving)
	{
	case ESelectedCharacter::SC_Character0:
		TempToTrade = EquippedItems0[SlotToGive];

		switch (CharacterRecieving)
		{
		case ESelectedCharacter::SC_Character0:
			UE_LOG(LogTemp, Warning, TEXT("This code should never run!  Giving to yourself!"));
			return false;
			break;
		case ESelectedCharacter::SC_Character1:
			if (HasEncForItem(CharacterRecieving, TempToTrade))
			{
				StatSubSys->AddEncumberence(CharacterRecieving, TempToTrade.ItemWeight);
				Character1Inventory.Add(TempToTrade);
				EquippedItems0.Emplace(SlotToGive, TempItem);
				StatSubSys->AddEncumberence(CharacterGiving, -TempToTrade.ItemWeight);
				return true;
			}
			else
			{
				FString RecievingCharName = CharData.CharacterName.ToString();
				ReportToTextLog(RecievingCharName + " cannot carry that much!", false);
				return false;
			}
			break;
		case ESelectedCharacter::SC_Character2:
			if (HasEncForItem(CharacterRecieving, TempToTrade))
			{
				StatSubSys->AddEncumberence(CharacterRecieving, TempToTrade.ItemWeight);
				Character2Inventory.Add(TempToTrade);
				EquippedItems0.Emplace(SlotToGive, TempItem);
				StatSubSys->AddEncumberence(CharacterGiving, -TempToTrade.ItemWeight);
				return true;
			}
			else
			{
				FString RecievingCharName = CharData.CharacterName.ToString();
				ReportToTextLog(RecievingCharName + " cannot carry that much!", false);
				return false;
			}
			break;
		}
		return false;
		break;
	case ESelectedCharacter::SC_Character1:
		TempToTrade = EquippedItems1[SlotToGive];

		switch (CharacterRecieving)
		{
		case ESelectedCharacter::SC_Character0:
			if (HasEncForItem(CharacterRecieving, TempToTrade))
			{
				StatSubSys->AddEncumberence(CharacterRecieving, TempToTrade.ItemWeight);
				Character0Inventory.Add(TempToTrade);
				EquippedItems1.Emplace(SlotToGive, TempItem);
				StatSubSys->AddEncumberence(CharacterGiving, -TempItem.ItemWeight);
				return true;
			}
			else
			{
				FString RecievingCharName = CharData.CharacterName.ToString();
				ReportToTextLog(RecievingCharName + " cannot carry that much!", false);
				return false;
			}
			break;
		case ESelectedCharacter::SC_Character1:
			UE_LOG(LogTemp, Warning, TEXT("This code should never run!  Giving to yourself!"));
			return false;
			break;
		case ESelectedCharacter::SC_Character2:
			if (HasEncForItem(CharacterRecieving, TempItem))
			{
				StatSubSys->AddEncumberence(CharacterRecieving, TempItem.ItemWeight);
				Character2Inventory.Add(TempToTrade);
				EquippedItems1.Emplace(SlotToGive, TempItem);
				StatSubSys->AddEncumberence(CharacterGiving, -TempToTrade.ItemWeight);
				return true;
			}
			break;
		}
		return false;
		break;
	case ESelectedCharacter::SC_Character2:
		TempToTrade = EquippedItems2[SlotToGive];

		switch (CharacterRecieving)
		{
		case ESelectedCharacter::SC_Character0:
			if (HasEncForItem(CharacterRecieving, TempToTrade))
			{
				StatSubSys->AddEncumberence(CharacterRecieving, TempToTrade.ItemWeight);
				Character0Inventory.Add(TempToTrade);
				EquippedItems2.Emplace(SlotToGive, TempItem);
				StatSubSys->AddEncumberence(CharacterGiving, -TempToTrade.ItemWeight);
				return true;
			}
			else
			{
				FString RecievingCharName = CharData.CharacterName.ToString();
				ReportToTextLog(RecievingCharName + " cannot carry that much!", false);
				return false;
			}
			break;
		case ESelectedCharacter::SC_Character1:
			if (HasEncForItem(CharacterRecieving, TempToTrade))
			{
				StatSubSys->AddEncumberence(CharacterRecieving, TempToTrade.ItemWeight);
				Character1Inventory.Add(TempToTrade);
				EquippedItems2.Emplace(SlotToGive, TempItem);
				StatSubSys->AddEncumberence(CharacterGiving, -TempToTrade.ItemWeight);
				return true;
			}
			else
			{
				FString RecievingCharName = CharData.CharacterName.ToString();
				ReportToTextLog(RecievingCharName + " cannot carry that much!", false);
				return false;
			}
			break;
		case ESelectedCharacter::SC_Character2:
			UE_LOG(LogTemp, Warning, TEXT("This code should never run!  Giving to yourself!"));
			return false;
			break;
		}
		break;
	}
	return false;
}

//TODO rewrite this to use saved room point in game instance
void UInventorySubsystem::PlaceDroppedItemsOnGround(FItem DroppedItem)
{
	AMainPlayer* MainPlayer = Cast<AMainPlayer>(GetWorld()->GetFirstPlayerController()->GetPawn());
	FHitResult outHit;
	FVector startPoint = MainPlayer->GetActorLocation();
	FVector endPoint = MainPlayer->GetActorLocation() + MainPlayer->GetActorLocation().Z - 100.0f;
	FQuat quat = MainPlayer->GetActorRotation().Quaternion();
	FCollisionShape shape;
	shape.SetSphere(500.0f);
	FCollisionQueryParams params;

	if (GetWorld()->SweepSingleByObjectType(outHit, startPoint, endPoint, quat, ECC_Vehicle, shape, params))
	{
		ARoomPoint* roomPoint;
		roomPoint = Cast<ARoomPoint>(outHit.Actor);
		roomPoint->AddToDroppedItems(DroppedItem);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("Room point not found"));
	}
}

bool UInventorySubsystem::HasEncForItem(ESelectedCharacter SelectedChar, FItem ItemToGet)
{
	SetupReferences();
	FCharacterData CharData = StatSubSys->GetCharacterData(SelectedChar);
	int32 currentEnc = CharData.CharacterBaseStats.EncumberanceCurrent;
	int32 MaxEnc = CharData.CharacterBaseStats.EncumberanceMax;
	int32 ItemsEnc = ItemToGet.ItemWeight;

	if ((currentEnc + ItemsEnc) <= MaxEnc)
		return true;
	return false;
}

void UInventorySubsystem::UseItemFromInventory(ESelectedCharacter SelectedChar, int32 ItemIndexInInventory, FItem ItemToUse)
{
	SetupReferences();
	switch (SelectedChar)
	{
	case ESelectedCharacter::SC_Character0:
		if (ItemToUse.ItemType == EItemType::IT_Scroll)
		{
			if (StatSubSys->AddSpellToChar(SelectedChar, (int32)* ItemToUse.ItemAbilities.Find(42)))
			{
				StatSubSys->AddEncumberence(SelectedChar, -ItemToUse.ItemWeight);
				Character0Inventory.RemoveAt(ItemIndexInInventory);
			}
			else
			{
				ReportToTextLog("You cannot learn that spell.", false);
			}
		}
		break;
	case ESelectedCharacter::SC_Character1:
		break;
	case ESelectedCharacter::SC_Character2:
		break;
	default:
		break;
	}
}

FItem UInventorySubsystem::GetEquippedItemInSlot(ESelectedCharacter SelectedCharacter, EEquipmentSlot SlotToGet)
{
	FItem* TempItem;
	switch (SelectedCharacter)
	{
	case ESelectedCharacter::SC_Character0:
		TempItem = EquippedItems0.Find(SlotToGet);
		if (TempItem)
		{
			return *TempItem;
		}
		else
		{
			TempItem = new FItem;
			TempItem->ItemName = "Empty";
			TempItem->ItemEquipSlot = SlotToGet;
			TempItem->ItemAbilities.Add(0, 0.0f);
			return *TempItem;
		}
		break;
	case ESelectedCharacter::SC_Character1:
		TempItem = EquippedItems1.Find(SlotToGet);
		if (TempItem)
		{
			return *TempItem;
		}
		else
		{
			TempItem = new FItem;
			TempItem->ItemName = "Empty";
			TempItem->ItemEquipSlot = SlotToGet;
			TempItem->ItemAbilities.Add(0, 0.0f);
			return *TempItem;
		}
		break;
	case ESelectedCharacter::SC_Character2:
		TempItem = EquippedItems2.Find(SlotToGet);
		if (TempItem)
		{
			return *TempItem;
		}
		else
		{
			TempItem = new FItem;
			TempItem->ItemName = "Empty";
			TempItem->ItemEquipSlot = SlotToGet;
			TempItem->ItemAbilities.Add(0, 0.0f);
			return *TempItem;
		}
		break;
	}
	TempItem = new FItem;
	TempItem->ItemName = "Empty";
	TempItem->ItemEquipSlot = SlotToGet;
	TempItem->ItemAbilities.Add(0, 0.0f);
	return *TempItem;
}

void UInventorySubsystem::EquipItem(ESelectedCharacter SelectedCharacter, FItem ItemToEquip, int32 IndexInMainInventory)
{
	SetupReferences();
	EEquipmentSlot TempSlot;
	FCharacterData CharData = StatSubSys->GetCharacterData(SelectedCharacter);
	FString CharName = CharData.CharacterName.ToString();
	FString ItemName;
	FString ActionString;
	switch (SelectedCharacter)
	{
	case ESelectedCharacter::SC_Character0:
		TempSlot = ItemToEquip.ItemEquipSlot;
		if ((EquippedItems0[TempSlot].ItemName == "None") || (EquippedItems0[TempSlot].ItemName == "Empty"))
		{
			StatSubSys->AddItemStatsToChar(ItemToEquip, SelectedCharacter);
			ItemName = ItemToEquip.ItemName.ToString();
			if ((ItemToEquip.ItemEquipSlot == EEquipmentSlot::ES_Weapon) || (ItemToEquip.ItemEquipSlot == EEquipmentSlot::ES_Offhand))
				ActionString = " is now holding ";
			else
				ActionString = " wears ";
			ReportToTextLog(CharName + ActionString + ItemName + ".", false);
		}
		else
		{
			StatSubSys->RemoveItemStatsFromChar(EquippedItems0[TempSlot], SelectedCharacter);
			StatSubSys->AddItemStatsToChar(ItemToEquip, SelectedCharacter);
			Character0Inventory.Add(EquippedItems0[TempSlot]);
			ItemName = EquippedItems0[TempSlot].ItemName.ToString();
			if ((EquippedItems0[TempSlot].ItemEquipSlot == EEquipmentSlot::ES_Weapon) || (EquippedItems0[TempSlot].ItemEquipSlot == EEquipmentSlot::ES_Offhand))
				ActionString = " is no longer holding ";
			else
				ActionString = " removes ";
			ReportToTextLog(CharName + ActionString + ItemName + ".", false);
			ItemName = ItemToEquip.ItemName.ToString();
			if ((ItemToEquip.ItemEquipSlot == EEquipmentSlot::ES_Weapon) || (ItemToEquip.ItemEquipSlot == EEquipmentSlot::ES_Offhand))
				ActionString = " is now holding ";
			else
				ActionString = " wears ";
			ReportToTextLog(CharName + ActionString + ItemName + ".", false);
		}
		SetEquippedItemInSlot(SelectedCharacter, TempSlot, ItemToEquip);
		Character0Inventory.RemoveAt(IndexInMainInventory);
		break;
	case ESelectedCharacter::SC_Character1:
		TempSlot = ItemToEquip.ItemEquipSlot;
		if (EquippedItems1[TempSlot].ItemName == "None" || EquippedItems1[TempSlot].ItemName == "Empty")
		{
			StatSubSys->AddItemStatsToChar(ItemToEquip, SelectedCharacter);
			ItemName = ItemToEquip.ItemName.ToString();
			if ((ItemToEquip.ItemEquipSlot == EEquipmentSlot::ES_Weapon) || (ItemToEquip.ItemEquipSlot == EEquipmentSlot::ES_Offhand))
				ActionString = " is now holding ";
			else
				ActionString = " wears ";
			ReportToTextLog(CharName + ActionString + ItemName + ".", false);
		}
		else
		{
			StatSubSys->RemoveItemStatsFromChar(EquippedItems1[TempSlot], SelectedCharacter);
			StatSubSys->AddItemStatsToChar(ItemToEquip, SelectedCharacter);
			Character1Inventory.Add(EquippedItems1[TempSlot]);
			ItemName = EquippedItems1[TempSlot].ItemName.ToString();
			if ((EquippedItems1[TempSlot].ItemEquipSlot == EEquipmentSlot::ES_Weapon) || (EquippedItems1[TempSlot].ItemEquipSlot == EEquipmentSlot::ES_Offhand))
				ActionString = " is no longer holding ";
			else
				ActionString = " removes ";
			ReportToTextLog(CharName + ActionString + ItemName + ".", false);
			ItemName = ItemToEquip.ItemName.ToString();
			if ((ItemToEquip.ItemEquipSlot == EEquipmentSlot::ES_Weapon) || (ItemToEquip.ItemEquipSlot == EEquipmentSlot::ES_Offhand))
				ActionString = " is now holding ";
			else
				ActionString = " wears ";
			ReportToTextLog(CharName + ActionString + ItemName + ".", false);
		}
		SetEquippedItemInSlot(SelectedCharacter, TempSlot, ItemToEquip);
		Character1Inventory.RemoveAt(IndexInMainInventory);
		break;
	case ESelectedCharacter::SC_Character2:
		TempSlot = ItemToEquip.ItemEquipSlot;
		if (EquippedItems2[TempSlot].ItemName == "None" || EquippedItems2[TempSlot].ItemName == "Empty")
		{
			StatSubSys->AddItemStatsToChar(ItemToEquip, SelectedCharacter);
			ItemName = ItemToEquip.ItemName.ToString();
			if ((ItemToEquip.ItemEquipSlot == EEquipmentSlot::ES_Weapon) || (ItemToEquip.ItemEquipSlot == EEquipmentSlot::ES_Offhand))
				ActionString = " is now holding ";
			else
				ActionString = " wears ";
			ReportToTextLog(CharName + ActionString + ItemName + ".", false);
		}
		else
		{
			StatSubSys->RemoveItemStatsFromChar(EquippedItems2[TempSlot], SelectedCharacter);
			StatSubSys->AddItemStatsToChar(ItemToEquip, SelectedCharacter);
			Character2Inventory.Add(EquippedItems2[TempSlot]);
			ItemName = EquippedItems2[TempSlot].ItemName.ToString();
			if ((EquippedItems2[TempSlot].ItemEquipSlot == EEquipmentSlot::ES_Weapon) || (EquippedItems2[TempSlot].ItemEquipSlot == EEquipmentSlot::ES_Offhand))
				ActionString = " is no longer holding ";
			else
				ActionString = " removes ";
			ReportToTextLog(CharName + ActionString + ItemName + ".", false);
			ItemName = ItemToEquip.ItemName.ToString();
			if ((ItemToEquip.ItemEquipSlot == EEquipmentSlot::ES_Weapon) || (ItemToEquip.ItemEquipSlot == EEquipmentSlot::ES_Offhand))
				ActionString = " is now holding ";
			else
				ActionString = " wears ";
			ReportToTextLog(CharName + ActionString + ItemName + ".", false);
		}
		SetEquippedItemInSlot(SelectedCharacter, TempSlot, ItemToEquip);
		Character2Inventory.RemoveAt(IndexInMainInventory);
		break;
	}
}

void UInventorySubsystem::UnequipItem(ESelectedCharacter SelectedCharacter, EEquipmentSlot SlotToRemove)
{
	SetupReferences();
	FItem EquippedItemToRemove;
	FItem TempEmptyItem;
	TempEmptyItem.ItemName = "None";
	FCharacterData CharData = StatSubSys->GetCharacterData(SelectedCharacter);
	FString CharName = CharData.CharacterName.ToString();
	FString ActionString;
	FString ItemName;

	switch (SelectedCharacter)
	{
	case ESelectedCharacter::SC_Character0:
		EquippedItemToRemove = EquippedItems0[SlotToRemove];
		StatSubSys->RemoveItemStatsFromChar(EquippedItemToRemove, SelectedCharacter);
		EquippedItems0.Emplace(SlotToRemove, TempEmptyItem);
		Character0Inventory.Add(EquippedItemToRemove);
		ItemName = EquippedItemToRemove.ItemName.ToString();
		if ((EquippedItemToRemove.ItemEquipSlot == EEquipmentSlot::ES_Weapon) || (EquippedItemToRemove.ItemEquipSlot == EEquipmentSlot::ES_Offhand))
			ActionString = " is no longer holding ";
		else
			ActionString = " removes ";
		ReportToTextLog(CharName + ActionString + ItemName + ".", false);
		break;
	case ESelectedCharacter::SC_Character1:
		EquippedItemToRemove = EquippedItems1[SlotToRemove];
		StatSubSys->RemoveItemStatsFromChar(EquippedItemToRemove, SelectedCharacter);
		EquippedItems1.Emplace(SlotToRemove, TempEmptyItem);
		Character1Inventory.Add(EquippedItemToRemove);
		ItemName = EquippedItemToRemove.ItemName.ToString();
		if ((EquippedItemToRemove.ItemEquipSlot == EEquipmentSlot::ES_Weapon) || (EquippedItemToRemove.ItemEquipSlot == EEquipmentSlot::ES_Offhand))
			ActionString = " is no longer holding ";
		else
			ActionString = " removes ";
		ReportToTextLog(CharName + ActionString + ItemName + ".", false);
		break;
	case ESelectedCharacter::SC_Character2:
		EquippedItemToRemove = EquippedItems2[SlotToRemove];
		StatSubSys->RemoveItemStatsFromChar(EquippedItemToRemove, SelectedCharacter);
		EquippedItems2.Emplace(SlotToRemove, TempEmptyItem);
		Character2Inventory.Add(EquippedItemToRemove);
		ItemName = EquippedItemToRemove.ItemName.ToString();
		if ((EquippedItemToRemove.ItemEquipSlot == EEquipmentSlot::ES_Weapon) || (EquippedItemToRemove.ItemEquipSlot == EEquipmentSlot::ES_Offhand))
			ActionString = " is no longer holding ";
		else
			ActionString = " removes ";
		ReportToTextLog(CharName + ActionString + ItemName + ".", false);
		break;
	}
}

bool UInventorySubsystem::AddItemToInventory(ESelectedCharacter SelectedChar, FItem ItemToAdd)
{
	SetupReferences();
	FCharacterData CharData = StatSubSys->GetCharacterData(SelectedChar);
	switch (SelectedChar)
	{
	case ESelectedCharacter::SC_Character0:
		if (HasEncForItem(SelectedChar, ItemToAdd))
		{
			Character0Inventory.Add(ItemToAdd);
			StatSubSys->AddEncumberence(SelectedChar, ItemToAdd.ItemWeight);
			return true;
		}
		else
		{
			FString charName = CharData.CharacterName.ToString();
			ReportToTextLog(charName + " cannot carry that much!", false);
			return false;
		}
		break;
	case ESelectedCharacter::SC_Character1:
		if (HasEncForItem(SelectedChar, ItemToAdd))
		{
			Character1Inventory.Add(ItemToAdd);
			StatSubSys->AddEncumberence(SelectedChar, ItemToAdd.ItemWeight);
			return true;
		}
		else
		{
			FString charName = CharData.CharacterName.ToString();
			ReportToTextLog(charName + " cannot carry that much!", false);
			return false;
		}
		break;
	case ESelectedCharacter::SC_Character2:
		if (HasEncForItem(SelectedChar, ItemToAdd))
		{
			Character2Inventory.Add(ItemToAdd);
			StatSubSys->AddEncumberence(SelectedChar, ItemToAdd.ItemWeight);
			return true;
		}
		else
		{
			FString charName = CharData.CharacterName.ToString();
			ReportToTextLog(charName + " cannot carry that much!", false);
			return false;
		}
		break;
	}
	return false;
}

int32 UInventorySubsystem::GetTotalEncumbrance(ESelectedCharacter SelectedChar)
{
	SetupReferences();
	TArray<FItem> WorkingInventory;
	int32 ReturnEnc = 0;
	FItem TempItem;
	switch (SelectedChar)
	{
	case ESelectedCharacter::SC_Character0:
		WorkingInventory = Character0Inventory;
		break;
	case ESelectedCharacter::SC_Character1:
		WorkingInventory = Character0Inventory;
		break;
	case ESelectedCharacter::SC_Character2:
		WorkingInventory = Character0Inventory;
		break;
	}
	for (int32 inv = 0; inv < WorkingInventory.Num(); inv++)
	{
		ReturnEnc += WorkingInventory[inv].ItemWeight;
	}
	for (uint8 eqInv = 0; eqInv <= 18; eqInv++)
	{
		TempItem = GetEquippedItemInSlot(SelectedChar, (EEquipmentSlot)eqInv);
		if ((TempItem.ItemName != "None") || (TempItem.ItemName != "Empty"))
		{
			ReturnEnc += TempItem.ItemWeight;
		}
	}
	return ReturnEnc;
}

void UInventorySubsystem::ReportToTextLog(const FString& String, bool bOverwrite)
{
	UWorld* World = GetWorld();
	if (!ensure(World != nullptr)) return;
	AMainPlayerController* MainPlayerController = Cast<AMainPlayerController>(World->GetFirstPlayerController());
	if (MainPlayerController)
	{
		MainHUD = Cast<AMainHUD>(MainPlayerController->GetHUD());
	}
	MainHUD->ReportToUI(String, bOverwrite);
}


TArray<FItem> UInventorySubsystem::GetCharInventory(ESelectedCharacter SelectedChar)
{
	UE_LOG(LogTemp, Warning, TEXT("Returning inventory for character %i"), (int32)SelectedChar);
	switch (SelectedChar)
	{
	case ESelectedCharacter::SC_Character0:
		return Character0Inventory;
		break;
	case ESelectedCharacter::SC_Character1:
		return Character1Inventory;
		break;
	case ESelectedCharacter::SC_Character2:
		return Character2Inventory;
		break;
	}
	
	return Character0Inventory;
}
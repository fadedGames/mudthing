// Copyright 2019, fadedGames - http://fadedGames.net

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "Combat/CombatManager.h"
#include "MainPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class MUDTHING_API AMainPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	AMainPlayerController();

protected:
	virtual void BeginPlay() override;
	virtual void PlayerTick(float DeltaTime) override;
	virtual void SetupInputComponent() override;
public:
	UFUNCTION(BlueprintCallable)
	void MoveToTargetPoint();
	UFUNCTION(BlueprintCallable)
	void MoveBackwards();
	UFUNCTION(BlueprintCallable)
	void TurnRight();
	UFUNCTION(BlueprintCallable)
	void TurnLeft();
	UFUNCTION(BlueprintCallable)
	void TurnAround();
	UFUNCTION(BlueprintCallable)
	void TurnRight45();
	UFUNCTION(BlueprintCallable)
	void TurnLeft45();

	void RotateCharacter(int RotationAmount);
protected:
	bool CheckIfMoving();

	class ACombatManager* CombatManager;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ACombatManager> CombatManagerBP;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	class ACombatManager* CombatManagerBP_REF;

	UPROPERTY(EditDefaultsOnly)
	class AMainHUD* MainHUD;

	bool bIsPaused = false;

	UPROPERTY()
	class UStatSubsystem* StatSubSys;

	UFUNCTION()
	void PassiveRegenTick();
private:
	UPROPERTY()
	int32 StepsTaken = 0;

	UPROPERTY()
	class AMainPlayer* MainPlayer;

public:
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	bool bMovementAllowed = true;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	bool bInCombat = false;

	UFUNCTION(BlueprintCallable)
	void ToggleCombat(TArray<int32> EnemyGroup);

	UFUNCTION(BlueprintCallable)
	void ToggleStatusScreen();

	UFUNCTION(BlueprintCallable)
	void TabUp();

	UFUNCTION(BlueprintCallable)
	void TabDown();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bIsInMenuSystem = true;

};

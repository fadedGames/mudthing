// Copyright 2019, fadedGames - http://fadedGames.net

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "GlobalData.h"
#include "StatSubsystem.generated.h"

/**
 * 
 */
UCLASS()
class MUDTHING_API UStatSubsystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()
	
public:
	// Sets default values for this component's properties
	UStatSubsystem();

	UPROPERTY(BlueprintReadWrite)
	bool bStopResting = false;

	UPROPERTY(BlueprintReadWrite)
	bool bIsCurrentlyResting = false;

protected:

	virtual void Initialize(FSubsystemCollectionBase& Collection) override;

	UFUNCTION()
		void SetRefs();

	UPROPERTY()
		class UInventorySubsystem* InventorySubSys;

	UPROPERTY()
		class UDataTable* ClassDT;

	UPROPERTY()
		class UDataTable* RaceDT;

	UPROPERTY()
		class UDataTable* SpellDT;

	UPROPERTY()
		class AMainHUD* MainHUD;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
		TMap<ESelectedCharacter, FCharacterData> CharacterData;

	UFUNCTION(BlueprintCallable)
		void SetupBetterBunkData();

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
		TMap<int32, float> AbilitiesFromStats0;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
		TMap<int32, float> AbilitiesFromStats1;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
		TMap<int32, float> AbilitiesFromStats2;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
		TMap<int32, float> AbilitiesFromSpells0;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
		TMap<int32, float> AbilitiesFromSpells1;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
		TMap<int32, float> AbilitiesFromSpells2;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
		TArray<int32> Char0Spells;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
		TArray<int32> Char1Spells;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
		TArray<int32> Char2Spells;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
		TArray<FStatusEffect> Char0Debuffs;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
		TArray<FStatusEffect> Char1Debuffs;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
		TArray<FStatusEffect> Char2Debuffs;

	UFUNCTION(BlueprintCallable)
		void RollHP(ESelectedCharacter SelectedChar, bool bIsFirstRoll);

	UFUNCTION(BlueprintCallable)
		void RollMana(ESelectedCharacter SelectedChar, bool bIsFirstRoll);

	UFUNCTION(BlueprintCallable)
		void RollAbilitiesFromStr(ESelectedCharacter SelectedChar);

	UFUNCTION(BlueprintCallable)
		void RollAbilitiesFromAgi(ESelectedCharacter SelectedChar);

	UFUNCTION(BlueprintCallable)
		void RollAbilitiesFromInt(ESelectedCharacter SelectedChar);

	UFUNCTION(BlueprintCallable)
		void RollAbilitiesFromWis(ESelectedCharacter SelectedChar);

	UFUNCTION(BlueprintCallable)
		void RollAbilitiesFromCha(ESelectedCharacter SelectedChar);

	UFUNCTION(BlueprintCallable)
		void SetBaseSecondaries(ESelectedCharacter SelectedChar);

	UFUNCTION(BlueprintCallable)
		void CombineAbilAndCharStats(ESelectedCharacter SelectedChar);


	UFUNCTION(BlueprintCallable)
		float CalcSpellcasting(FCharacterData CharData);

	UFUNCTION(BlueprintCallable)
		float CalcPicklocks(FCharacterData CharData);

private:

	UPROPERTY()
		int32 NumberOfRestTicks;

	UFUNCTION()
	bool CanSpellBeLearned(ESelectedCharacter SelectedChar, int32 SpellIndexToLearn);

	

	UFUNCTION()
	FSpell GetSpellData(int32 SpellIndex) const;

	UFUNCTION()
	void AddAbilitiesToStatAbils(TMap<int32, float> AbilsToAdd, ESelectedCharacter SelectedChar);

	UFUNCTION()
	void RemoveAbilitiesFromStatAbils(TMap<int32, float> AbilsToRemove, ESelectedCharacter SelectedChar);

	UFUNCTION()
	void AddAbilitiesToChar(TMap<int32, float> AbilsToAdd, ESelectedCharacter SelectedChar);

	UFUNCTION()
	void RemoveAbilitiesFromChar(TMap<int32, float> AbilsToRemove, ESelectedCharacter SelectedChar);


public:
	UFUNCTION(BlueprintCallable)
	FCharacterData CreateCharacterData(ERaces SelectedRace, EClasses SelectedClass, FName CharacterName, ESelectedCharacter SelectedCharacter);

	UFUNCTION(BlueprintCallable, BlueprintPure)
	FCharacterData GetCharacterData(ESelectedCharacter SelectedCharacter) const;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	int32 NumberOfPlayersInParty = 1;

	UFUNCTION()
	void GrantSomeExp(ESelectedCharacter SelectedCharacter, int32 ExpForEach);

	UFUNCTION()
	void ReduceTargetHPByAmount(ESelectedCharacter Target, int32 Amount);

	UFUNCTION()
	void AddItemStatsToChar(FItem ItemToAdd, ESelectedCharacter SelectedChar);

	UFUNCTION()
	void RemoveItemStatsFromChar(FItem ItemToRemove, ESelectedCharacter SelectedChar);

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	TArray<FString> AbilityStrings;

	UFUNCTION()
	void AddEncumberence(ESelectedCharacter SelectedChar, int32 AmountToAdd);

	UFUNCTION(BlueprintCallable)
	bool AddSpellToChar(ESelectedCharacter SelectedChar, int32 SpellIndexToAdd);

	UFUNCTION()
	void ReduceCastedSpellStats(ESelectedCharacter SelectedChar, int32 ManaUsed, int32 EnergyUsed);

	UFUNCTION()
	void ResetTurnStats(ESelectedCharacter SelectedChar);

	UFUNCTION()
	void SetPlayerHasCast(ESelectedCharacter SelectedChar);

	UFUNCTION()
	void RegenTick(ESelectedCharacter SelectedChar);

	UFUNCTION()
	FString GetRaceAsString(ERaces Race) const;

	UFUNCTION()
	FString GetClassAsString(EClasses Class) const;

	UFUNCTION(BlueprintCallable)
	void StopResting();

	UFUNCTION(BlueprintCallable)
	void RestTick(ESelectedCharacter SelectedChar);

	UFUNCTION()
	FRaceData GetRaceData(ERaces SelectedRace) const;

	UFUNCTION()
	FClassData GetClassData(EClasses SelectedClass) const;

	UFUNCTION(BlueprintCallable)
	void RaiseStatPoint(int32 MinStat, int32 MaxStat, int32 CurrentStat, int32 CPCurrent, int32& StatOut, int32& CPOut);

	UFUNCTION(BlueprintCallable)
	void LowerStatPoint(int32 MinStat, int32 MaxStat, int32 CurrentStat, int32 CPCurrent, int32& StatOut, int32& CPOut);

	UFUNCTION()
	void RollFinalStats(FCharacterData CharData, ESelectedCharacter SelectedChar, bool bIsFirstRoll);
};

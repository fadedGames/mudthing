// Copyright 2019, fadedGames - http://fadedGames.net

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "MainPlayer.generated.h"

UCLASS()
class MUDTHING_API AMainPlayer : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AMainPlayer();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	class USpringArmComponent* CameraBoom;
	
	class UCameraComponent* PlayerCamera;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	class ARoomPoint* CurrentRoomPoint;

	UFUNCTION(BlueprintImplementableEvent)
	void HideItemsOnGround();

};

// Copyright 2019, fadedGames - http://fadedGames.net


#include "StatSubsystem.h"

#include "ConstructorHelpers.h"
#include "Engine/DataTable.h"
#include "Engine/World.h"
#include "Player/MainPlayerController.h"
#include "Player/InventorySubsystem.h"
#include "UI/MainHUD.h"

UStatSubsystem::UStatSubsystem()
{
	static ConstructorHelpers::FObjectFinder<UDataTable> ClassDTObject(TEXT("DataTable'/Game/GameLogic/Data/DT_ClassData.DT_ClassData'"));
	ClassDT = ClassDTObject.Object;

	static ConstructorHelpers::FObjectFinder<UDataTable> RaceDTObject(TEXT("DataTable'/Game/GameLogic/Data/DT_RaceData.DT_RaceData'"));
	RaceDT = RaceDTObject.Object;

	static ConstructorHelpers::FObjectFinder<UDataTable> SpellDTObject(TEXT("DataTable'/Game/GameLogic/Data/DT_SpellData.DT_SpellData'"));
	SpellDT = SpellDTObject.Object;

	AbilityStrings = { "empty", "Damage", "AC Hit", "Resist Cold", "Max Damage", "Resist Fire", "Enslave", "AC DR", "Drain", "Shadow", "AC Blur", "Energy Level", "Summon", "Illumination",
	"Room Illumination", "Give Item", "NotUsed", "Damage -MR", "Heal", "Poison", "Cure Poison", "Poison Immunity", "Accuracy", "Affects Undead", "Protection From Evil", "Protection From Good",
	"Detect Magic", "Stealth", "Magical", "Punch", "Kick", "Bash", "Smash", "Killing Blow", "Dodge", "Jumpkick", "Magic Resist", "Picklocks", "Tracking", "Theivery", "Find Traps", "Disarm Traps",
	"Learn Spell", "Casts Spell", "Intellect", "Wisdom", "Strength", "Constitution", "Agility", "Charm", "Magebane Quest", "Anti-Magic", "Evil In Combat", "Blinding Light",
	"Illuminate Target", "Alter Light Duration", "Recharge Item", "See Hidden", "Crit Bonus", "Class OK", "Fear", "Affect Exit", "Evil Chance", "Experience", "Add CP", "Resist Stone",
	"Resist Lightning", "Quickness", "Slowness", "Max Mana", "Spellcasting", "Confusion", "Damaging Shield", "Dispell Magic", "Hold Person", "Paralyze", "Mute", "Perception", "Animal",
	"Mage Bind", "Affects Animals", "Freedom", "Cursed", "CURSED", "Remove Curse", "Shatter", "Quality", "Speed", "Alter HP", "Punch Accuracy", "Kick Accuracy", "Jumpkick Accuracy",
	"Punch Damage", "Kick Damage", "Jumpkick Damage", "Slay", "Encumberance Bonus", "Good Aligned", "Evil Aligned", "Alter DR Percent", "Loyal Item", "Confuse Message", "Racial Stealth",
	"Class Stealth", "Defense Modifier", "Accuracy2", "Accuracy3", "Blind User", "Affects Living", "Non-Living", "Not Good Aligned", "Not Evil Aligned", "Neutral Aligned", "Not Neutral Aligned",
	"Percent Spell Cast", "Description Message", "BS Accuracy", "BS Min Damage", "BS Max Damage", "Not Used", "Start Message", "Recharge", "Removes Spell", "HP Regen", "Negate Ability",
	"Ice Sorc Quest", "Good Quest", "Neutral Quest", "Evil Quest", "High Druid Quest", "Blood Champ Quest", "Red Dragon Quest", "Apparatus Quest", "Phoenix Quest", "Pyramid Quest",
	"Min Level", "Max Level", "Shock", "Room Visible", "Spell Immunity", "Teleport To Room", "Teleport To Map", "Hit Magical", "Clear Item", "Non-Magical Spell", "Mana Regen",
	"Monster Guards", "Resist Water", "Text Block", "Not Used", "Heal Mana", "End Cast", "Rune", "Kill Spell", "Not Used", "Death Text", "Quest Item", "Scatter Items", "Req to Hit",
	"Kai Bind", "Give Temp Spell", "Open Door", "Lore", "Spell Component", "Cast on End %", "Alter Spell Damage", "Alter Spell Duration", "Unequip Item", "Cannot Wear in Location",
	"Sleep", "Invisibility", "See Invisible", "Scry", "Steal Mana", "Steal HP to MP", "Steal MP to HP", "Spell Colors", "Shadowform", "Find Traps Value", "Picklocks Value", "Not Used",
	"Not Used", "Not Used", "Not Used", "Bad Attack", "Per Stealth", "Meditate" };
}

void UStatSubsystem::Initialize(FSubsystemCollectionBase& Collection)
{
	Super::Initialize(Collection);
	SetRefs();
	SetupBetterBunkData();
}

void UStatSubsystem::SetRefs()
{
	UGameInstance* GameInstance = GetGameInstance();
	if (!ensure(GameInstance != nullptr))
	{
		UE_LOG(LogTemp, Error, TEXT("Game instance not found!!"));
		return;
	}
	InventorySubSys = GameInstance->GetSubsystem<UInventorySubsystem>();
}

void UStatSubsystem::SetupBetterBunkData()
{
	TMap<int32, float> TempAbilities;
	ESelectedCharacter TempChar = ESelectedCharacter::SC_Character1;
	switch (NumberOfPlayersInParty)
	{
	case 1:
		TempChar = ESelectedCharacter::SC_Character0;
		CharacterData.Add(TempChar);
		CharacterData[TempChar].CharacterName = "Empty";
		CharacterData[TempChar].CharacterSlot = 0;
		CharacterData[TempChar].CharacterBaseStats.HP_Max = 0;
		CharacterData[TempChar].CharacterBaseStats.HP_Current = 0;
		CharacterData[TempChar].CharacterBaseStats.MP_Max = 0;
		CharacterData[TempChar].CharacterBaseStats.MP_Current = 0;
		CharacterData[TempChar].CharacterBaseStats.Race = ERaces::R_Human;
		CharacterData[TempChar].CharacterBaseStats.Class = EClasses::C_Warrior;
		CharacterData[TempChar].CharacterBaseStats.Level = 1;
		CharacterData[TempChar].CharacterSecondaryStats.Perception = 0;

		TempAbilities.Empty();
		TempAbilities.Add(0, 0.0f);
		TempAbilities.Add(77, 0.0f);

		CharacterData[TempChar].CharacterAbilities = TempAbilities;
		// add empty 2nd character
		TempChar = ESelectedCharacter::SC_Character1;
		CharacterData.Add(TempChar);
		CharacterData[TempChar].CharacterName = "Empty";
		CharacterData[TempChar].CharacterSlot = 0;
		CharacterData[TempChar].CharacterBaseStats.HP_Max = 0;
		CharacterData[TempChar].CharacterBaseStats.HP_Current = 0;
		CharacterData[TempChar].CharacterBaseStats.MP_Max = 0;
		CharacterData[TempChar].CharacterBaseStats.MP_Current = 0;
		CharacterData[TempChar].CharacterBaseStats.Race = ERaces::R_Human;
		CharacterData[TempChar].CharacterBaseStats.Class = EClasses::C_Warrior;
		CharacterData[TempChar].CharacterBaseStats.Level = 1;

		TempAbilities.Empty();
		TempAbilities.Add(0, 0.0f);

		CharacterData[TempChar].CharacterAbilities = TempAbilities;
		// add 3rd empty char
		TempChar = ESelectedCharacter::SC_Character2;
		CharacterData.Add(TempChar);
		CharacterData[TempChar].CharacterName = "Empty";
		CharacterData[TempChar].CharacterSlot = 0;
		CharacterData[TempChar].CharacterBaseStats.HP_Max = 0;
		CharacterData[TempChar].CharacterBaseStats.HP_Current = 0;
		CharacterData[TempChar].CharacterBaseStats.MP_Max = 0;
		CharacterData[TempChar].CharacterBaseStats.MP_Current = 0;
		CharacterData[TempChar].CharacterBaseStats.Race = ERaces::R_Human;
		CharacterData[TempChar].CharacterBaseStats.Class = EClasses::C_Warrior;
		CharacterData[TempChar].CharacterBaseStats.Level = 1;

		TempAbilities.Empty();
		TempAbilities.Add(0, 0.0f);

		CharacterData[TempChar].CharacterAbilities = TempAbilities;
		break;
	case 2:
		// add empty 3rd character
		TempChar = ESelectedCharacter::SC_Character2;
		CharacterData.Add(TempChar);
		CharacterData[TempChar].CharacterName = "Empty";
		CharacterData[TempChar].CharacterSlot = 0;
		CharacterData[TempChar].CharacterBaseStats.HP_Max = 0;
		CharacterData[TempChar].CharacterBaseStats.HP_Current = 0;
		CharacterData[TempChar].CharacterBaseStats.MP_Max = 0;
		CharacterData[TempChar].CharacterBaseStats.MP_Current = 0;
		CharacterData[TempChar].CharacterBaseStats.Race = ERaces::R_Human;
		CharacterData[TempChar].CharacterBaseStats.Class = EClasses::C_Warrior;
		CharacterData[TempChar].CharacterBaseStats.Level = 1;

		TempAbilities.Empty();
		TempAbilities.Add(0, 0.0f);

		CharacterData[TempChar].CharacterAbilities = TempAbilities;
	default:
		break;
	}
}

FCharacterData UStatSubsystem::CreateCharacterData(ERaces SelectedRace, EClasses SelectedClass, FName CharacterName, ESelectedCharacter SelectedCharacter)
{
	FClassData ClassData = GetClassData(SelectedClass);
	FRaceData RaceData = GetRaceData(SelectedRace);

	CharacterData[SelectedCharacter].CharacterBaseStats.Strength = RaceData.RaceStrMin;
	CharacterData[SelectedCharacter].CharacterBaseStats.Agility = RaceData.RaceAgiMin;
	CharacterData[SelectedCharacter].CharacterBaseStats.Wisdom = RaceData.RaceWisMin;
	CharacterData[SelectedCharacter].CharacterBaseStats.Intellect = RaceData.RaceIntMin;
	CharacterData[SelectedCharacter].CharacterBaseStats.Constitution = RaceData.RaceConMin;
	CharacterData[SelectedCharacter].CharacterBaseStats.Charm = RaceData.RaceChaMin;
	CharacterData[SelectedCharacter].CharacterBaseStats.Class = SelectedClass;
	CharacterData[SelectedCharacter].CharacterBaseStats.Race = SelectedRace;
	CharacterData[SelectedCharacter].CharacterBaseStats.Level = 1;
	CharacterData[SelectedCharacter].CharacterBaseStats.CP = 100;

	CharacterData[SelectedCharacter].CharacterName = CharacterName;


	TMap<int32, float> TempAbilities;
	TempAbilities.Empty();
	TempAbilities.Add(0, 0.0f);

	CharacterData[SelectedCharacter].CharacterAbilities = TempAbilities;

	AddAbilitiesToChar(ClassData.ClassAbilities, SelectedCharacter);
	AddAbilitiesToChar(RaceData.RaceAbilities, SelectedCharacter);

	//TODO Fix this for non-first roll
	if (SelectedCharacter == ESelectedCharacter::SC_Character0)
	{
		// new game, no others in party
		NumberOfPlayersInParty = 1;
		CharacterData[SelectedCharacter].CharacterSlot = 0;
	}
	return CharacterData[SelectedCharacter];
}

void UStatSubsystem::RaiseStatPoint(int32 MinStat, int32 MaxStat, int32 CurrentStat, int32 CPCurrent, int32& StatOut, int32& CPOut)
{
	int32 CPMultiplier = FMath::DivideAndRoundUp((CurrentStat - MinStat + 1), 10);
	if (CPMultiplier == 0) CPMultiplier = 1;

	if (CPCurrent == 0)
	{
		StatOut = CurrentStat;
		CPOut = 0;
		return;
	}

	if (CPCurrent >= CPMultiplier)
	{
		if (CurrentStat < MaxStat)
		{
			StatOut = CurrentStat + 1;
			CPOut = CPCurrent - CPMultiplier;
		}
		else
		{
			StatOut = CurrentStat;
			CPOut = CPCurrent;
		}
	}
	else
	{
		StatOut = CurrentStat;
		CPOut = CPCurrent;
	}
}

void UStatSubsystem::LowerStatPoint(int32 MinStat, int32 MaxStat, int32 CurrentStat, int32 CPCurrent, int32& StatOut, int32& CPOut)
{
	int32 CPMultiplier = FMath::DivideAndRoundUp((CurrentStat - MinStat), 10);
	if (CPMultiplier == 0) CPMultiplier = 1;

	if (CurrentStat > MinStat)
	{
		StatOut = CurrentStat - 1;
		CPOut = CPCurrent + CPMultiplier;
	}
	else
	{
		StatOut = CurrentStat;
		CPOut = CPCurrent;
	}
}

void UStatSubsystem::RollHP(ESelectedCharacter SelectedChar, bool bIsFirstRoll)
{
	FCharacterData CharData = GetCharacterData(SelectedChar);
	int32 HPRoll = 0;
	FRaceData RaceData = GetRaceData(CharData.CharacterBaseStats.Race);
	FClassData ClassData = GetClassData(CharData.CharacterBaseStats.Class);
	int32 CharLevel = CharData.CharacterBaseStats.Level;
	int32 BonusHPRoll = ClassData.ClassHPRollMax;
	float* BonusHPFromAbilities;
	int32 BonusHPToAdd = 0;
	if (!bIsFirstRoll)
	{
		BonusHPRoll = FMath::RandRange(ClassData.ClassHPRollMin, ClassData.ClassHPRollMax);
		// add hp roll to hp_roll array to use when training to eliminate lower hp after leveling
		CharData.CharacterBaseStats.HP_Roll.Add(BonusHPRoll);
	}
	else
	{
		// add full max hp roll to hp_roll array
		CharData.CharacterBaseStats.HP_Roll.Add(ClassData.ClassHPRollMax);
	}

	HPRoll = ((CharData.CharacterBaseStats.Constitution / 2) + CharLevel * BonusHPRoll) +
		(((CharData.CharacterBaseStats.Constitution - 50) * CharLevel) / 16);

	// add bonus HP from abil#88 alterHP
	BonusHPFromAbilities = CharData.CharacterAbilities.Find(88);
	if (BonusHPFromAbilities)
	{
		BonusHPToAdd = *BonusHPFromAbilities;
	}

	BonusHPToAdd += RaceData.RaceHPBonus;

	HPRoll += BonusHPToAdd;
	CharData.CharacterBaseStats.HP_Max = HPRoll;
	if (bIsFirstRoll)
	{
		CharData.CharacterBaseStats.HP_Current = HPRoll;
	}
	CharacterData.Add(SelectedChar, CharData);
}

void UStatSubsystem::RollMana(ESelectedCharacter SelectedChar, bool bIsFirstRoll)
{
	FCharacterData CharData = GetCharacterData(SelectedChar);
	int32 Mana = 6;
	FClassData ClassData = GetClassData(CharData.CharacterBaseStats.Class);

	float* bonusManaFromAbility;
	int32 bonusManaToAdd = 0;
	Mana += (CharData.CharacterBaseStats.Level * (ClassData.ClassMageryLevel * 2));

	if (CharData.CharacterBaseStats.Class == EClasses::C_Monk)
	{
		Mana = CharData.CharacterBaseStats.Level - 1;
	}

	bonusManaFromAbility = CharData.CharacterAbilities.Find(69);
	if (bonusManaFromAbility)
	{
		bonusManaToAdd = *bonusManaFromAbility;
	}

	Mana += bonusManaToAdd;
	if (bIsFirstRoll)
	{
		CharData.CharacterSecondaryStats.Perception = 0;
		CharData.CharacterBaseStats.MP_Current = Mana;
	}
	if (ClassData.ClassMageryLevel == 0)
	{
		Mana = -1;
	}
	CharData.CharacterBaseStats.MP_Max = Mana;
	CharacterData.Add(SelectedChar, CharData);
}

void UStatSubsystem::RollAbilitiesFromStr(ESelectedCharacter SelectedChar)
{
	FCharacterData CharData = GetCharacterData(SelectedChar);
	// all stat functions will be identical, look into making 1 master function
	TMap<int32, float> TempAbilMap;
	float AmountToAdd;
	AmountToAdd = (CharData.CharacterBaseStats.Strength - 50) / 10;
	TempAbilMap.Add(4, AmountToAdd);

	AddAbilitiesToStatAbils(TempAbilMap, SelectedChar);
	//CharacterData.Add(SelectedChar, CharData);
}

void UStatSubsystem::RollAbilitiesFromAgi(ESelectedCharacter SelectedChar)
{
	FCharacterData CharData = GetCharacterData(SelectedChar);
	TMap<int32, float> TempAbilMap;
	float AmountToAdd;
	AmountToAdd = (CharData.CharacterBaseStats.Agility - 50) / 10;
	TempAbilMap.Add(22, AmountToAdd);
	AmountToAdd *= (float)2.5f;
	TempAbilMap.Add(2, AmountToAdd);
	TempAbilMap.Add(27, AmountToAdd);

	AddAbilitiesToStatAbils(TempAbilMap, SelectedChar);
}

void UStatSubsystem::RollAbilitiesFromInt(ESelectedCharacter SelectedChar)
{
	FCharacterData CharData = GetCharacterData(SelectedChar);
	TMap<int32, float> TempAbilMap;
	float AmountToAdd;
	AmountToAdd = (CharData.CharacterBaseStats.Intellect - 50) / 10;
	TempAbilMap.Add(58, AmountToAdd);
	TempAbilMap.Add(77, (AmountToAdd * 6));
	TempAbilMap.Add(27, AmountToAdd);
	TempAbilMap.Add(39, (AmountToAdd * 1.5));
	TempAbilMap.Add(40, (AmountToAdd * 2));
	TempAbilMap.Add(38, (AmountToAdd * 2));
	TempAbilMap.Add(36, (AmountToAdd * 2));

	AddAbilitiesToStatAbils(TempAbilMap, SelectedChar);
}

void UStatSubsystem::RollAbilitiesFromWis(ESelectedCharacter SelectedChar)
{
	FCharacterData CharData = GetCharacterData(SelectedChar);
	TMap<int32, float> TempAbilMap;
	float AmountToAdd;
	// add AC
	AmountToAdd = (CharData.CharacterBaseStats.Wisdom - 50) / 10;
	TempAbilMap.Add(77, AmountToAdd * 2);
	TempAbilMap.Add(38, AmountToAdd);
	TempAbilMap.Add(36, AmountToAdd * 7);

	AddAbilitiesToStatAbils(TempAbilMap, SelectedChar);
}

void UStatSubsystem::RollAbilitiesFromCha(ESelectedCharacter SelectedChar)
{
	FCharacterData CharData = GetCharacterData(SelectedChar);
	TMap<int32, float> TempAbilMap;
	float AmountToAdd;
	// add AC
	AmountToAdd = (CharData.CharacterBaseStats.Charm - 50) / 10;

	TempAbilMap.Add(77, AmountToAdd);
	TempAbilMap.Add(27, AmountToAdd * 2.5);
	TempAbilMap.Add(39, AmountToAdd * 1.5);
	TempAbilMap.Add(40, AmountToAdd * 3);
	TempAbilMap.Add(38, AmountToAdd);
	TempAbilMap.Add(58, AmountToAdd);
	TempAbilMap.Add(22, AmountToAdd * 1.2);
	TempAbilMap.Add(34, AmountToAdd);
	AddAbilitiesToStatAbils(TempAbilMap, SelectedChar);
}

void UStatSubsystem::SetBaseSecondaries(ESelectedCharacter SelectedChar)
{
	FCharacterData CharData = GetCharacterData(SelectedChar);
	CharData.CharacterSecondaryStats.MartialArts = 16;

	// get total enc from inventory comp
	CharData.CharacterBaseStats.EncumberanceCurrent = InventorySubSys->GetTotalEncumbrance(SelectedChar);
	CharData.CharacterBaseStats.EncumberanceMax = (CharData.CharacterBaseStats.Strength * 48);
	float* LookupValue;
	float AmountToAdd = 0;
	// Martial arts roll done here
	AmountToAdd = (CharData.CharacterBaseStats.Agility - 50) / 10;
	CharData.CharacterSecondaryStats.MartialArts += (AmountToAdd * 3);

	LookupValue = CharData.CharacterAbilities.Find(13);
	if (LookupValue)
	{
		AmountToAdd = *LookupValue;
	}
	CharData.CharacterSecondaryStats.Illumination += AmountToAdd;

	LookupValue = CharData.CharacterAbilities.Find(77);
	if (LookupValue)
	{
		UE_LOG(LogTemp, Warning, TEXT("Adding to perception Current value: %f, adding %f"), CharData.CharacterSecondaryStats.Perception, *LookupValue);
		AmountToAdd = *LookupValue;
	}
	CharData.CharacterSecondaryStats.Perception += AmountToAdd;

	if (CharData.CharacterBaseStats.Class == EClasses::C_Missionary || CharData.CharacterBaseStats.Class == EClasses::C_Ninja || CharData.CharacterBaseStats.Class == EClasses::C_Rogue || CharData.CharacterBaseStats.Class == EClasses::C_Bard || CharData.CharacterBaseStats.Class == EClasses::C_Gypsy)
	{
		LookupValue = CharData.CharacterAbilities.Find(37);
		if (LookupValue)
		{
			AmountToAdd = *LookupValue;
		}
		CharData.CharacterSecondaryStats.Picklocks = CalcPicklocks(CharData) + AmountToAdd;
	}
	else
	{
		CharData.CharacterSecondaryStats.Picklocks = 0;
	}

	if (CharData.CharacterBaseStats.Class == EClasses::C_Missionary || CharData.CharacterBaseStats.Class == EClasses::C_Ninja || CharData.CharacterBaseStats.Class == EClasses::C_Rogue || CharData.CharacterBaseStats.Class == EClasses::C_Bard || CharData.CharacterBaseStats.Class == EClasses::C_Gypsy)
	{
		LookupValue = CharData.CharacterAbilities.Find(40);
		if (LookupValue)
		{
			AmountToAdd = *LookupValue;
		}
		else
		{
			CharData.CharacterAbilities.Add(40, 0);
		}
		CharData.CharacterSecondaryStats.Traps = 32 + AmountToAdd;
	}
	else
	{
		CharData.CharacterSecondaryStats.Traps = 0;
	}

	LookupValue = CharData.CharacterAbilities.Find(27);
	if (LookupValue)
	{
		AmountToAdd = *LookupValue;
		if (CharData.CharacterAbilities.Contains(102) || CharData.CharacterAbilities.Contains(103))
		{
			if (CharData.CharacterAbilities.Contains(102))
			{
				CharData.CharacterSecondaryStats.Stealth += 10;
			}
			if (CharData.CharacterAbilities.Contains(103))
			{
				CharData.CharacterSecondaryStats.Stealth += 25;
			}
		}
		else
		{
			CharData.CharacterSecondaryStats.Stealth = 0;
		}
	}


	if (CharData.CharacterBaseStats.Class == EClasses::C_Rogue || CharData.CharacterBaseStats.Class == EClasses::C_Bard || CharData.CharacterBaseStats.Class == EClasses::C_Gypsy)
	{
		LookupValue = CharData.CharacterAbilities.Find(39);
		if (LookupValue)
		{
			AmountToAdd = *LookupValue;
		}
		else
		{
			CharData.CharacterAbilities.Add(39, 0);
		}
		CharData.CharacterSecondaryStats.Thievery = 29 + AmountToAdd;
	}
	else
	{
		CharData.CharacterSecondaryStats.Thievery = 0;
	}

	if (CharData.CharacterBaseStats.Class == EClasses::C_Ranger || CharData.CharacterBaseStats.Class == EClasses::C_Ninja)
	{
		LookupValue = CharData.CharacterAbilities.Find(38);
		if (LookupValue)
		{
			AmountToAdd = *LookupValue;
		}
		else
		{
			CharData.CharacterAbilities.Add(38, 0);
		}
		CharData.CharacterSecondaryStats.Tracking = 30 + AmountToAdd;
	}
	else
	{
		CharData.CharacterSecondaryStats.Tracking = 0;
	}

	LookupValue = CharData.CharacterAbilities.Find(36);
	if (LookupValue)
	{
		AmountToAdd = *LookupValue;
		CharData.CharacterCombatStats.ResistMagic = 50 + AmountToAdd;
	}

	LookupValue = CharData.CharacterAbilities.Find(34);
	if (LookupValue)
	{
		AmountToAdd = *LookupValue;
		CharData.CharacterSecondaryStats.MartialArts += AmountToAdd;
	}

	LookupValue = CharData.CharacterAbilities.Find(58);
	if (LookupValue)
	{
		AmountToAdd = *LookupValue;
		CharData.CharacterSecondaryStats.MartialArts += AmountToAdd;
	}

	LookupValue = CharData.CharacterAbilities.Find(96);
	if (LookupValue)
	{
		AmountToAdd = *LookupValue;
		CharData.CharacterBaseStats.EncumberanceMax *= ((AmountToAdd / 100) + 1);
	}

	CharData.CharacterCombatStats.Spellcasting = CalcSpellcasting(CharData);

	if (CharData.CharacterCombatStats.Spellcasting != -1)
	{
		LookupValue = CharData.CharacterAbilities.Find(70);
		if (LookupValue)
		{
			AmountToAdd = *LookupValue;
			CharData.CharacterCombatStats.Spellcasting += AmountToAdd;
		}
	}
	CharacterData.Add(SelectedChar, CharData);
}

void UStatSubsystem::CombineAbilAndCharStats(ESelectedCharacter SelectedChar)
{
	switch (SelectedChar)
	{
	case ESelectedCharacter::SC_Character0:
		AddAbilitiesToChar(AbilitiesFromStats0, SelectedChar);
		break;
	case ESelectedCharacter::SC_Character1:
		AddAbilitiesToChar(AbilitiesFromStats1, SelectedChar);
		break;
	case ESelectedCharacter::SC_Character2:
		AddAbilitiesToChar(AbilitiesFromStats2, SelectedChar);
		break;
	}
}

void UStatSubsystem::RestTick(ESelectedCharacter SelectedChar)
{
	FCharacterData CharData = GetCharacterData(SelectedChar);
	int32 CalcdHPRegen;
	NumberOfRestTicks++;
	if (NumberOfRestTicks % 3 == 0)
	{
		FClassData ClassData = GetClassData(CharData.CharacterBaseStats.Class);

		// HP Regen
		int32 HPRegen = floor(((CharData.CharacterBaseStats.Level + 20) * CharData.CharacterBaseStats.Constitution) / 750);
		if (HPRegen < 1)
			HPRegen = 1;
		CharData.CharacterBaseStats.HP_Current = FMath::Clamp(CharData.CharacterBaseStats.HP_Current + HPRegen, -15, CharData.CharacterBaseStats.HP_Max);
		// End HP Regen

		// MP Regen
		int32 ManaStat = 0;
		int32 MagicLevel;

		if (CharData.CharacterBaseStats.Class == EClasses::C_Cleric || CharData.CharacterBaseStats.Class == EClasses::C_Missionary ||
			CharData.CharacterBaseStats.Class == EClasses::C_Paladin || CharData.CharacterBaseStats.Class == EClasses::C_Priest)
		{
			ManaStat = CharData.CharacterBaseStats.Wisdom;
		}

		if (CharData.CharacterBaseStats.Class == EClasses::C_Gypsy || CharData.CharacterBaseStats.Class == EClasses::C_Mage ||
			CharData.CharacterBaseStats.Class == EClasses::C_Warlock)
		{
			ManaStat = CharData.CharacterBaseStats.Intellect;
		}

		if (CharData.CharacterBaseStats.Class == EClasses::C_Druid || CharData.CharacterBaseStats.Class == EClasses::C_Ranger)
		{
			ManaStat = ((CharData.CharacterBaseStats.Intellect + CharData.CharacterBaseStats.Wisdom) / 2);
		}

		if (CharData.CharacterBaseStats.Class == EClasses::C_Bard)
		{
			ManaStat = CharData.CharacterBaseStats.Charm;
		}

		MagicLevel = ClassData.ClassMageryLevel;

		int32 MPRegen = floor(((CharData.CharacterBaseStats.Level + 20) * ManaStat) * (MagicLevel + 2) / 1650);

		CharData.CharacterBaseStats.MP_Current = FMath::Clamp(CharData.CharacterBaseStats.MP_Current + MPRegen, 0, CharData.CharacterBaseStats.MP_Max);
	}

	float* HPRegenAbilLookup = CharData.CharacterAbilities.Find(123);
	float HPRegenAbility = 1.0;
	if (HPRegenAbilLookup)
	{
		HPRegenAbility = *HPRegenAbilLookup;
	}
	int32 HPRegen = floor(((CharData.CharacterBaseStats.Level + 20) * CharData.CharacterBaseStats.Constitution) / 750);
	if (HPRegen < 1)
		HPRegen = 1;

	HPRegen *= 3;
	CalcdHPRegen = floor(((HPRegenAbility + 100) * HPRegen) / 100);

	CharData.CharacterBaseStats.HP_Current = FMath::Clamp(CharData.CharacterBaseStats.HP_Current + CalcdHPRegen, -15, CharData.CharacterBaseStats.HP_Max);

	CharacterData.Add(SelectedChar, CharData);
}

float UStatSubsystem::CalcSpellcasting(FCharacterData CharData)
{
	float FinalSpellcasting = 0.0f;
	FClassData ClassData = GetClassData(CharData.CharacterBaseStats.Class);
	int32 mageryLevel = ClassData.ClassMageryLevel;
	int32 charINT = CharData.CharacterBaseStats.Intellect;
	int32 charWis = CharData.CharacterBaseStats.Wisdom;
	int32 charCha = CharData.CharacterBaseStats.Charm;
	int32 charLevel = CharData.CharacterBaseStats.Level;


	switch (ClassData.ClassMageryType)
	{
	case EAbilMagicClass::AMC_Arcane:
		FinalSpellcasting = (((charINT * 3) + charWis) / 6) + (charLevel * 2) + (mageryLevel * 5);
		break;
	case EAbilMagicClass::AMC_Holy:
		FinalSpellcasting = (((charWis * 3) + charINT) / 6) + (charLevel * 2) + (mageryLevel * 5);
		break;
	case EAbilMagicClass::AMC_Ki:
		FinalSpellcasting = 500 + (charLevel * 2) + (mageryLevel * 5);
		break;
	case EAbilMagicClass::AMC_Lyrical:
		FinalSpellcasting = (((charCha * 3) + charWis) / 6) + (charLevel * 2) + (mageryLevel * 5);
		break;
	case EAbilMagicClass::AMC_Nature:
		FinalSpellcasting = ((charWis + charINT) / 3) + (charLevel * 2) + (mageryLevel * 5);
		break;
	case EAbilMagicClass::AMC_None:
		FinalSpellcasting = -1;
		break;
	}
	return FinalSpellcasting;
}

float UStatSubsystem::CalcPicklocks(FCharacterData CharData)
{
	float PicksRoll;
	float FinalPicks;
	int32 CharLevel = CharData.CharacterBaseStats.Level;
	int32 CharAgi = CharData.CharacterBaseStats.Agility;
	int32 CharInt = CharData.CharacterBaseStats.Intellect;

	if (CharLevel <= 15)
	{
		PicksRoll = (CharLevel * 2);
	}
	else
	{
		PicksRoll = (((CharLevel - 15) / 2) + 15) * 2;
	}
	FinalPicks = (((PicksRoll * 5) + (CharAgi + CharInt) * 2) / 7);
	return FinalPicks;
}

bool UStatSubsystem::CanSpellBeLearned(ESelectedCharacter SelectedChar, int32 SpellIndexToLearn)
{
	FSpell SpellToLearn = GetSpellData(SpellIndexToLearn);
	FCharacterData CharData = GetCharacterData(SelectedChar);
	TMap<EAbilMagicClass, int32> CharMagery;
	FClassData ClassData = 	GetClassData(CharData.CharacterBaseStats.Class);
	int32* ClassMageryLevel;

	ClassMageryLevel = (int32*)ClassData.ClassMageryLevel;
	EAbilMagicClass ClassMageryType = ClassData.ClassMageryType;
	if ((CharData.CharacterBaseStats.Level >= SpellToLearn.SpellRequiredLevel) && (SpellToLearn.SpellMageryType.Find(ClassMageryType) && (SpellToLearn.SpellMageryType.Find(ClassMageryType) >= ClassMageryLevel)))
		return true;
	return false;
}

FRaceData UStatSubsystem::GetRaceData(ERaces SelectedRace) const
{
	FString RaceString = GetRaceAsString(SelectedRace);
	FString ContextString = "";
	FName RaceName = FName(*RaceString);
	FRaceData* RaceData;
	if (!ensure(RaceDT != nullptr))
	{
		UE_LOG(LogTemp, Error, TEXT("Race DataTable is NULL!"));
		FRaceData EmptyRaceData;
		EmptyRaceData.Race = "Empty";
		return EmptyRaceData;
	}
	RaceData = RaceDT->FindRow<FRaceData>(RaceName, ContextString, false);
	return *RaceData;
}

FClassData UStatSubsystem::GetClassData(EClasses SelectedClass) const
{
	FString ClassString = GetClassAsString(SelectedClass);
	FString ContextString = "";
	FName ClassName = FName(*ClassString);
	FClassData* ClassData;
	if (!ensure(ClassDT != nullptr))
	{
		UE_LOG(LogTemp, Error, TEXT("Class DataTable is NULL!"));
		FClassData EmptyClassData;
		EmptyClassData.Class = "Empty";
		return EmptyClassData;
	}
	ClassData = ClassDT->FindRow<FClassData>(ClassName, ContextString, false);
	return *ClassData;
}

FSpell UStatSubsystem::GetSpellData(int32 SpellIndex) const
{
	FString SpellString = FString::FromInt(SpellIndex);
	FName SpellName = FName(*SpellString);
	FString ContextString = "";
	FSpell* SpellData;
	if (!ensure(SpellDT != nullptr))
	{
		UE_LOG(LogTemp, Error, TEXT("Spell DataTable is NULL!"));
		FSpell EmptySpell;
		EmptySpell.SpellName = "Empty";
		return EmptySpell;
	}
	SpellData = SpellDT->FindRow<FSpell>(SpellName, ContextString, false);
	return *SpellData;
}

void UStatSubsystem::AddAbilitiesToStatAbils(TMap<int32, float> AbilsToAdd, ESelectedCharacter SelectedChar)
{
	TMap<int32, float> TempAbilMap;
	switch (SelectedChar)
	{
	case ESelectedCharacter::SC_Character0:
		TempAbilMap = AbilitiesFromStats0;
		break;
	case ESelectedCharacter::SC_Character1:
		TempAbilMap = AbilitiesFromStats1;
		break;
	case ESelectedCharacter::SC_Character2:
		TempAbilMap = AbilitiesFromStats2;
		break;
	}
	for (auto AbilKey = AbilsToAdd.CreateConstIterator(); AbilKey; ++AbilKey)
	{
		if (TempAbilMap.Contains(AbilKey->Key))
		{
			float* TempFloat = TempAbilMap.Find(AbilKey->Key);
			*TempFloat += AbilKey->Value;
			TempAbilMap.Add(AbilKey->Key, *TempFloat);
		}
		else
		{
			TempAbilMap.Add(AbilKey->Key, AbilKey->Value);
		}
	}
	switch (SelectedChar)
	{
	case ESelectedCharacter::SC_Character0:
		AbilitiesFromStats0 = TempAbilMap;
		break;
	case ESelectedCharacter::SC_Character1:
		AbilitiesFromStats1 = TempAbilMap;
		break;
	case ESelectedCharacter::SC_Character2:
		AbilitiesFromStats2 = TempAbilMap;
		break;
	}
}

void UStatSubsystem::RemoveAbilitiesFromStatAbils(TMap<int32, float> AbilsToRemove, ESelectedCharacter SelectedChar)
{
	TMap<int32, float> TempAbilMap;
	switch (SelectedChar)
	{
	case ESelectedCharacter::SC_Character0:
		TempAbilMap = AbilitiesFromStats0;
		break;
	case ESelectedCharacter::SC_Character1:
		TempAbilMap = AbilitiesFromStats1;
		break;
	case ESelectedCharacter::SC_Character2:
		TempAbilMap = AbilitiesFromStats2;
		break;
	}
	for (auto AbilKey = AbilsToRemove.CreateConstIterator(); AbilKey; ++AbilKey)
	{
		float* TempFloat = TempAbilMap.Find(AbilKey->Key);
		*TempFloat -= AbilKey->Value;
		TempAbilMap.Add(AbilKey->Key, *TempFloat);
	}
	switch (SelectedChar)
	{
	case ESelectedCharacter::SC_Character0:
		AbilitiesFromStats0 = TempAbilMap;
		break;
	case ESelectedCharacter::SC_Character1:
		AbilitiesFromStats1 = TempAbilMap;
		break;
	case ESelectedCharacter::SC_Character2:
		AbilitiesFromStats2 = TempAbilMap;
		break;
	}
}

void UStatSubsystem::AddAbilitiesToChar(TMap<int32, float> AbilsToAdd, ESelectedCharacter SelectedChar)
{
	FCharacterData CharData = GetCharacterData(SelectedChar);
	for (auto AbilKey = AbilsToAdd.CreateConstIterator(); AbilKey; ++AbilKey)
	{
		if (CharData.CharacterAbilities.Contains(AbilKey->Key))
		{
			float* TempFloat = CharData.CharacterAbilities.Find(AbilKey->Key);
			*TempFloat += AbilKey->Value;
			CharData.CharacterAbilities.Add(AbilKey->Key, *TempFloat);
		}
		else
		{
			CharData.CharacterAbilities.Add(AbilKey->Key, AbilKey->Value);
		}
	}
	CharacterData.Add(SelectedChar, CharData);
}

void UStatSubsystem::RemoveAbilitiesFromChar(TMap<int32, float> AbilsToRemove, ESelectedCharacter SelectedChar)
{
	FCharacterData CharData = GetCharacterData(SelectedChar);
	for (auto AbilKey = AbilsToRemove.CreateConstIterator(); AbilKey; ++AbilKey)
	{
		float* TempFloat = CharacterData[SelectedChar].CharacterAbilities.Find(AbilKey->Key);
		*TempFloat -= AbilKey->Value;
		CharData.CharacterAbilities.Add(AbilKey->Key, *TempFloat);
	}
	CharacterData.Add(SelectedChar, CharData);
}

FCharacterData UStatSubsystem::GetCharacterData(ESelectedCharacter SelectedCharacter) const
{
	return CharacterData[SelectedCharacter];
	/*UE_LOG(LogTemp, Error, TEXT("CharacterData length: %i"), CharacterData.Num());
	FCharacterData* HoldingCharData;
	HoldingCharData = CharacterData.Find(SelectedCharacter);
	if (HoldingCharData)
	{
		FString temp = HoldingCharData->CharacterName.ToString();
		UE_LOG(LogTemp, Error, TEXT("Character %s loaded"), *temp);
		return *HoldingCharData;
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("Character data NOT loaded"));
		FCharacterData EmptyChar;
		EmptyChar.CharacterName = "Empty";
		return EmptyChar;
	}*/
}

void UStatSubsystem::GrantSomeExp(ESelectedCharacter SelectedCharacter, int32 ExpForEach)
{
	CharacterData[SelectedCharacter].CharacterBaseStats.Experience += ExpForEach;
}

void UStatSubsystem::ReduceTargetHPByAmount(ESelectedCharacter Target, int32 Amount)
{
	CharacterData[Target].CharacterBaseStats.HP_Current = FMath::Clamp(CharacterData[Target].CharacterBaseStats.HP_Current - Amount, -15, CharacterData[Target].CharacterBaseStats.HP_Max);
	if (CharacterData[Target].CharacterBaseStats.HP_Current <= 0)
	{
		CharacterData[Target].bIsDropped = true;
	}
}

void UStatSubsystem::AddItemStatsToChar(FItem ItemToAdd, ESelectedCharacter SelectedChar)
{
	FCharacterData CharData = GetCharacterData(SelectedChar);
	// Stats First
	CharData.CharacterBaseStats.Agility += ItemToAdd.ItemBaseStats.Agility;
	CharData.CharacterBaseStats.Strength += ItemToAdd.ItemBaseStats.Strength;
	CharData.CharacterBaseStats.Charm += ItemToAdd.ItemBaseStats.Charm;
	CharData.CharacterBaseStats.Intellect += ItemToAdd.ItemBaseStats.Intellect;
	CharData.CharacterBaseStats.Constitution += ItemToAdd.ItemBaseStats.Constitution;
	CharData.CharacterBaseStats.Wisdom += ItemToAdd.ItemBaseStats.Wisdom;

	// Secondary Stats
	CharData.CharacterSecondaryStats.Illumination += ItemToAdd.ItemSecondaryStats.Illumination;
	CharData.CharacterSecondaryStats.MartialArts += ItemToAdd.ItemSecondaryStats.MartialArts;
	CharData.CharacterSecondaryStats.Perception += ItemToAdd.ItemSecondaryStats.Perception;
	CharData.CharacterSecondaryStats.Picklocks += ItemToAdd.ItemSecondaryStats.Picklocks;
	CharData.CharacterSecondaryStats.Stealth += ItemToAdd.ItemSecondaryStats.Stealth;
	CharData.CharacterSecondaryStats.Traps += ItemToAdd.ItemSecondaryStats.Traps;

	// Combat Stats
	CharData.CharacterCombatStats.AC_DR += ItemToAdd.ItemCombatStats.AC_DR;
	CharData.CharacterCombatStats.AC_Hit += ItemToAdd.ItemCombatStats.AC_Hit;
	CharData.CharacterCombatStats.Accuracy += ItemToAdd.ItemCombatStats.Accuracy;
	CharData.CharacterCombatStats.Backstab += ItemToAdd.ItemCombatStats.Backstab;
	CharData.CharacterCombatStats.ResistCold += ItemToAdd.ItemCombatStats.ResistCold;
	CharData.CharacterCombatStats.ResistEvil += ItemToAdd.ItemCombatStats.ResistEvil;
	CharData.CharacterCombatStats.ResistFire += ItemToAdd.ItemCombatStats.ResistFire;
	CharData.CharacterCombatStats.ResistGood += ItemToAdd.ItemCombatStats.ResistGood;
	CharData.CharacterCombatStats.ResistLightning += ItemToAdd.ItemCombatStats.ResistLightning;
	CharData.CharacterCombatStats.ResistMagic += ItemToAdd.ItemCombatStats.ResistMagic;
	CharData.CharacterCombatStats.ResistPoison += ItemToAdd.ItemCombatStats.ResistPoison;
	CharData.CharacterCombatStats.ResistStone += ItemToAdd.ItemCombatStats.ResistStone;

	// Abilities
	AddAbilitiesToChar(ItemToAdd.ItemAbilities, SelectedChar);

	CharacterData.Add(SelectedChar, CharData);
}

void UStatSubsystem::RemoveItemStatsFromChar(FItem ItemToRemove, ESelectedCharacter SelectedChar)
{
	FCharacterData CharData = GetCharacterData(SelectedChar);
	// Ability Stats
	CharData.CharacterBaseStats.Agility -= ItemToRemove.ItemBaseStats.Agility;
	CharData.CharacterBaseStats.Strength -= ItemToRemove.ItemBaseStats.Strength;
	CharData.CharacterBaseStats.Charm -= ItemToRemove.ItemBaseStats.Charm;
	CharData.CharacterBaseStats.Intellect -= ItemToRemove.ItemBaseStats.Intellect;
	CharData.CharacterBaseStats.Constitution -= ItemToRemove.ItemBaseStats.Constitution;
	CharData.CharacterBaseStats.Wisdom -= ItemToRemove.ItemBaseStats.Wisdom;

	// Secondary Stats
	CharData.CharacterSecondaryStats.Illumination -= ItemToRemove.ItemSecondaryStats.Illumination;
	CharData.CharacterSecondaryStats.MartialArts -= ItemToRemove.ItemSecondaryStats.MartialArts;
	CharData.CharacterSecondaryStats.Perception -= ItemToRemove.ItemSecondaryStats.Perception;
	CharData.CharacterSecondaryStats.Picklocks -= ItemToRemove.ItemSecondaryStats.Picklocks;
	CharData.CharacterSecondaryStats.Stealth -= ItemToRemove.ItemSecondaryStats.Stealth;
	CharData.CharacterSecondaryStats.Traps -= ItemToRemove.ItemSecondaryStats.Traps;

	// Combat Stats
	CharData.CharacterCombatStats.AC_DR -= ItemToRemove.ItemCombatStats.AC_DR;
	CharData.CharacterCombatStats.AC_Hit -= ItemToRemove.ItemCombatStats.AC_Hit;
	CharData.CharacterCombatStats.Accuracy -= ItemToRemove.ItemCombatStats.Accuracy;
	CharData.CharacterCombatStats.Backstab -= ItemToRemove.ItemCombatStats.Backstab;
	CharData.CharacterCombatStats.ResistCold -= ItemToRemove.ItemCombatStats.ResistCold;
	CharData.CharacterCombatStats.ResistEvil -= ItemToRemove.ItemCombatStats.ResistEvil;
	CharData.CharacterCombatStats.ResistFire -= ItemToRemove.ItemCombatStats.ResistFire;
	CharData.CharacterCombatStats.ResistGood -= ItemToRemove.ItemCombatStats.ResistGood;
	CharData.CharacterCombatStats.ResistLightning -= ItemToRemove.ItemCombatStats.ResistLightning;
	CharData.CharacterCombatStats.ResistMagic -= ItemToRemove.ItemCombatStats.ResistMagic;
	CharData.CharacterCombatStats.ResistPoison -= ItemToRemove.ItemCombatStats.ResistPoison;
	CharData.CharacterCombatStats.ResistStone -= ItemToRemove.ItemCombatStats.ResistStone;

	// Abilities
	RemoveAbilitiesFromChar(ItemToRemove.ItemAbilities, SelectedChar);
	CharacterData.Add(SelectedChar, CharData);
}

void UStatSubsystem::AddEncumberence(ESelectedCharacter SelectedChar, int32 AmountToAdd)
{
	FCharacterData CharData = GetCharacterData(SelectedChar);
	CharData.CharacterBaseStats.EncumberanceCurrent += AmountToAdd;
	CharacterData.Add(SelectedChar, CharData);
}

bool UStatSubsystem::AddSpellToChar(ESelectedCharacter SelectedChar, int32 SpellIndexToAdd)
{
	switch (SelectedChar)
	{
	case ESelectedCharacter::SC_Character0:
		if (CanSpellBeLearned(SelectedChar, SpellIndexToAdd))
		{
			Char0Spells.Add(SpellIndexToAdd);
			return true;
		}
		else
		{
			return false;
		}
		break;
	case ESelectedCharacter::SC_Character1:
		if (CanSpellBeLearned(SelectedChar, SpellIndexToAdd))
		{
			Char1Spells.Add(SpellIndexToAdd);
			return true;
		}
		else
		{
			return false;
		}
		break;
	case ESelectedCharacter::SC_Character2:
		if (CanSpellBeLearned(SelectedChar, SpellIndexToAdd))
		{
			Char2Spells.Add(SpellIndexToAdd);
			return true;
		}
		else
		{
			return false;
		}
		break;
	}
	return false;
}

void UStatSubsystem::ReduceCastedSpellStats(ESelectedCharacter SelectedChar, int32 ManaUsed, int32 EnergyUsed)
{
	FCharacterData CharData = GetCharacterData(SelectedChar);
	CharData.CharacterBaseStats.MP_Current -= ManaUsed;
	CharData.CharacterCombatStats.Energy -= EnergyUsed;
	CharacterData.Add(SelectedChar, CharData);
}

void UStatSubsystem::ResetTurnStats(ESelectedCharacter SelectedChar)
{
	FCharacterData CharData = GetCharacterData(SelectedChar);
	CharData.CharacterCombatStats.Energy = 1000;
	CharData.bHasCastSpell = false;
	CharData.bHasTakenTurn = false;
	CharacterData.Add(SelectedChar, CharData);
}

void UStatSubsystem::SetPlayerHasCast(ESelectedCharacter SelectedChar)
{
	FCharacterData CharData = GetCharacterData(SelectedChar);
	CharData.bHasCastSpell = true;
	CharacterData.Add(SelectedChar, CharData);
}

void UStatSubsystem::RegenTick(ESelectedCharacter SelectedChar)
{
	FCharacterData CharData = GetCharacterData(SelectedChar);
	FClassData ClassData = GetClassData(CharData.CharacterBaseStats.Class);

	// HP Regen
	int32 HPRegen = floor(((CharData.CharacterBaseStats.Level + 20) * CharData.CharacterBaseStats.Constitution) / 750);
	if (HPRegen < 1)
		HPRegen = 1;
	CharData.CharacterBaseStats.HP_Current = FMath::Clamp(CharData.CharacterBaseStats.HP_Current + HPRegen, -15, CharData.CharacterBaseStats.HP_Max);
	// End HP Regen

	// MP Regen
	int32 ManaStat = 0;
	int32 MagicLevel;

	if (CharData.CharacterBaseStats.Class == EClasses::C_Cleric || CharData.CharacterBaseStats.Class == EClasses::C_Missionary ||
		CharData.CharacterBaseStats.Class == EClasses::C_Paladin || CharData.CharacterBaseStats.Class == EClasses::C_Priest)
	{
		ManaStat = CharData.CharacterBaseStats.Wisdom;
	}

	if (CharData.CharacterBaseStats.Class == EClasses::C_Gypsy || CharData.CharacterBaseStats.Class == EClasses::C_Mage ||
		CharData.CharacterBaseStats.Class == EClasses::C_Warlock)
	{
		ManaStat = CharData.CharacterBaseStats.Intellect;
	}

	if (CharData.CharacterBaseStats.Class == EClasses::C_Druid || CharData.CharacterBaseStats.Class == EClasses::C_Ranger)
	{
		ManaStat = ((CharData.CharacterBaseStats.Intellect + CharData.CharacterBaseStats.Wisdom) / 2);
	}

	if (CharData.CharacterBaseStats.Class == EClasses::C_Bard)
	{
		ManaStat = CharData.CharacterBaseStats.Charm;
	}

	MagicLevel = ClassData.ClassMageryLevel;

	int32 MPRegen = floor(((CharData.CharacterBaseStats.Level + 20) * ManaStat) * (MagicLevel + 2) / 1650);

	CharData.CharacterBaseStats.MP_Current = FMath::Clamp(CharData.CharacterBaseStats.MP_Current + MPRegen, 0, CharData.CharacterBaseStats.MP_Max);
	UE_LOG(LogTemp, Warning, TEXT("Mana tick: %i"), FMath::Clamp(CharData.CharacterBaseStats.MP_Current + MPRegen, 0, CharData.CharacterBaseStats.MP_Max));
	// End MP Regen

	CharacterData.Add(SelectedChar, CharData);
}

FString UStatSubsystem::GetRaceAsString(ERaces Race) const
{
	switch (Race)
	{
	case ERaces::R_Human:
		return "1";
		break;
	case ERaces::R_Dwarf:
		return "2";
		break;
	case ERaces::R_Gnome:
		return "3";
		break;
	case ERaces::R_Halfling:
		return "4";
		break;
	case ERaces::R_Elf:
		return "5";
		break;
	case ERaces::R_HalfElf:
		return "6";
		break;
	case ERaces::R_DarkElf:
		return "7";
		break;
	case ERaces::R_HalfOrc:
		return "8";
		break;
	case ERaces::R_Goblin:
		return "9";
		break;
	case ERaces::R_HalfOgre:
		return "10";
		break;
	case ERaces::R_Kang:
		return "11";
		break;
	case ERaces::R_Nekojin:
		return "12";
		break;
	case ERaces::R_GauntOne:
		return "13";
		break;
	}
	return "0";
}

FString UStatSubsystem::GetClassAsString(EClasses Class) const
{
	switch (Class)
	{
	case EClasses::C_Warrior:
		return "1";
		break;
	case EClasses::C_Rogue:
		return "8";
		break;
	case EClasses::C_Monk:
		return "15";
		break;
	case EClasses::C_Ninja:
		return "7";
		break;
	case EClasses::C_Witchunter:
		return "2";
		break;
	case EClasses::C_Mage:
		return "12";
		break;
	case EClasses::C_Priest:
		return "5";
		break;
	case EClasses::C_Druid:
		return "13";
		break;
	case EClasses::C_Bard:
		return "9";
		break;
	case EClasses::C_Paladin:
		return "3";
		break;
	case EClasses::C_Cleric:
		return "4";
		break;
	case EClasses::C_Missionary:
		return "6";
		break;
	case EClasses::C_Gypsy:
		return "10";
		break;
	case EClasses::C_Warlock:
		return "11";
		break;
	case EClasses::C_Ranger:
		return "14";
		break;
	}
	return "Empty";
}

void UStatSubsystem::StopResting()
{
	UWorld* World = GetWorld();
	if (!ensure(World != nullptr)) return;

	AMainPlayerController* MPC = Cast<AMainPlayerController>(World->GetFirstPlayerController());
	if (MPC)
	{
		MainHUD = Cast<AMainHUD>(MPC->GetHUD());
	}
	if (bIsCurrentlyResting)
	{
		if (bStopResting == false)
		{
			NumberOfRestTicks = 0;
			MainHUD->ReportToUI("You are no longer resting.", false);
			bStopResting = true;
			MainHUD->bIsInRestMode = false;
			MainHUD->ToggleCombatUI();
			MainHUD->ToggleMainUI();
			bIsCurrentlyResting = false;
		}
	}
}

void UStatSubsystem::RollFinalStats(FCharacterData CharData, ESelectedCharacter SelectedChar, bool bIsFirstRoll)
{
	CharacterData.Add(SelectedChar, CharData);
	RollHP(SelectedChar, bIsFirstRoll);
	RollMana(SelectedChar, bIsFirstRoll);
	RollAbilitiesFromStr(SelectedChar);
	RollAbilitiesFromAgi(SelectedChar);
	RollAbilitiesFromInt(SelectedChar);
	RollAbilitiesFromWis(SelectedChar);
	RollAbilitiesFromCha(SelectedChar);
	CombineAbilAndCharStats(SelectedChar);
	SetBaseSecondaries(SelectedChar);
}
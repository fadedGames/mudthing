// Copyright 2019, fadedGames - http://fadedGames.net


#include "MainPlayerController.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "Engine/World.h"
#include "MainPlayer.h"
#include "Movement/RoomPoint.h"
#include "Components/SphereComponent.h"
#include "Combat/CombatManager.h"
#include "UI/MainHUD.h"
#include "Player/MainPlayer.h"
#include "Player/StatSubsystem.h"
#include "ConstructorHelpers.h"


AMainPlayerController::AMainPlayerController()
{
	//auto MainHUDBPtemp = ConstructorHelpers::FObjectFinder<AMainHUD>(TEXT("Blueprint'/Game/UI/BP_MainHUD.BP_MainHUD'"));
	//MainHUDBP = MainHUDBPtemp.Object;
}

void AMainPlayerController::BeginPlay()
{
	Super::BeginPlay();
	MainHUD = Cast<AMainHUD>(GetHUD());
	
	UGameInstance* GameInstance = Cast<UGameInstance>(GetGameInstance());
	StatSubSys = GameInstance->GetSubsystem<UStatSubsystem>();
	
	if (!bIsInMenuSystem)
	{
		if (MainHUD)
		{
			MainHUD->ToggleMainUI();
			MainHUD->ToggleTextLog();
		}
	}
}

void AMainPlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);
}

void AMainPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAction("MoveForward", IE_Pressed, this, &AMainPlayerController::MoveToTargetPoint);
	InputComponent->BindAction("TurnRight", IE_Pressed, this, &AMainPlayerController::TurnRight);
	InputComponent->BindAction("TurnLeft", IE_Pressed, this, &AMainPlayerController::TurnLeft);
	InputComponent->BindAction("TurnAround", IE_Pressed, this, &AMainPlayerController::TurnAround);
	InputComponent->BindAction("TurnRight45", IE_Pressed, this, &AMainPlayerController::TurnRight45);
	InputComponent->BindAction("TurnLeft45", IE_Pressed, this, &AMainPlayerController::TurnLeft45);
	InputComponent->BindAction("StatusScreen", IE_Pressed, this, &AMainPlayerController::ToggleStatusScreen);
	InputComponent->BindAction("TabUp", IE_Pressed, this, &AMainPlayerController::TabUp);
	InputComponent->BindAction("TabDown", IE_Pressed, this, &AMainPlayerController::TabDown);
	/// Uncomment to re-enable moving backwards
	//InputComponent->BindAction("MoveBackwards", IE_Pressed, this, &AMainPlayerController::MoveBackwards);
}

#pragma region Movement
void AMainPlayerController::MoveToTargetPoint()
{
	if (bMovementAllowed)
	{
		if (!CheckIfMoving())
		{
			if (AMainPlayer* MainPlayer = Cast<AMainPlayer>(GetPawn()))
			{
				// build vars for sweep trace
				FHitResult outHit;
				FVector startPoint = MainPlayer->GetActorLocation() + MainPlayer->GetActorForwardVector() * 450;
				FVector endPoint = MainPlayer->GetActorLocation() + MainPlayer->GetActorForwardVector() * 1400;
				FQuat quat = MainPlayer->GetActorRotation().Quaternion();
				FCollisionShape shape;
				shape.SetSphere(300.0f);
				FCollisionQueryParams params;
				//endPoint.Z += 100;
				if (GetWorld()->SweepSingleByObjectType(outHit, startPoint, endPoint, quat, ECC_Pawn, shape, params))
				{
					return;
				}
				else if (GetWorld()->SweepSingleByObjectType(outHit, startPoint, endPoint, quat, ECC_Vehicle, shape, params))
				{
					//UE_LOG(LogTemp, Warning, TEXT("Hit: %s"), *outHit.GetActor()->GetName())
					StatSubSys->StopResting();
					UAIBlueprintHelperLibrary::SimpleMoveToActor(this, outHit.GetActor());
					StepsTaken++;
					if (StepsTaken % 2 == 0)
					{
						PassiveRegenTick();
					}
				}
			}
		}
	}
}
/// This function is currently inactive
void AMainPlayerController::MoveBackwards()
{
	if (bMovementAllowed)
	{
		if (!CheckIfMoving())
		{
			if (AMainPlayer* MainPlayer = Cast<AMainPlayer>(GetPawn()))
			{
				// build vars for sweep trace
				FHitResult outHit;
				FVector startPoint = MainPlayer->GetActorLocation() + MainPlayer->GetActorForwardVector() * -300;
				FVector endPoint = MainPlayer->GetActorLocation() + MainPlayer->GetActorForwardVector() * -1500;
				FQuat quat = MainPlayer->GetActorRotation().Quaternion();
				FCollisionShape shape;
				shape.SetSphere(200.0f);

				if (GetWorld()->SweepSingleByObjectType(outHit, startPoint, endPoint, quat, ECC_Vehicle, shape))
				{
					StatSubSys->StopResting();
					UAIBlueprintHelperLibrary::SimpleMoveToActor(this, outHit.GetActor());;
				}
			}
		}
	}
}

void AMainPlayerController::TurnRight()
{
	if (bMovementAllowed)
	{
		RotateCharacter(90);
	}
}

void AMainPlayerController::TurnLeft()
{
	if (bMovementAllowed)
	{
		RotateCharacter(-90);
	}
}

void AMainPlayerController::TurnAround()
{
	if (bMovementAllowed)
	{
		RotateCharacter(-180);
	}
}

void AMainPlayerController::TurnRight45()
{
	if (bMovementAllowed)
	{
		RotateCharacter(45);
	}
}

void AMainPlayerController::TurnLeft45()
{
	if (bMovementAllowed)
	{
		RotateCharacter(-45);
	}
}

void AMainPlayerController::RotateCharacter(int RotationAmount)
{
	if (!CheckIfMoving())
	{
		if (AMainPlayer* MainPlayer = Cast<AMainPlayer>(GetPawn()))
		{
			FRotator playerRotation = MainPlayer->GetActorRotation();
			playerRotation.Yaw += RotationAmount;
			MainPlayer->SetActorRotation(playerRotation);
			StatSubSys->StopResting();
		}
	}
}

bool AMainPlayerController::CheckIfMoving()
{
	if (AMainPlayer * MainPlayer = Cast<AMainPlayer>(GetPawn()))
	{
		FVector playerVelocity = MainPlayer->GetVelocity();
		// check if player is moving
		if (FMath::Abs(playerVelocity.X) > 0.0f || FMath::Abs(playerVelocity.Y) > 0.0f)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	UE_LOG(LogTemp, Error, TEXT("Cast to MainPlayer FAILED!"));
	return true;
}
#pragma endregion Movement

void AMainPlayerController::ToggleCombat(TArray<int32> EnemyGroup)
{
	UE_LOG(LogTemp, Warning, TEXT("Combat Toggled"));
	
	/// This spawns the c++ variant of the class
	/*if (AMainPlayer * MainPlayer = Cast<AMainPlayer>(GetPawn()))
	{
		FVector spawnLocation;
		FRotator spawnRotator;
		FActorSpawnParameters spawnInfo;
		spawnLocation = MainPlayer->GetActorLocation();
		spawnRotator = MainPlayer->GetControlRotation();
		CombatManager = GetWorld()->SpawnActor<ACombatManager>(ACombatManager::StaticClass(), spawnLocation, spawnRotator, spawnInfo);
	}*/

	if (MainHUD->bIsInRestMode)
	{
		MainHUD->ToggleMainUI();
		MainHUD->ToggleCombatUI();
	}
	else
	{
		if (CombatManagerBP)
		{
			if (AMainPlayer * MainPlayer = Cast<AMainPlayer>(GetPawn()))
			{
				if (!bInCombat)
				{
					bMovementAllowed = false;
					FActorSpawnParameters spawnParams;

					CombatManagerBP_REF = GetWorld()->SpawnActor<ACombatManager>(CombatManagerBP, MainPlayer->GetTransform(), spawnParams);

					MainHUD->ToggleMainUI();
					MainHUD->ToggleCombatUI();
					FString tempString = "*Combat Engaged*";
					MainHUD->ReportToUI(tempString, false);
					bInCombat = true;
					CombatManagerBP_REF->EnemyGroupArray = EnemyGroup;
					CombatManagerBP_REF->SpawnTheEnemies();
				}
				else
				{
					FString tempString = "*Combat Off*";
					MainHUD->ReportToUI(tempString, false);
					CombatManagerBP_REF->RemoveEnemies();
					CombatManagerBP_REF->Destroy();
					MainHUD->ToggleCombatUI();
					MainHUD->ToggleMainUI();
					bInCombat = false;
					bMovementAllowed = true;
				}
			}
		}
	}
}

#pragma region UI_Interaction
void AMainPlayerController::ToggleStatusScreen()
{
	if (bMovementAllowed)
	{
		if (bInCombat)
			return;
		else if (!CheckIfMoving())
		{
			bMovementAllowed = false;
			//UE_LOG(LogTemp, Warning, TEXT("Not paused, pausing"));
			MainHUD->ToggleStatusScreen();
		}
	}
	else
	{
		if (bInCombat)
			return;
		if (!CheckIfMoving())
		{
			bMovementAllowed = true;
			//UE_LOG(LogTemp, Warning, TEXT("Paused, Unpausing"));
			MainHUD->ToggleStatusScreen();
		}
	}
	
}

void AMainPlayerController::TabUp()
{
	MainHUD->TabUp();
}

void AMainPlayerController::TabDown()
{
	MainHUD->TabDown();
}

#pragma endregion UI_Interaction

void AMainPlayerController::PassiveRegenTick()
{
	UE_LOG(LogTemp, Warning, TEXT("PassiveRegenTick"));
	
	if (StatSubSys)
	{
		int32 numberInParty = StatSubSys->NumberOfPlayersInParty;

		for (int32 i = 0; i <= numberInParty - 1; i++)
		{
			FCharacterData charData = StatSubSys->GetCharacterData((ESelectedCharacter)i);
			if ((charData.CharacterName != "None") || (charData.CharacterName != "Empty"))
			{
				UE_LOG(LogTemp, Warning, TEXT("executing regen tick"));
				StatSubSys->RegenTick((ESelectedCharacter)i);
			}
		}
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("No Stat Component!"));
	}
}
// Fill out your copyright notice in the Description page of Project Settings.


#include "CombatManager.h"
#include "Engine/DataTable.h"
#include "Engine/World.h"
#include "Enemies/TestEnemy.h"
#include "Player/MainPlayer.h"
#include "ConstructorHelpers.h"
#include "Player/MainPlayerController.h"
#include "UI/CombatUI.h"
#include "Player/StatSubsystem.h"
#include "Player/InventorySubsystem.h"
#include "UI/MainHUD.h"

#pragma region Core
// Sets default values
ACombatManager::ACombatManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	auto EnemyDataTableData = ConstructorHelpers::FObjectFinder<UDataTable>(TEXT("DataTable'/Game/Enemies/Data/DT_EnemyData.DT_EnemyData'"));
	EnemyDataTable = EnemyDataTableData.Object;
}

// Called when the game starts or when spawned
void ACombatManager::BeginPlay()
{
	Super::BeginPlay();
	SetRefs();
	CreatePlayerArray();
	// roll for starting team
	bIsPlayerPhase = RollInitiative();
	CurrentCombatantsTurn = 0;
	CurrentEnemyIndex = 0;
	CurrentPlayerIndex = 0;
	StatSubSys->ResetTurnStats(ESelectedCharacter::SC_Character0);
}

// Called every frame
void ACombatManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ACombatManager::SetRefs()
{
	UGameInstance* GameInstance = Cast<UGameInstance>(GetGameInstance());
	StatSubSys = GameInstance->GetSubsystem<UStatSubsystem>();
	InventorySubSys = GameInstance->GetSubsystem<UInventorySubsystem>();
	AMainPlayerController* MPC = Cast<AMainPlayerController>(GetWorld()->GetFirstPlayerController());
	MainHUD = Cast<AMainHUD>(MPC->GetHUD());
}

bool ACombatManager::RollInitiative()
{
	bool InitiativeRoll = (bool)FMath::RandRange(0, 1);
	return InitiativeRoll;
}

#pragma endregion Core

#pragma region Getters
bool ACombatManager::GetIsPlayerTurn()
{
	return bIsPlayerPhase;
}

bool ACombatManager::GetIsCurrentEnemyDropped(int32 EnemyIndex)
{
	return EnemyData[EnemyIndex].bIsDropped;
}
#pragma endregion Getters

#pragma region CombatantData
void ACombatManager::CreatePlayerArray()
{
	CombatCharData.Empty();
	int32 arrayIndex = 0;
	
	arrayIndex = StatSubSys->NumberOfPlayersInParty - 1;
	for (int i = 0; i <= arrayIndex; i++)
	{
		FCharacterData characterToAdd = StatSubSys->GetCharacterData((ESelectedCharacter)arrayIndex);
		CombatCharData.Add(characterToAdd);
	}

}

void ACombatManager::LookupEnemy(int32 i)
{
	FString enemyIDstring = FString::FromInt(EnemyGroupArray[i]);
	FName enemyID = FName(*enemyIDstring);
	FString ContextString = "";
	FEnemyData* tempData = EnemyDataTable->FindRow<FEnemyData>(enemyID, ContextString, false);
	FEnemyData tempData2 = *(tempData);
	tempData2.bIsDropped = false;

	// add in randomized forename
	// TODO extract this as a function
	int32 tempNameModifierRoll = FMath::RandRange(0, tempData2.EnemyNameModifiers.Num() - 1);
	FString tempEnemyForeName = tempData2.EnemyNameModifiers[tempNameModifierRoll];
	FString tempEnemyName = tempEnemyForeName + " " + tempData2.EnemyName.ToString();
	tempData2.EnemyName = FName(*tempEnemyName);
	EnemyData.Add(tempData2);
}
#pragma endregion CombatantData

#pragma region EnemySpawning
void ACombatManager::SpawnTheEnemies()
{

	for (int i = EnemyGroupArray.Num() - 1; i > -1; i--)
	{
		LookupEnemy(i);
		FActorSpawnParameters spawnParams;		
		EnemyBP_REF = GetWorld()->SpawnActor<ATestEnemy>(EnemyBP, SetupTransform(), spawnParams);
		// TODO Replace vector.z with results from line trace vector.z
		EnemyBP_REF->SetActorLocation(FVector(EnemyBP_REF->GetActorLocation().X, EnemyBP_REF->GetActorLocation().Y, 20.0));
		EnemyBP_REFArray.Add(EnemyBP_REF);
	}

	NumberOfCombatants = CombatCharData.Num() + EnemyData.Num();
	ReportEnemiesInRoom();

	if (!bIsPlayerPhase)
	{
		BPNextTurn();
	}
}

FTransform ACombatManager::SetupTransform()
{
	AMainPlayerController* MainPC = Cast<AMainPlayerController>(GetWorld()->GetFirstPlayerController());
	AMainPlayer* PlayerChar = Cast<AMainPlayer>(MainPC->GetPawn());
	float groundZ = PlayerChar->GetActorLocation().Z;
	//TODO Line trace to ground to get floor vector z so enemy spawns on ground
	FVector tempVec = FVector(PlayerChar->GetActorLocation() +
		(PlayerChar->GetActorForwardVector() * FMath::RandRange(200, 300) * FVector(1.0, 1.0, 1.0) +
			FVector(FMath::RandRange(-100, 100), FMath::RandRange(-250, 250), 0)));
	FTransform spawnTransform = FTransform(GetActorRotation().Quaternion(), tempVec, FVector(0.75, 0.75, 0.75));
	return spawnTransform;
}

void ACombatManager::RemoveEnemies()
{
	for (int i = EnemyBP_REFArray.Num() - 1; i > -1; i--)
	{
		auto enemyToDie = EnemyBP_REFArray[i];
		if (enemyToDie)
		{
			enemyToDie->Destroy();
		}
	}
	EnemyBP_REFArray.Empty();
}

void ACombatManager::RemoveEnemyFromCombat(int32 TargetIndex)
{
	//EnemyData.RemoveAt(TargetIndex);
	auto enemyToDie = EnemyBP_REFArray[TargetIndex];
	FString tempNameString = EnemyData[TargetIndex].EnemyName.ToString();
	//TODO add bIsDropped to enemy data, set it to dropped here
	EnemyData[TargetIndex].bIsDropped = true;
	enemyToDie->Destroy();
	//EnemyBP_REFArray.RemoveAt(TargetIndex);
}
#pragma endregion EnemySpawning

#pragma region ReportToScreen
void ACombatManager::ReportEnemiesInRoom()
{
	FString alsoHereString = "";
	FString completedAlsoHereString = "Also here: ";
	for (uint8 i = 0; i <= EnemyData.Num() - 1; i++)
	{
		if (!EnemyData[i].bIsDropped)
		{
			// TODO fix this, last enemy always has a ","
			if ((EnemyData.Num() - 1 == 0) || (EnemyData.Num() - 1 == i))
			{
				alsoHereString += EnemyData[i].EnemyName.ToString() + ".";
			}
			else
			{
				alsoHereString += EnemyData[i].EnemyName.ToString() + ", ";
			}
		}
	}
	if (alsoHereString != "")
	{
		completedAlsoHereString += alsoHereString;
		MainHUD->ReportToUI(completedAlsoHereString, false);
	}
}
#pragma endregion ReportToScreen


#pragma region Physical Attacks
//Player Attack
void ACombatManager::AttackTarget(int32 TargetIndex, int32 AttackerIndex)
{
	FCharacterData currentCharacterData = StatSubSys->GetCharacterData((ESelectedCharacter)AttackerIndex);
	FString attackerTest = currentCharacterData.CharacterName.ToString();
	FItem equippedWeapon = InventorySubSys->GetEquippedItemInSlot((ESelectedCharacter)AttackerIndex, EEquipmentSlot::ES_Weapon);
	// do damage based on weapon
	int32 minWeaponDmg = 0;
	int32 maxWeaponDmg = 1;
	if (equippedWeapon.ItemName != "Empty")
	{
		minWeaponDmg = equippedWeapon.ItemCombatStats.WeaponMinHit;
		maxWeaponDmg = equippedWeapon.ItemCombatStats.WeaponMaxHit;
	}
	// TODO - factor in str and other bonuses
	int32 damageToDo = FMath::RandRange(minWeaponDmg, maxWeaponDmg);
	EnemyData[TargetIndex].EnemyHP -= damageToDo;
	FString tempEnemyName = EnemyData[TargetIndex].EnemyName.ToString();
	FString tempAttackerName = currentCharacterData.CharacterName.ToString();
	uint8 tempWeaponHitIndex = FMath::RandRange(0, equippedWeapon.ItemHitMsg.Num() - 1);
	FString tempWeaponHit = equippedWeapon.ItemHitMsg[tempWeaponHitIndex];
	FString tempHitString = tempAttackerName + " " + tempWeaponHit + " " + tempEnemyName + " with his " + equippedWeapon.ItemName.ToString() + " for " + FString::FromInt(damageToDo) + " damage!";
	//UE_LOG(LogTemp, Warning, TEXT("Attacking Enemy: %s, by %s, for %i damage!"), *nameTest, *attackerTest, damageToDo);
	MainHUD->ReportToUI(tempHitString, false);
	//BPReportDamage(tempAttackerName, tempEnemyName, damageToDo, tempHitString);
	currentCharacterData.bHasTakenTurn = true;
	if (EnemyData[TargetIndex].EnemyHP <= 0)
	{
		EnemiesDropped++;
		MainHUD->ReportToUI(EnemyData[TargetIndex].EnemyDeathMsg, false);
		GrantExp((EnemyData[TargetIndex].EnemyEXP * EnemyData[TargetIndex].EnemyEXPMultiplier), StatSubSys->NumberOfPlayersInParty);

		RemoveEnemyFromCombat(TargetIndex);
		ReportEnemiesInRoom();
	}
	
	CurrentCombatantsTurn++;

	if (EnemyBP_REFArray.Num() == EnemiesDropped)
	{
		// no enemies, end combat
		AMainPlayerController* MainPC = Cast<AMainPlayerController>(GetWorld()->GetFirstPlayerController());
		MainPC->ToggleCombat(EnemyGroupArray);
	}

	BPNextTurn();
}

// Enemy Attack
void ACombatManager::AttackPlayerTarget(int32 TargetIndex, int32 AttackerIndex)
{
	FCharacterData targetChar = StatSubSys->GetCharacterData((ESelectedCharacter)TargetIndex);
	// get enemy attack data here
	TArray<FEnemyAttacks> enemyAttackArray;
	FEnemyData attackingEnemyData;
	attackingEnemyData = EnemyData[AttackerIndex];
	enemyAttackArray = attackingEnemyData.EnemyAttacks;
	int32 currentEnegryThisTurn = attackingEnemyData.EnemyEnergy;
	// roll random 0 - numberOfAttacks to see which attack is used
	// check if currentEnegyThisTurn is sufficient to use attack (possible function here)
	//UE_LOG(LogTemp, Warning, TEXT("About to run ChooseAttack"));
	FEnemyAttacks chosenAttack;
	chosenAttack = ChooseAttack(enemyAttackArray, attackingEnemyData);
	if (attackingEnemyData.bIsDropped)
	{
		return;
	}
	if (currentEnegryThisTurn >= chosenAttack.AttackReqEnergy)
	{
		currentEnegryThisTurn -= chosenAttack.AttackReqEnergy;
		uint8 attackRoll = FMath::RandRange(0, 100);
		if (attackRoll <= chosenAttack.AttackAccuracy)
		{
			DoAttack(chosenAttack, (ESelectedCharacter)TargetIndex, attackingEnemyData);
			EnemyData[AttackerIndex].bHasTakenTurn = true;
		}
		else
		{
			EnemyData[AttackerIndex].bHasTakenTurn = true;
			FString attackerString = attackingEnemyData.EnemyName.ToString();
			FString targetString = targetChar.CharacterName.ToString();
			FString missString = chosenAttack.AttackMissMsg;
			FText attackMissString = FText::FormatNamed(FText::FromString(missString), TEXT("attacker"), FText::FromString(attackerString), TEXT("target"), FText::FromString(targetString));
			MainHUD->ReportToUI(attackMissString.ToString(), false);
		}
	}
	else
	{
		///Maybe
		NewNextTurn();
	}
}

FEnemyAttacks ACombatManager::ChooseAttack(TArray<FEnemyAttacks> AttackArray, FEnemyData AttackingEnemyData)
{
	int32 currentEnergyThisTurn = AttackingEnemyData.EnemyEnergy;
	FEnemyAttacks chosenAttack;
	TArray<FEnemyAttacks> attackRollArray;
	for (int i = AttackArray.Num() - 1; i > -1; i--)
	{
		int32 tempInt = AttackArray[i].AttackChanceOfUse;
		int32 numberOfEntries = tempInt / 10;
		for (int j = numberOfEntries; j > 0; j--)
		{
			attackRollArray.Add(AttackArray[i]);
		}
	}
	int32 chosenAttackRoll = FMath::RandRange(0, 9);
	chosenAttack = attackRollArray[chosenAttackRoll];
	return chosenAttack;
}

void ACombatManager::DoAttack(FEnemyAttacks chosenAttack, ESelectedCharacter Target, FEnemyData AttackingEnemyData)
{
	FCharacterData currentCharacterData = StatSubSys->GetCharacterData(Target);
	int32 damageToDo = FMath::RandRange(chosenAttack.AttackMinHit, chosenAttack.AttackMaxHit);
	
	FString tempPlayerName = currentCharacterData.CharacterName.ToString();
	FString tempEnemyName = AttackingEnemyData.EnemyName.ToString();
	FString textDamage = FString::FromInt(damageToDo);
	FText hitString = FText::FormatNamed(FText::FromString(chosenAttack.AttackHitMsg), TEXT("attacker"), FText::FromString(tempEnemyName), TEXT("target"), FText::FromString(tempPlayerName), TEXT("damage"), FText::FromString(textDamage));
	FString tempHitString = hitString.ToString();
	MainHUD->ReportToUI(tempHitString, false);

	// TODO - Use character modifiers ACDR, and such

	StatSubSys->ReduceTargetHPByAmount(Target, damageToDo);

}
#pragma endregion Physical Attacks

void ACombatManager::NewNextTurn()
{
	// check if players turn
	if (CurrentRound % 3 == 0)
	{
		if (bIsRegenTick)
		{
			UE_LOG(LogTemp, Warning, TEXT("Regen Tick Round"));
			StatSubSys->RegenTick((ESelectedCharacter)CurrentPlayerIndex);
			bIsRegenTick = false;
		}
	}
	if (bIsPlayerPhase)
	{
		switch (CurrentPlayerIndex)
		{
		case 0:
			if (CombatCharData[CurrentPlayerIndex].bIsDropped)
			{
				if (CurrentPlayerIndex + 1 >= CombatCharData.Num())
				{
					bIsPlayerPhase = false;
					CurrentEnemyIndex = 0;
					EnemyData[CurrentEnemyIndex].EnemyEnergy = 1000;
					StatSubSys->ResetTurnStats((ESelectedCharacter)CurrentPlayerIndex);
					break;
				}
				else
				{
					StatSubSys->ResetTurnStats((ESelectedCharacter)CurrentPlayerIndex);
					CurrentPlayerIndex++;
					break;
				}
			}
			else
			{
				if (CombatCharData[CurrentPlayerIndex].bHasTakenTurn)
				{
					if (CurrentPlayerIndex + 1 >= CombatCharData.Num())
					{
						bIsPlayerPhase = false;
						CurrentEnemyIndex = 0; 
						EnemyData[CurrentEnemyIndex].EnemyEnergy = 1000;
						break;
					}
					else
					{
						StatSubSys->ResetTurnStats((ESelectedCharacter)CurrentPlayerIndex);
						CurrentPlayerIndex++;
						break;
					}
				}
				else
				{
					CombatCharData[CurrentPlayerIndex].bHasTakenTurn = true;
					StatSubSys->ResetTurnStats((ESelectedCharacter)CurrentPlayerIndex);
					CurrentPlayerIndex++;
					if (CurrentPlayerIndex >= CombatCharData.Num())
					{
						CurrentEnemyIndex = 0;
						EnemyData[CurrentEnemyIndex].EnemyEnergy = 1000;
						bIsPlayerPhase = false;
					}
					break;
				}
			}
			break;
		case 1:
			if (CombatCharData[CurrentPlayerIndex].bIsDropped)
			{
				if (CurrentPlayerIndex + 1 >= CombatCharData.Num())
				{
					bIsPlayerPhase = false;
					CurrentEnemyIndex = 0;
					EnemyData[CurrentEnemyIndex].EnemyEnergy = 1000;
					break;
				}
				else
				{
					StatSubSys->ResetTurnStats((ESelectedCharacter)CurrentPlayerIndex);
					CurrentPlayerIndex++;
					break;
				}
			}
			else
			{
				if (CombatCharData[CurrentPlayerIndex].bHasTakenTurn)
				{
					if (CurrentPlayerIndex + 1 >= CombatCharData.Num())
					{
						bIsPlayerPhase = false;
						CurrentEnemyIndex = 0;
						EnemyData[CurrentEnemyIndex].EnemyEnergy = 1000;
						break;
					}
					else
					{
						StatSubSys->ResetTurnStats((ESelectedCharacter)CurrentPlayerIndex);
						CurrentPlayerIndex++;
						break;
					}
				}
				else
				{
					CombatCharData[CurrentPlayerIndex].bHasTakenTurn = true;
					StatSubSys->ResetTurnStats((ESelectedCharacter)CurrentPlayerIndex);
					CurrentPlayerIndex++;
					if (CurrentPlayerIndex >= CombatCharData.Num())
					{
						CurrentEnemyIndex = 0;
						EnemyData[CurrentEnemyIndex].EnemyEnergy = 1000;
						bIsPlayerPhase = false;
					}
					break;
				}
			}
			break;
		case 2:
			if (CombatCharData[CurrentPlayerIndex].bIsDropped)
			{
				if (CurrentPlayerIndex + 1 >= CombatCharData.Num())
				{
					bIsPlayerPhase = false;
					CurrentEnemyIndex = 0;
					EnemyData[CurrentEnemyIndex].EnemyEnergy = 1000;
					break;
				}
				else
				{
					StatSubSys->ResetTurnStats((ESelectedCharacter)CurrentPlayerIndex);
					CurrentPlayerIndex++;
					break;
				}
			}
			else
			{
				if (CombatCharData[CurrentPlayerIndex].bHasTakenTurn)
				{
					if (CurrentPlayerIndex + 1 >= CombatCharData.Num())
					{
						bIsPlayerPhase = false;
						CurrentEnemyIndex = 0;
						EnemyData[CurrentEnemyIndex].EnemyEnergy = 1000;
						StatSubSys->ResetTurnStats((ESelectedCharacter)CurrentPlayerIndex);
						break;
					}
					else
					{
						StatSubSys->ResetTurnStats((ESelectedCharacter)CurrentPlayerIndex);
						CurrentPlayerIndex++;
						break;
					}
				}
				else
				{
					CombatCharData[CurrentPlayerIndex].bHasTakenTurn = true;
					StatSubSys->ResetTurnStats((ESelectedCharacter)CurrentPlayerIndex);
					CurrentPlayerIndex++;
					if (CurrentPlayerIndex >= CombatCharData.Num())
					{
						CurrentEnemyIndex = 0;
						EnemyData[CurrentEnemyIndex].EnemyEnergy = 1000;
						bIsPlayerPhase = false;
					}
					break;
				}
			}
			break;
		}
	}
	
	else
	{
		switch (CurrentEnemyIndex)
		{
		case 0:
			if (EnemyData[CurrentEnemyIndex].bIsDropped)
			{
				if (CurrentEnemyIndex + 1 >= EnemyData.Num())
				{
					bIsPlayerPhase = true;
					CurrentPlayerIndex = 0;
					StatSubSys->ResetTurnStats((ESelectedCharacter)CurrentPlayerIndex);
					ResetEnemyTurn(CurrentEnemyIndex);
					CurrentRound++;
					bIsRegenTick = true;
					CurrentEnemyIndex++;
					break;
				}
				else
				{
					ResetEnemyTurn(CurrentEnemyIndex);
					CurrentEnemyIndex++;
					break;
				}
			}
			else
			{
				if (EnemyData[CurrentEnemyIndex].bHasTakenTurn)
				{
					if (CurrentEnemyIndex + 1 >= EnemyData.Num())
					{
						bIsPlayerPhase = true;
						CurrentPlayerIndex = 0;
						StatSubSys->ResetTurnStats((ESelectedCharacter)CurrentPlayerIndex);
						ResetEnemyTurn(CurrentEnemyIndex);
						CurrentRound++;
						bIsRegenTick = true;
						CurrentEnemyIndex++;
						break;
					}
					else
					{
						ResetEnemyTurn(CurrentEnemyIndex);
						CurrentEnemyIndex++;
						break;
					}
				}
				else
				{
					EnemyData[CurrentEnemyIndex].bHasTakenTurn = true;
					EnemyData[CurrentEnemyIndex].EnemyEnergy = 1000;
					CurrentEnemyIndex++;
					if (CurrentEnemyIndex >= EnemyData.Num())
					{
						CurrentPlayerIndex = 0;
						CurrentRound++;
						bIsRegenTick = true;
						StatSubSys->ResetTurnStats((ESelectedCharacter)CurrentPlayerIndex);
						bIsPlayerPhase = true;
					}
					break;
				}
			}
			break;
		case 1:
			if (EnemyData[CurrentEnemyIndex].bIsDropped)
			{
				if (CurrentEnemyIndex + 1 >= EnemyData.Num())
				{
					bIsPlayerPhase = true;
					CurrentPlayerIndex = 0;
					CurrentRound++;
					bIsRegenTick = true;
					StatSubSys->ResetTurnStats((ESelectedCharacter)CurrentPlayerIndex);
					ResetEnemyTurn(CurrentEnemyIndex);
					CurrentEnemyIndex++;
					break;
				}
				else
				{
					ResetEnemyTurn(CurrentEnemyIndex);
					CurrentEnemyIndex++;
					break;
				}
			}
			else
			{
				if (EnemyData[CurrentEnemyIndex].bHasTakenTurn)
				{
					if (CurrentEnemyIndex + 1 >= EnemyData.Num())
					{
						bIsPlayerPhase = true;
						CurrentPlayerIndex = 0;
						CurrentRound++;
						bIsRegenTick = true;
						StatSubSys->ResetTurnStats((ESelectedCharacter)CurrentPlayerIndex);
						ResetEnemyTurn(CurrentEnemyIndex);
						CurrentEnemyIndex++;
						break;
					}
					else
					{
						ResetEnemyTurn(CurrentEnemyIndex);
						CurrentEnemyIndex++;
						break;
					}
				}
				else
				{
					EnemyData[CurrentEnemyIndex].bHasTakenTurn = true;
					EnemyData[CurrentEnemyIndex].EnemyEnergy = 1000;
					CurrentEnemyIndex++;
					if (CurrentEnemyIndex >= EnemyData.Num())
					{
						CurrentPlayerIndex = 0;
						CurrentRound++;
						bIsRegenTick = true;
						StatSubSys->ResetTurnStats((ESelectedCharacter)CurrentPlayerIndex);
						bIsPlayerPhase = true;
					}
					break;
				}
			}
			break;
		case 2:
			if (EnemyData[CurrentEnemyIndex].bIsDropped)
			{
				if (CurrentEnemyIndex + 1 >= EnemyData.Num())
				{
					bIsPlayerPhase = true;
					CurrentPlayerIndex = 0;
					CurrentRound++;
					bIsRegenTick = true;
					StatSubSys->ResetTurnStats((ESelectedCharacter)CurrentPlayerIndex);
					ResetEnemyTurn(CurrentEnemyIndex);
					CurrentEnemyIndex++;
					break;
				}
				else
				{
					ResetEnemyTurn(CurrentEnemyIndex);
					CurrentEnemyIndex++;
					break;
				}
				break;
			}
			else
			{
				if (EnemyData[CurrentEnemyIndex].bHasTakenTurn)
				{
					if (CurrentEnemyIndex + 1 >= EnemyData.Num())
					{
						bIsPlayerPhase = true;
						CurrentPlayerIndex = 0;
						CurrentRound++;
						bIsRegenTick = true;
						StatSubSys->ResetTurnStats((ESelectedCharacter)CurrentPlayerIndex);
						ResetEnemyTurn(CurrentEnemyIndex);
						CurrentEnemyIndex++;
						break;
					}
					else
					{
						ResetEnemyTurn(CurrentEnemyIndex);
						CurrentEnemyIndex++;
						break;
					}
				}
				else
				{
					EnemyData[CurrentEnemyIndex].bHasTakenTurn = true;
					EnemyData[CurrentEnemyIndex].EnemyEnergy = 1000;
					CurrentEnemyIndex++;
					if (CurrentEnemyIndex >= EnemyData.Num())
					{
						CurrentPlayerIndex = 0;
						CurrentRound++;
						bIsRegenTick = true;
						StatSubSys->ResetTurnStats((ESelectedCharacter)CurrentPlayerIndex);
						bIsPlayerPhase = true;
					}
					break;
				}
			}
			break;
		case 3:
			if (EnemyData[CurrentEnemyIndex].bIsDropped)
			{
				if (CurrentEnemyIndex + 1 >= EnemyData.Num())
				{
					bIsPlayerPhase = true;
					CurrentPlayerIndex = 0;
					CurrentRound++;
					bIsRegenTick = true;
					StatSubSys->ResetTurnStats((ESelectedCharacter)CurrentPlayerIndex);
					ResetEnemyTurn(CurrentEnemyIndex);
					CurrentEnemyIndex++;
					break;
				}
				else
				{
					ResetEnemyTurn(CurrentEnemyIndex);
					CurrentEnemyIndex++;
					break;
				}
			}
			else
			{
				if (EnemyData[CurrentEnemyIndex].bHasTakenTurn)
				{
					if (CurrentEnemyIndex + 1 >= EnemyData.Num())
					{
						bIsPlayerPhase = true;
						CurrentPlayerIndex = 0;
						CurrentRound++;
						bIsRegenTick = true;
						StatSubSys->ResetTurnStats((ESelectedCharacter)CurrentPlayerIndex);
						ResetEnemyTurn(CurrentEnemyIndex);
						break;
					}
					else
					{
						ResetEnemyTurn(CurrentEnemyIndex);
						CurrentEnemyIndex++;
						break;
					}
				}
				else
				{
					EnemyData[CurrentEnemyIndex].bHasTakenTurn = true;
					EnemyData[CurrentEnemyIndex].EnemyEnergy = 1000;
					CurrentEnemyIndex++;
					if (CurrentEnemyIndex >= EnemyData.Num())
					{
						CurrentPlayerIndex = 0;
						CurrentRound++;
						bIsRegenTick = true;
						StatSubSys->ResetTurnStats((ESelectedCharacter)CurrentPlayerIndex);
						bIsPlayerPhase = true;
					}
					break;
				}
			}
			break;
		case 4:
			if (EnemyData[CurrentEnemyIndex].bIsDropped)
			{
				if (CurrentEnemyIndex + 1 >= EnemyData.Num())
				{
					bIsPlayerPhase = true;
					CurrentPlayerIndex = 0;
					CurrentRound++;
					bIsRegenTick = true;
					StatSubSys->ResetTurnStats((ESelectedCharacter)CurrentPlayerIndex);
					ResetEnemyTurn(CurrentEnemyIndex);
					CurrentEnemyIndex++;
					break;
				}
				else
				{
					ResetEnemyTurn(CurrentEnemyIndex);
					CurrentEnemyIndex++;
					break;
				}
			}
			else
			{
				if (EnemyData[CurrentEnemyIndex].bHasTakenTurn)
				{
					if (CurrentEnemyIndex + 1 >= EnemyData.Num())
					{
						bIsPlayerPhase = true;
						CurrentPlayerIndex = 0;
						CurrentRound++;
						bIsRegenTick = true;
						StatSubSys->ResetTurnStats((ESelectedCharacter)CurrentPlayerIndex);
						ResetEnemyTurn(CurrentEnemyIndex);
						break;
					}
					else
					{
						ResetEnemyTurn(CurrentEnemyIndex);
						CurrentEnemyIndex++;
						break;
					}
				}
				else
				{
					EnemyData[CurrentEnemyIndex].bHasTakenTurn = true;
					EnemyData[CurrentEnemyIndex].EnemyEnergy = 1000;
					CurrentEnemyIndex++;
					if (CurrentEnemyIndex >= EnemyData.Num())
					{
						CurrentPlayerIndex = 0;
						CurrentRound++;
						bIsRegenTick = true;
						StatSubSys->ResetTurnStats((ESelectedCharacter)CurrentPlayerIndex);
						bIsPlayerPhase = true;
					}
					break;
				}
			}
			break;
		case 5:
			if (EnemyData[CurrentEnemyIndex].bIsDropped)
			{
				if (CurrentEnemyIndex + 1 >= EnemyData.Num())
				{
					bIsPlayerPhase = true;
					CurrentPlayerIndex = 0;
					CurrentRound++;
					bIsRegenTick = true;
					StatSubSys->ResetTurnStats((ESelectedCharacter)CurrentPlayerIndex);
					ResetEnemyTurn(CurrentEnemyIndex);
					CurrentEnemyIndex++;
					break;
				}
				else
				{
					ResetEnemyTurn(CurrentEnemyIndex);
					CurrentEnemyIndex++;
					break;
				}
			}
			else
			{
				if (EnemyData[CurrentEnemyIndex].bHasTakenTurn)
				{
					if (CurrentEnemyIndex + 1 >= EnemyData.Num())
					{
						bIsPlayerPhase = true;
						CurrentPlayerIndex = 0;
						CurrentRound++;
						bIsRegenTick = true;
						StatSubSys->ResetTurnStats((ESelectedCharacter)CurrentPlayerIndex);
						ResetEnemyTurn(CurrentEnemyIndex);
						break;
					}
					else
					{
						ResetEnemyTurn(CurrentEnemyIndex);
						CurrentEnemyIndex++;
						break;
					}
				}
				else
				{
					EnemyData[CurrentEnemyIndex].bHasTakenTurn = true;
					EnemyData[CurrentEnemyIndex].EnemyEnergy = 1000;
					CurrentEnemyIndex++;
					if (CurrentEnemyIndex >= EnemyData.Num())
					{
						CurrentPlayerIndex = 0;
						CurrentRound++;
						bIsRegenTick = true;
						StatSubSys->ResetTurnStats((ESelectedCharacter)CurrentPlayerIndex);
						bIsPlayerPhase = true;
					}
					break;
				}
			}
			break;
		}
	}
}

void ACombatManager::GrantExp(int32 AmountToGrant, int32 NumberInParty)
{
	int32 expForEach;
	expForEach = AmountToGrant / NumberInParty;
	for (int32 characterIndex = NumberInParty - 1; characterIndex > -1; characterIndex--)
	{
		StatSubSys->GrantSomeExp((ESelectedCharacter)characterIndex, expForEach);
	}
	FString tempExpString;
	FString tempExp;
	tempExp = FString::FromInt(expForEach);
	tempExpString = "Each character gains " + tempExp + " experience.";
	MainHUD->ReportToUI(tempExpString, false);
}

#pragma region Spellcasting
// Player spell cast
bool ACombatManager::CastSpell(ESelectedCharacter SelectedChar, int32 TargetIndex, FSpell SpellBeingCast, bool bIsTargetFriendly)
{
	// get char data
	FCharacterData casterCharData = StatSubSys->GetCharacterData(SelectedChar);
	FEnemyData targetData;
	FCharacterData friendlyTargetData;
	int32 lookupIndex = 0;
		
	// check if can cast (energy, mana, level, magery) *Make function*
	if (CanSpellBeCast(casterCharData, SpellBeingCast))
	{
		// use energy/mana
		casterCharData.CharacterBaseStats.MP_Current -= SpellBeingCast.SpellManaCost;
		casterCharData.CharacterCombatStats.Energy -= SpellBeingCast.SpellEnergyCost;
		StatSubSys->ReduceCastedSpellStats(SelectedChar, SpellBeingCast.SpellManaCost, SpellBeingCast.SpellEnergyCost);
		FText casterText;
		FText targetText;
		FText damageText;
		FString reportString;
		// check difficulty vs spellcasting
		if ((casterCharData.CharacterCombatStats.Spellcasting - SpellBeingCast.SpellDifficulty) > 1)
		{
			// spell has single target
			if (SpellBeingCast.SpellTargetType == ETargetType::TT_Any || SpellBeingCast.SpellTargetType == ETargetType::TT_Enemy || SpellBeingCast.SpellTargetType == ETargetType::TT_Self || SpellBeingCast.SpellTargetType == ETargetType::TT_Friendly)
			{
				if (!bIsTargetFriendly)
				{
					// look up target
					targetData = EnemyData[TargetIndex];
					switch (SpellBeingCast.SpellApplication)
					{
					case EAbilApplication::AA_ApplyEffect:
						// apply buff/debuff
						break;
					case EAbilApplication::AA_Direct:
						// roll damage 
						int32 damageToDo;
						damageToDo = RollSpellDamage(casterCharData, SpellBeingCast);
						// apply resists
						// report damage
						casterText = FText::FromName(casterCharData.CharacterName);
						targetText = FText::FromName(EnemyData[TargetIndex].EnemyName);
						damageText = FText::FromString(FString::FromInt(damageToDo));
						reportString = FText::FormatNamed((SpellBeingCast.SpellCastMsg), TEXT("caster"), casterText, TEXT("target"), targetText, TEXT("damage"), damageText).ToString();
						MainHUD->ReportToUI(reportString, false);
						// apply damage
						DoSpellDamageToTarget(damageToDo, TargetIndex, bIsTargetFriendly);
						break;
					case EAbilApplication::AA_OverTime:
						// do damage over time stuffs
						break;
					case EAbilApplication::AA_RemoveEffect:
						// remove buff/debuff
						break;
					}
				}
				else
					/// Target is friendly, damage now does heals
				{
					// lookup target
					UE_LOG(LogTemp, Warning, TEXT("Target is friendly!"));
					friendlyTargetData = CombatCharData[TargetIndex];
					switch (SpellBeingCast.SpellApplication)
					{
					case EAbilApplication::AA_ApplyEffect:
						// apply buff/debuff
						break;
					case EAbilApplication::AA_Direct:
						// roll damage 
						int32 healingToDo;
						healingToDo = RollSpellDamage(casterCharData, SpellBeingCast);
						// apply damage
						casterText = FText::FromName(casterCharData.CharacterName);
						targetText = FText::FromName(CombatCharData[TargetIndex].CharacterName);
						damageText = FText::FromString(FString::FromInt(healingToDo));
						reportString = FText::FormatNamed((SpellBeingCast.SpellCastMsg), TEXT("caster"), casterText, TEXT("target"), targetText, TEXT("damage"), damageText).ToString();
						MainHUD->ReportToUI(reportString, false);
						DoSpellDamageToTarget(healingToDo, TargetIndex, bIsTargetFriendly);
						StatSubSys->SetPlayerHasCast(SelectedChar);
						break;
					case EAbilApplication::AA_OverTime:
						// do damage over time stuffs
						break;
					case EAbilApplication::AA_RemoveEffect:
						// remove buff/debuff
						break;
					
					}
					// check resists
					// roll amount (heal/buff)
					// apply heal/buff
				}
				// is target dropped?
					// dropped return false (no more casting against this target
				// target not dropped
				// energy remaining?
					// energy remains return true (can cast again)
					// no energy left return false (nothing left to do)
			}
			else
			{
				// spell has multiple targets
			}
		}
	}
	else
	{
		return false;
	}
	
	return false;
}

bool ACombatManager::CanSpellBeCast(FCharacterData CasterCharData, FSpell SpellToCheck)
{
	int32 casterEnergy = CasterCharData.CharacterCombatStats.Energy;
	int32 casterMana = CasterCharData.CharacterBaseStats.MP_Current;
	int32 casterLevel = CasterCharData.CharacterBaseStats.Level;
	// if any of these fail to meet spell data, return false

	if (casterLevel < SpellToCheck.SpellRequiredLevel)
	{
		MainHUD->ReportToUI("Your level is too low to cast this spell!", false);
		return false;
	}
	if (casterEnergy < SpellToCheck.SpellEnergyCost)
	{
		MainHUD->ReportToUI("You have already cast a spell this round!", false);
		return false;
	}
	if (casterMana < SpellToCheck.SpellManaCost)
	{
		MainHUD->ReportToUI("You do not have enough mana to cast that spell!", false);
		return false;
	}
	if (CasterCharData.bHasCastSpell)
	{
		MainHUD->ReportToUI("You have already cast a spell this round!", false);
		return false;
	}
	// otherwise
	return true;
}

int32 ACombatManager::RollSpellDamage(FCharacterData CasterData, FSpell SpellBeingCast)
{
	int32 finalRoll = 0;
	int32 casterLevel = CasterData.CharacterBaseStats.Level;
	int32 modifiedMax = 0;
	int32 modifiedMin = 0;
	FString tempSpellName = SpellBeingCast.SpellName.ToString();

	// only modify if stat should be increased (ie. a multiple of levels/max increase or level is high enough for mod to have happened previously)
	if ((casterLevel >= SpellBeingCast.SpellLevelsPerMaxIncrease) || (casterLevel % SpellBeingCast.SpellLevelsPerMaxIncrease == 0))
	{	
		if (SpellBeingCast.SpellLevelsPerMaxIncrease != 0)
		{
			int32 modCasterLevel = FMath::FloorToInt(casterLevel / SpellBeingCast.SpellLevelsPerMaxIncrease);
			// clamp to avoid spell growing over level cap, and so max cannot be zero
			modifiedMax = FMath::Clamp(((SpellBeingCast.SpellLevelsPerMaxIncrease / modCasterLevel) * SpellBeingCast.SpellMaxIncreaseAmount),
				SpellBeingCast.SpellBaseMax,
				(((SpellBeingCast.SpellLevelsPerMaxIncrease / SpellBeingCast.SpellLevelCap) * SpellBeingCast.SpellMaxIncreaseAmount) + SpellBeingCast.SpellBaseMax));
		}
		else
		{
			modifiedMax = SpellBeingCast.SpellBaseMax;
		}
	}
	else
	{
		modifiedMax = SpellBeingCast.SpellBaseMax;
	}
	// clamp to avoid spell growing over level cap, and so min cannot be zero
	if ((casterLevel >= SpellBeingCast.SpellLevelsPerMinIncrease) || (casterLevel % SpellBeingCast.SpellLevelsPerMinIncrease == 0))
	{				
		if (SpellBeingCast.SpellLevelsPerMinIncrease != 0)
		{
			int32 modCasterLevel = FMath::FloorToInt(casterLevel / SpellBeingCast.SpellLevelsPerMinIncrease);
			// clamp to avoid spell growing over level cap, and so min cannot be zero
			modifiedMax = FMath::Clamp(((SpellBeingCast.SpellLevelsPerMinIncrease / modCasterLevel) * SpellBeingCast.SpellMinIncreaseAmount),
				SpellBeingCast.SpellBaseMin,
				(((SpellBeingCast.SpellLevelsPerMinIncrease / SpellBeingCast.SpellLevelCap) * SpellBeingCast.SpellMinIncreaseAmount) + SpellBeingCast.SpellBaseMin));
		}
		else
		{
			modifiedMin = SpellBeingCast.SpellBaseMin;
		}
	}
	else
	{
		modifiedMin = SpellBeingCast.SpellBaseMin;
	}
	// roll damage with modified min/max
	finalRoll = FMath::RandRange(modifiedMin, modifiedMax);
	return finalRoll;
}

void ACombatManager::DoSpellDamageToTarget(int32 damageToDo, int32 TargetIndex, bool bIsTargetFriendly)
{
	if (!bIsTargetFriendly)
	{
		UE_LOG(LogTemp, Warning, TEXT("Target is hostile!"));
		EnemyData[TargetIndex].EnemyHP -= damageToDo;
		if (EnemyData[TargetIndex].EnemyHP <= 0)
		{
			EnemyData[TargetIndex].bIsDropped = true;
			EnemiesDropped++;
			GrantExp((EnemyData[TargetIndex].EnemyEXP * EnemyData[TargetIndex].EnemyEXPMultiplier), StatSubSys->NumberOfPlayersInParty);
			RemoveEnemyFromCombat(TargetIndex);
			ReportEnemiesInRoom();
		}

		if (EnemyBP_REFArray.Num() == EnemiesDropped)
		{
			// no enemies, end combat
			MainPC->ToggleCombat(EnemyGroupArray);
		}
		BPNextTurn();
	}
	else
	{
		StatSubSys->ReduceTargetHPByAmount((ESelectedCharacter)TargetIndex, -damageToDo);
	}
}

#pragma endregion Spellcasting

void ACombatManager::ResetEnemyTurn(int32 EnemyIndex)
{
	EnemyData[CurrentEnemyIndex].bHasTakenTurn = false;
	EnemyData[CurrentEnemyIndex].bHasCastSpell = false;
	EnemyData[CurrentEnemyIndex].EnemyEnergy = 1000;
}

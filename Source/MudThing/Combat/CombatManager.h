// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GlobalData.h"
#include "CombatManager.generated.h"

class ATestEnemy;

UENUM(BlueprintType)
enum class EClickedAttackType : uint8
{
	CAT_Attack,
	CAT_Magic,
	CAT_Defend,
	CAT_Flee
};

UCLASS()
class MUDTHING_API ACombatManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACombatManager();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ATestEnemy> EnemyBP;

	UPROPERTY()
	ATestEnemy* EnemyBP_REF;

	UPROPERTY()
	class AMainPlayerController* MainPC;

	UPROPERTY()
	class AMainPlayer* PlayerChar;

	UPROPERTY()
	class UStatSubsystem* StatSubSys;

	UPROPERTY()
	class AMainHUD* MainHUD;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	TArray<ATestEnemy*> EnemyBP_REFArray;

	UFUNCTION()
	void RemoveEnemyFromCombat(int32 TargetIndex);

	UFUNCTION()
	FEnemyAttacks ChooseAttack(TArray<FEnemyAttacks> AttackArray, FEnemyData AttackingEnemyData);
	
	UFUNCTION()
	void DoAttack(FEnemyAttacks chosenAttack, ESelectedCharacter Target, FEnemyData AttackingEnemyData);

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	EClickedAttackType ClickedAttackType;

	UFUNCTION(BlueprintCallable)
	void AttackTarget(int32 TargetIndex, int32 AttackerIndex);

	UFUNCTION(BlueprintCallable)
	void AttackPlayerTarget(int32 TargetIndex, int32 AttackerIndex);

	UFUNCTION(BlueprintImplementableEvent)
	void BPReportDamage(const FString& AttackerStr, const FString& TargetStr, int32 DamageDoneNum, const FString& HitStringStr);

	UPROPERTY()
	class UInventorySubsystem* InventorySubSys;

	UFUNCTION(BlueprintImplementableEvent)
	void BPUpdateTurnPhase();

	UFUNCTION()
	void GrantExp(int32 AmountToGrant, int32 NumberInParty);

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	uint8 NumberOfCombatants;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	uint8 EnemiesDropped;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	uint8 CurrentCombatantsTurn;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	TArray<FCharacterData> CombatCharData;

	UFUNCTION(BlueprintCallable)
	void NewNextTurn();

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	uint8 CurrentPlayerIndex = 0;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	uint8 CurrentEnemyIndex = 0;

	UFUNCTION(BlueprintCallable)
	bool GetIsPlayerTurn();

	UFUNCTION(BlueprintCallable)
	bool GetIsCurrentEnemyDropped(int32 EnemyIndex);

	UFUNCTION(BlueprintCallable)
	bool CastSpell(ESelectedCharacter SelectedChar, int32 TargetIndex, FSpell SpellBeingCast, bool bIsTargetFriendly);

	UFUNCTION()
	void ResetEnemyTurn(int32 EnemyIndex);

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	bool bIsRegenTick;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	int32 CurrentRound = 0;

private:
	UFUNCTION()
	FTransform SetupTransform();

	UFUNCTION()
	void LookupEnemy(int32 i);

	UPROPERTY()
	class UDataTable* EnemyDataTable;

	UFUNCTION()
	bool CanSpellBeCast(FCharacterData CasterCharData, FSpell SpellToCheck);

	UFUNCTION()
	int32 RollSpellDamage(FCharacterData CasterData, FSpell SpellBeingCast);

	UFUNCTION()
	void DoSpellDamageToTarget(int32 damageToDo, int32 TargetIndex, bool bIsTargetFriendly);

	UFUNCTION()
	void SetRefs();

	UFUNCTION()
	void ReportEnemiesInRoom();

	UFUNCTION()
	bool RollInitiative();

	UFUNCTION()
	void CreatePlayerArray();

public:	
	
	UFUNCTION()
	void SpawnTheEnemies();

	UFUNCTION()
	void RemoveEnemies();

	UFUNCTION(BlueprintImplementableEvent)
	void BPNextTurn();

	virtual void Tick(float DeltaTime) override;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	TArray<int32> EnemyGroupArray;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	TArray<FEnemyData> EnemyData;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	bool bIsPlayerPhase = false;
};
